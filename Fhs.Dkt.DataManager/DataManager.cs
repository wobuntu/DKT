﻿// CREATED BY MATHIAS LACKNER
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

using Newtonsoft.Json;

using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Messaging;


[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.DataManager))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This component is responsible of serializing/deserializing objects and data from/to files. Note that also private members
    /// will be serialized / deserialized.
    /// Created by Mathias Lackner.
    /// </summary>
    public class DataManager : IDataManager
    {
        /// <summary>
        /// The only constructor of the <see cref="DataManager"/>, which is used to setup a default configuration for the serialization.
        /// </summary>
        public DataManager()
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new Fhs.Dkt.Types.JsonHiddenPropertyResolver()
            };

            //this.Notify(new LogOnlyMessage("DataManager created."));
        }


        /// <summary>
        /// This private method generates a default file name for the given type parameter. It is used if the file name is omitted in
        /// other methods of the <see cref="DataManager"/> class.
        /// </summary>
        /// <typeparam name="T">The type of object which shall be serialized/deserialized.</typeparam>
        private string getDefaultSerializationPath<T>() where T : class
        {
            string filename = typeof(T) + ".json";

            // Get the executing assembly (not calling or entry assembly, so that data is always expected to be located relative to the
            // current component), thanks to stackoverflow:
            // https://stackoverflow.com/questions/52797/how-do-i-get-the-path-of-the-assembly-the-code-is-in
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string progPath = Uri.UnescapeDataString(uri.Path);

            return Path.Combine(Path.GetDirectoryName(progPath), "Data", filename);
        }


        /// <summary>
        /// Deserializes a list of objects (of type <typeparamref name="T"/>) from the given file. If no filename is given, a default one
        /// is generated from the type name. Note that the type of <typeparamref name="T"/> may also be a base class or an interface and
        /// that also private members are deserialized.
        /// </summary>
        /// <typeparam name="T">The type of the objects to deserialize (or a common base class or interface).</typeparam>
        /// <param name="filename">The name of the file to use or null if a default file for the given type shall be used.</param>
        public List<T> DeserializeList<T>(string filename = null)
        {
            if (filename == null)
                filename = getDefaultSerializationPath<List<T>>();

            if (!File.Exists(filename))
                throw new FileNotFoundException($"Cannot deserialize data for type '{typeof(List<T>).Name}', since the associated "
                    + $"file '{filename}' cannot be found.");

            using (TextReader reader = new StreamReader(filename))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Objects
                };

                return JsonConvert.DeserializeObject<List<T>>(reader.ReadToEnd(), settings);
            }
        }


        /// <summary>
        /// Deserializes an object (of type <typeparamref name="T"/>) from the given file. If no filename is given, a default one is
        /// generated from the type name. Note that the type of <typeparamref name="T"/> may also be a base class or an interface and that
        /// also private members are deserialized.
        /// </summary>
        /// <typeparam name="T">The type of the object to deserialize (or a base class or interface).</typeparam>
        /// <param name="filename">The name of the file to use or null if the default file for the given type shall be used.</param>
        public T DeserializeObject<T>(string filename = null)
        {
            if (filename == null)
                filename = getDefaultSerializationPath<List<T>>();

            if (!File.Exists(filename))
                throw new FileNotFoundException($"Cannot deserialize data for an object of type '{typeof(T).Name}', since the associated "
                    + $"file '{filename}' cannot be found.");

            using (TextReader reader = new StreamReader(filename))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Objects
                };

                return JsonConvert.DeserializeObject<T>(reader.ReadToEnd(), settings);
            }
        }


        /// <summary>
        /// Serializes the given object. If no filename is given, a default file name will be generated from the type name.
        /// Note that also private fields will be serialized.
        /// </summary>
        /// <typeparam name="T">The type of the object to serialize.</typeparam>
        /// <param name="toSerialize">The object which shall be serialized to the file.</param>
        /// <param name="filename">The name of the file to use or null if the default file for the given type shall be used.</param>
        /// <param name="overwriteExisting">If true, existing files will be overwritten, otherwise an exception will be thrown.</param>
        public void SerializeObject<T>(T toSerialize, string filename = null, bool overwriteExisting = true)
        {
            if (filename == null)
                filename = getDefaultSerializationPath<List<T>>();

            if (!overwriteExisting && File.Exists(filename))
                throw new Exception($"Cannot serialize the object of type '{typeof(T).Name}', since the file '{filename}' exists already.");

            using (TextWriter writer = new StreamWriter(filename))
            {
                string serialized = JsonConvert.SerializeObject(toSerialize, typeof(T), new JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented,
                    TypeNameHandling = TypeNameHandling.Objects
                });

                writer.Write(serialized);
            }
        }
    }
}
