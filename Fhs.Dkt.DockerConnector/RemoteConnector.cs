﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Fhs.Dkt.Types;
using Fhs.Dkt.Extensions;
using Fhs.Dkt.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Fhs.Dkt
{
    public static class RemoteConnector
    {
        static TcpClient client = null;
        static NetworkStream stream = null;
        static bool isShuttingDown = false;

        static byte[] header = new byte[RemoteConnectionSettings.HeaderByteLength];
        static byte[] buffer = new byte[8192];

        static ILogger logger = DependencyService.Get<ILogger>();

        static Object _readLock = new Object();
        static Object _writeLock = new Object();

        static Thread workerThread = null;

        private static ConcurrentQueue<RemoteInvocationInfo> sendQueue = new ConcurrentQueue<RemoteInvocationInfo>();

        private static void startAsyncSenderWorkerIfRequired()
        {
            connectToHostIfRequired();

            Console.WriteLine("Connector: Starting sender worker requested");
            if (workerThread == null)
            {
                workerThread = new Thread(delegate () { writerLoop(); });
                workerThread.Start();
            }
            else
            {
                Console.WriteLine("Connector: Sender worker seems to be already running");
            }
        }

        private static void connectToHostIfRequired()
        {
            string host = RemoteConnectionSettings.Host;
            int port = RemoteConnectionSettings.ServerPort;

            while (client == null || !client.Connected)
            {
                Console.WriteLine($"Connector: Connecting to host '{host}' and port '{port}' on thread {Thread.CurrentThread.ManagedThreadId}...");
                try
                {
                    client = new TcpClient(host, port);
                    client.ReceiveTimeout = RemoteConnectionSettings.MsTcpReceiveTimeout;
                    client.SendTimeout = RemoteConnectionSettings.MsTcpSendTimeout;

                    stream = client.GetStream();
                    stream.ReadTimeout = RemoteConnectionSettings.MsStreamReadTimeout;
                    stream.WriteTimeout = RemoteConnectionSettings.MsStreamWriteTimeout;

                    Console.WriteLine($"Connector: Connection established");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Connector: Connecting failed (Host up and running? Retry in {RemoteConnectionSettings.MsBetweenConnectionAttempts} " +
                        $"milliseconds). Details: {ex.Message}");
                    Thread.Sleep(RemoteConnectionSettings.MsBetweenConnectionAttempts);
                }
            }
        }

        public static Task<RemoteInvocationInfo> RemoteInvokeWithReturnAsync(/*string host, int ip, */ RemoteInvocationInfo info)
        {
            if (info.ReturnExpected == false)
            {
                string errMsg = $"Cannot execute, since the {nameof(RemoteInvocationInfo)} does not expect a return value " +
                    $"(use the overload of this method instead";
                Console.WriteLine(errMsg);
                throw new Exception(errMsg);
            }

            try
            {
                // Connect to the remote system if the connections has not been established yet
                startAsyncSenderWorkerIfRequired();
                // Add the data to be sent to the queue (will be sent by the writerWorker)
                sendQueue.Enqueue(info);
                return Task.Run(()=>readDirectResponse());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occurred during execution of {nameof(RemoteInvokeWithReturnAsync)}, see the exception for details: {ex}");
                throw;
            }
        }

        public static void RemoteInvokeAsync(/*string host, int ip,*/ RemoteInvocationInfo info)
        {
            if (info.ReturnExpected)
            {
                string errMsg = $"Cannot execute, since the {nameof(RemoteInvocationInfo)} does expect a return value (use the overload of this method instead";
                Console.WriteLine(errMsg);
                throw new Exception(errMsg);
            }

            try
            {
                // Connect to the remote system if the connections has not been established yet
                startAsyncSenderWorkerIfRequired();
                // Add the data to be sent to the queue (will be sent by the writerWorker)
                sendQueue.Enqueue(info);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occurred during execution of {nameof(RemoteInvokeAsync)}, see the exception for details: {ex}");
                throw;
            }
        }


        private static void writerLoop()
        {
            while (!isShuttingDown)
            {
                inWriteLock++;
                lock (_writeLock)
                {

                    Console.WriteLine($"Connector: Starting to wait for data to be sent on thread {Thread.CurrentThread.ManagedThreadId}...");

                    RemoteInvocationInfo info = null;
                    while (!sendQueue.TryDequeue(out info))
                    {
                        Thread.Sleep(RemoteConnectionSettings.MsBeforeDataAvailableChecks);
                    }

                    // finally got an entry from the dictionary at this point
                    string data = info.ToString();
                    try
                    {
                        byte[] payload = Encoding.Unicode.GetBytes(data);
                        header.WriteInt(0, payload.Length, RemoteConnectionSettings.HeaderBitLength);

                        if (info.ComponentName == "___custom___"
                            && info.TargetName == "___notify___"
                            && info.Params != null
                            && info.Params.Length > 0
                            && info.Params[0] is IMessage)
                        {
                            // A message is about to be sent
                            Console.WriteLine($"Connector: A message of type {info.ParamTypes[0]} is about to be sent to the remote...");
                        }
                        else if (info.ComponentName == "___custom___"
                            && info.TargetName == "___response___"
                            && info.Params != null
                            && info.Params.Length > 0)
                        {
                            Console.WriteLine($"Connector: A response is about to be sent to the remote (response code = {info.Id})");
                        }
                        else
                        {
                            // A real invocation request is about to be sent
                            Console.WriteLine($"Connector: Sending remote invocation request " +
                                $"(Target={info.ComponentName}.{info.TargetName}; " +
                                $"HeaderLg={RemoteConnectionSettings.HeaderByteLength} byte; PayloadLg={payload.Length} byte");
                        }

                        stream.Write(header, 0, RemoteConnectionSettings.HeaderByteLength);
                        stream.Write(payload, 0, payload.Length);
                        stream.Flush();

                        //Console.WriteLine(System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() + Thread.CurrentThread.Name);
                        Console.WriteLine("Connector: Remote invocation request sent");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Connector: Exception occurred during execution of {nameof(writerLoop)} in " +
                            $"{nameof(RemoteConnector)}, see the exception for details: {ex}");
                        throw;
                    }

                    // TODO: Is shutting down berücksichtigen
                }
                inWriteLock--;
            }
        }

        private static int _inReadLock = 0;
        private static int inReadLock
        {
            get => _inReadLock;

            set
            {
                _inReadLock = value;
                Console.WriteLine("##### In readlock " + _inReadLock + " #####");
            }
        }
        private static int _inWriteLock = 0;
        private static int inWriteLock
        {
            get => _inWriteLock;

            set
            {
                _inWriteLock = value;
                Console.WriteLine("##### In writeLock " + _inWriteLock + " #####");
            }
        }

        private static RemoteInvocationInfo readDirectResponse()
        {
            lock (_readLock)
            {
                Console.WriteLine("Connector: About to read from stream on Thread " + Thread.CurrentThread.ManagedThreadId);

                try
                {
                    //Console.WriteLine(System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() + Thread.CurrentThread.Name);
                    Console.WriteLine("Connector: Waiting for remote to answer the remote invocation request");
                    // wait for incoming data
                    while (!stream.DataAvailable)
                    {
                        Thread.Sleep(RemoteConnectionSettings.MsBeforeDataAvailableChecks);
                    }

                    // Read the first 4 byte, which contains the length of the data
                    int byteRead = stream.Read(header, 0, RemoteConnectionSettings.HeaderByteLength);

                    if (byteRead != RemoteConnectionSettings.HeaderByteLength)
                        throw new Exception("It seems that an invalid request was made (header length is wrong)");

                    // Using our new byte extension methods here for convenience
                    int lg = header.ReadInt(0, RemoteConnectionSettings.HeaderBitLength);

                    Console.WriteLine($"Connector: Data available, header tells length of {lg} bytes");

                    // Now read the full content of the received message
                    string partialResponse = "";
                    while (lg > 0)
                    {
                        byteRead = stream.Read(buffer, 0, Math.Min(buffer.Length, lg));
                        Console.WriteLine($"Connector: Received {byteRead}/{lg} Bytes");
                        partialResponse += buffer.ReadString(0, (ushort)byteRead);

                        lg -= byteRead;
                    }

                    return RemoteInvocationInfo.FromString(partialResponse);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception occurred during execution of {nameof(readDirectResponse)} in {nameof(RemoteConnector)}, see the exception for details: {ex}");
                    throw;
                }
            }
        }
    }
}
