﻿using System;
using Fhs.Dkt.Interfaces;
using System.Net;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Fhs.Dkt.Messaging;

[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.CloudBank))]

/// <summary>
/// Specifies the top level namespace of this application.
/// Created by Boris Brankovic and Gabriel Dax.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The class provides the stub for the CloudBank.
    /// Created by Boris Brankovic.
    /// </summary>
    public class CloudBank : IBank
    {
        /// <summary>
        /// Server url to the Azure funtkion CreateBank(...).
        /// </summary>
        const string serverUrl1 = "http://localhost:7071/api/CreateBank/?bankID=bank1";

        /// <summary>
        /// Server url to the Azure funtkion GetBankID(...).
        /// </summary>
        const string serverUrl2= "http://localhost:7071/api/GetBankID/";

        /// <summary>
        /// Server url to the Azure funtkion OpenAccount(...).
        /// </summary>
        const string serverUrl3 = "http://localhost:7071/api/OpenAccount/?playerID=";

        /// <summary>
        /// Server url to the Azure funtkion GetAccountBalance(...).
        /// </summary>
        const string serverUrl4 = "http://localhost:7071/api/GetAccountBalance/?playerID=";

        /// <summary>
        /// Http reqest handler.
        /// </summary>.
        WebRequest request = null;

        /// <summary>
        /// Http response handler.
        /// </summary>
        HttpWebResponse response = null;

        /// <summary>
        /// Stream reader for the response.
        /// </summary>
        StreamReader reader = null;
        string result = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CloudBank{T}"/> class.
        /// </summary>
        public CloudBank()
        {
            try
            {
                request = WebRequest.Create(serverUrl1);
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Error in Skeleton");

                reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd().Trim('"');
                this.Notify(new LogOnlyMessage("Bank created."));
                Console.WriteLine(result);
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                throw new Exception("Cloud-Server unreachable!");
            }
        }

        /// <summary>
        /// This method checks the bank account permission (Not Implemented).
        /// </summary>
        /// <param name="playerId">The unique id of the player.</param>
        /// <param name="key">Private bank key of the player.</param>
        /// <returns>A <see cref="NotImplementedException"/> Exception</returns>
        public bool CheckAccountPermission(Guid playerId, Guid key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that sends a request to the Cloud to deposit a spezific amount of money.
        /// </summary>
        /// <param name="playerId">The unique id of the player.</param>
        /// <param name="amount">Amount of money to deposit</param>
        /// <returns><c>true</c> if process was successful; otherwise, <c>false</c></returns>
        public bool DepositAmount(Guid playerId, double amount)
        {
            try
            {
                String newServerCall = "http://localhost:7071/api/DepositAmount/?playerID=" + playerId.ToString() + "&amount=" + amount.ToString();
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not deposit amount");

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');
                Console.WriteLine(result);


                JToken token = JObject.Parse(result);

                bool status = (bool)token.SelectToken("Status");
                double purchasingBalanceBefore = (double)token.SelectToken("PurchasingAccountBefore");
                double purchasingBalanceAfter = (double)token.SelectToken("PurchasingAccountAfter");
                double receivingBalanceBefore = (double)token.SelectToken("ReceivingAccountBefore");
                double receivingBalanceAfter = (double)token.SelectToken("ReceivingAccountAfter");

                this.Notify(new AccountBalanceChangedMessage(playerId, receivingBalanceBefore, receivingBalanceAfter));

                return status;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return false;
            }
        }

        /// <summary>
        /// Method that sends a request to the Cloud to deposit the "new round" (by passing the start field) amount of money to the player.
        /// </summary>
        /// <param name="playerId">The unique id of the player.</param>
        /// <returns><c>true</c> if process was successful; otherwise, <c>false</c></returns>
        public bool DepositNewRoundAmount(Guid playerId)
        {
            try
            {
                String newServerCall = "http://localhost:7071/api/DepositNewRoundAmount/?playerID=" + playerId.ToString();
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not deposit new amount");

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');
                Console.WriteLine(result);


                JToken token = JObject.Parse(result);

                bool status = (bool)token.SelectToken("Status");
                double purchasingBalanceBefore = (double)token.SelectToken("PurchasingAccountBefore");
                double purchasingBalanceAfter = (double)token.SelectToken("PurchasingAccountAfter");
                double receivingBalanceBefore = (double)token.SelectToken("ReceivingAccountBefore");
                double receivingBalanceAfter = (double)token.SelectToken("ReceivingAccountAfter");

                this.Notify(new AccountBalanceChangedMessage(playerId, receivingBalanceBefore, receivingBalanceAfter));

                return status;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return false;
            }
        }

        /// <summary>
        /// Method that sends a request to the Cloud to get the account balance of a player.
        /// </summary>
        /// <param name="playerId">The unique id of the player.</param>
        /// <param name="amount">Amount of money to deposit</param>
        /// <returns>The players account balance</returns>
        public double GetAccountBalance(Guid playerId, Guid key)
        {
            try
            {
                String newServerCall = "http://localhost:7071/api/GetAccountBalance/?playerID=" + playerId.ToString() + "&key=" + key.ToString();
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not get Account Balance");

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');
                Console.WriteLine(result);

                //TODO: GetAccountBalance -> conversion into right format (Precision 2)
                double balance = double.Parse(result, System.Globalization.CultureInfo.InvariantCulture);
                return balance;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return 1.0;
            }
        }

        /// <summary>
        /// Method that sends a request to the Cloud to get the bank account id.
        /// </summary>
        /// <returns>The bank account id.</returns>
        public Guid GetBankAccountId()
        {
            try
            {
                request = WebRequest.Create(serverUrl2 + "?bankID=bank1");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not get bank account id");

                reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd().Trim('"');

                Console.WriteLine(result);

                Guid id = Guid.Parse(result);
                return id;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return new Guid();
            }
        }

        /// <summary>
        /// Method to get the transaction list (Not Implemented).
        /// </summary>
        /// <returns>A <see cref="NotImplementedException"/> Exception</returns>
        public List<string> GetTransactionList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method that sends a request to the Cloud to get the wealthiest player.
        /// </summary>
        /// <returns>The id of the wealthiest player</returns>
        public Guid GetWealthiestPlayer()
        {
            Guid player = new Guid();
            try
            {
                String newServerCall = "http://localhost:7071/api/GetWealthiestPlayer";
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not get player id");

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');

                Console.WriteLine(result);

                player = Guid.Parse(result);

            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return player;
            }

            if (player == null)
            {
                throw new Exception("No player found");
            }

            return player;
        }

        /// <summary>
        /// Method that sends a request to the Cloud to make a bank transaction between bank and player.
        /// </summary>
        /// <param name="playerId">The unique id of the player.</param>
        /// <param name="key">Private bank key of the player.</param>
        /// <param name="amount">Amount of money to deposit</param>
        /// <returns><c>true</c> if process was successful; otherwise, <c>false</c></returns>
        public bool MakeBankTransaction(Guid playerId, Guid key, double amount)
        {
            try
            {
                String newServerCall = "http://localhost:7071/api/MakeBankTransaction/?playerID=" + playerId.ToString() + "&amount=" + amount + "&key=" + key.ToString();
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not make bank transaction");

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');

                Console.WriteLine(result);


                JToken token = JObject.Parse(result);

                bool status = (bool)token.SelectToken("Status");
                double purchasingBalanceBefore = (double)token.SelectToken("PurchasingAccountBefore");
                double purchasingBalanceAfter = (double)token.SelectToken("PurchasingAccountAfter");
                double receivingBalanceBefore = (double)token.SelectToken("ReceivingAccountBefore");
                double receivingBalanceAfter = (double)token.SelectToken("ReceivingAccountAfter");

                this.Notify(new AccountBalanceChangedMessage(playerId, purchasingBalanceBefore, purchasingBalanceAfter));

                return status;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return false;
            }
        }

        /// <summary>
        /// Method that sends a request to the Cloud to make a bank transaction between two players.
        /// </summary>
        /// <param name="playerId">The unique id of the player (sender).</param>
        /// <param name="recipientId">The unique id of the player (receiver).</param>
        /// <param name="key">Private bank key of the player.</param>
        /// <param name="amount">Amount of money to deposit</param>
        /// <returns><c>true</c> if process was successful; otherwise, <c>false</c></returns>
        public bool MakeTransaction(Guid playerId, Guid recipientId, Guid key, double amount)
        {
            try
            {
                String newServerCall = "http://localhost:7071/api/MakeTransaction/?playerID=" + playerId.ToString() + "&recepID=" + recipientId.ToString() + "&amount=" + amount + "&key=" + key.ToString();
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not make transaction");


                this.Notify(new LogOnlyMessage("Bank - Transaction: From " + playerId + " to " + recipientId + " for " +
                                                amount));


                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');
                Console.WriteLine(result);

                JToken token = JObject.Parse(result);

                bool status = (bool)token.SelectToken("Status");
                double purchasingBalanceBefore = (double)token.SelectToken("PurchasingAccountBefore");
                double purchasingBalanceAfter = (double)token.SelectToken("PurchasingAccountAfter");
                double receivingBalanceBefore = (double)token.SelectToken("ReceivingAccountBefore");
                double receivingBalanceAfter = (double)token.SelectToken("ReceivingAccountAfter");

                this.Notify(new AccountBalanceChangedMessage(playerId, purchasingBalanceBefore, purchasingBalanceAfter));
                this.Notify(new AccountBalanceChangedMessage(recipientId, receivingBalanceBefore, receivingBalanceAfter));

                return status;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return false;
            }
        }

        /// <summary>
        /// Method that sends a request to the Cloud to open a new bank account for a player.
        /// </summary>
        /// <param name="playerId">The unique id of the player (sender).</param>
        /// <returns>The unique id of the player.</returns>
        public Guid OpenAccount(Guid playerId)
        {
            try
            {
                String newServerCall = "http://localhost:7071/api/OpenAccount/?playerID=" + playerId.ToString();
                WebRequest request = WebRequest.Create(newServerCall);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Could not Open Account");

                StreamReader reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd().Trim('"');

                this.Notify(new LogOnlyMessage("Bank - Create account for " + playerId));

                Console.WriteLine(result);

                Guid g = Guid.Parse(result);
                return g;
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudBank)));
                return new Guid();
            }
        }
    }
}
