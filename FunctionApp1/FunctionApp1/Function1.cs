
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Skeleton - for the Components "CloudBank" and "CloudDice" in Fhs.Dkt
/// With the help of: 
/// https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
/// https://docs.microsoft.com/de-de/dotnet/api/microsoft.azure.documents.client.documentclient.replacedocumentasync?redirectedfrom=MSDN&view=azure-dotnet#overloads
/// Created by Boris Brankovic and Gabriel Dax.
/// </summary>
namespace FunctionApp1
{
    public static class Function1
    {
        /// <summary>
        /// Credentials for authentification with the CosmosDB on the azure-cloud 
        /// </summary>
        // static string endpoint = "https://dkt.documents.azure.com:443/";
        // static string authKey = "L7PM2OiPHvZnZvNduBtGLfE1DSg72jrNkTZ1ofNy1pqfFQO9RlACsXKjKdsdX4WvHxRTZo91rm5ByaN2rDZkvg==";

        /// <summary>
        /// Credentials for authentification with the local CosmosDB 
        /// </summary>
        static string endpoint = "https://localhost:8081/";
        static string authKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";

        //DocumentClient variable, important for creating a new Database, Collection or Document in the CosmosDB
        static DocumentClient client;
        
        //*****************************************************************************
        //Run-Functions for the CloudBank and CloudDice
        //*****************************************************************************

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("CreateBank")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function creates a new Database for the Bank and Player in the CosmosDB.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("CreateBank")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string bankID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "bankID", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            bankID = bankID ?? data?.bankID;

            HttpResponseMessage httpResponseMessage;

            if(bankID != null)
            {
                try
                {
                    var task = CreateBank(bankID);
                    task.Wait();
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in a bank name");
            }

            return httpResponseMessage;

        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("GetBankID")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function returns the specific ID as type "GUID" of the created Bank. 
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("GetBankID")]
        public static async Task<HttpResponseMessage> RunBankApp([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string bankID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "bankID", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            bankID = bankID ?? data?.bankID;

            HttpResponseMessage httpResponseMessage;

            if (bankID != null)
            {
                try
                {
                    var task = GetBankID(bankID);
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in a bank name");
            }

            return httpResponseMessage;

        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("GetBankAccount")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function returns the specific ID as type "GUID" of the created Bank. 
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("GetBankAccount")]
        public static async Task<HttpResponseMessage> RunGetBankAccount([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;
            
            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = GetBankID(playerID);
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in a bank name");
            }

            return httpResponseMessage;

        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("OpenAccount")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function opens a new Account for every new created Player in the Game. A new Document-entry in the CosmosDB represents one Bank-Account each.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("OpenAccount")]
        public static async Task<HttpResponseMessage> RunOpenAccount([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;

            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = OpenAccount(playerID);
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in a playerID");
            }

            return httpResponseMessage;

        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("MakeTransaction")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function performs a new Transaction. This function is called if a specific player wants to buy a property,
        /// has to pay rent or the player needs to perform some other kind of payment. 
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("MakeTransaction")]
        public static async Task<HttpResponseMessage> RunTransaction([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            string recepID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "recepID", true) == 0)
                .Value;

            string key = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "key", true) == 0)
                .Value;

           // Guid key2 = GetBankKeyFromPlayer(playerID);

            string amountStr = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "amount", true) == 0)
                .Value;
            
            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;
            recepID = recepID ?? data?.recepID;
            key = key ?? data?.key;
            amountStr = amountStr ?? data?.amountStr;

            double amount = double.Parse(amountStr);
            
            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = MakeTransaction(playerID, recepID, key, amount);
                    httpResponseMessage = task.Result;
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in required information!");
            }

            return httpResponseMessage;

        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("MakeBankTransaction")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function will be called, if a specific player has to make a payment to the bank.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("MakeBankTransaction")]
        public static async Task<HttpResponseMessage> RunBankTransaction([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            // Guid key = GetBankKeyFromPlayer(playerID);
            string key = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "key", true) == 0)
                .Value;

            string amountStr = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "amount", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;
            key = key ?? data?.key;
            amountStr = amountStr ?? data?.amountStr;

            double amount = double.Parse(amountStr);

            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = MakeBankTransaction(playerID, key, amount);
                    httpResponseMessage = task.Result;
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in required information!");
            }

            return httpResponseMessage;
        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("GetWealthiestPlayer")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function returns the most wealthiest player in the game.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("GetWealthiestPlayer")]
        public static async Task<HttpResponseMessage> RunWealthiestPlayer([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();
            
            HttpResponseMessage httpResponseMessage;

            try
            {
                var task = GetWealthiestPlayer();
                httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
            }
            catch
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
            }
            
            return httpResponseMessage;

        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("DepositAmount")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function deposits a certain amount in the account of a specific player.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("DepositAmount")]
        public static async Task<HttpResponseMessage> RunDepositAmount([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            string amountStr = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "amount", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;
            amountStr = amountStr ?? data?.amountStr;

            double amount = double.Parse(amountStr);

            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = DepositAmount(playerID, amount);
                    httpResponseMessage = task.Result;
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in required information!");
            }

            return httpResponseMessage;
        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("DepositNewRoundAmount")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function deposits the amount in the account of a specific player, if he beginns a new round in the game.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("DepositNewRoundAmount")]
        public static async Task<HttpResponseMessage> RunDepositNewRoundAmount([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;
            
            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = DepositNewRoundAmount(playerID);
                    httpResponseMessage = task.Result;
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in required information!");
            }

            return httpResponseMessage;
        }

        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("GetAccountBalance")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function returns the current balance of the account of a specific player.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("GetAccountBalance")]
        public static async Task<HttpResponseMessage> RunGetAccountBalance([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string playerID = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "playerID", true) == 0)
                .Value;

            string key = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "key", true) == 0)
                .Value;

            // Guid key = GetBankKeyFromPlayer(playerID);

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            playerID = playerID ?? data?.playerID;
            key = key ?? data?.key;

            HttpResponseMessage httpResponseMessage;

            if (playerID != null)
            {
                try
                {
                    var task = GetAccountBalance(playerID, key);
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
                }
                catch
                {
                    httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
                }
            }
            else
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Type in required information!");
            }

            return httpResponseMessage;
        }
        
        /// <summary>
        /// Run-Function as async Task to start a Azure-Function - [FunctionName("RollDice")] marks the function as Azure-Function.
        /// and to send a Task-Result as HttpResponse-Message back to the client. 
        /// In this case the Azure-Function rolls the dice once and returns the eye-sum.
        /// The Function has it's own URI and the client (Stub) can communicate with the Azure Function (Skeleton), to retrieve a
        /// required information for executing a specific task on the client-side.
        /// </summary>
        /// <param name="req">Parameter which contains a HTTP-Request Message.</param>
        /// <param name="log">Parameter required for Logging-Functionality.</param>
        /// <returns>This Run-Function returns a HTTP-Response Message with a certain result.</returns>
        [FunctionName("RollDice")]
        public static async Task<HttpResponseMessage> RunRollDice([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            HttpResponseMessage httpResponseMessage;

            try
            {
                var task = RollDice();
                httpResponseMessage = req.CreateResponse(HttpStatusCode.OK, task.Result);
            }
            catch
            {
                httpResponseMessage = req.CreateResponse(HttpStatusCode.BadRequest, "Error, game over");
            }

            return httpResponseMessage;
        }

        //*****************************************************************************
        //Async-Tasks for the Run-Functions
        //*****************************************************************************
        
        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task creates a random number between 1 and 6.
        /// </summary>
        /// <returns>Returns a random number between 1 and 6 as byte.</returns>
        static async Task<byte> RollDice()
        {
            client = new DocumentClient(new Uri(endpoint), authKey);

            Random rnd = new Random();
            int min = 1;
            int max = 6;

            //Tuple<byte, byte> result = new Tuple<byte, byte>((byte)rnd.Next(min, max + 1), (byte)rnd.Next(1, max + 1));
            byte result = (byte)rnd.Next(1, max + 1);

            return await Task.FromResult(result);
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task creates each a Database for the Bank and the Player in the ComsosDB.
        /// For the Bank it creates a Database and in addition the Collection and as well as the Document-entry. 
        /// For the Player it creates just the Database and the Collection.
        /// With the help of: 
        /// https://github.com/Azure/azure-documentdb-dotnet/blob/530c8d9cf7c99df7300246da05206c57ce654233/samples/code-samples/DatabaseManagement/Program.cs#L102
        /// </summary>
        /// <returns>Returns a string if the creation was successfull.</returns>
        static async Task<string> CreateBank(string bankID)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            Guid bankKey = Guid.NewGuid();
            Guid secretKey = Guid.NewGuid();

            //Get Database information
            Database database = client.CreateDatabaseQuery().Where(db => db.Id == "BankDB2").AsEnumerable().FirstOrDefault();

            //If database exists - delete old ones and create new databases
            //else create them if there is no existing database
            if (database != null)
            {
                await client.DeleteDatabaseAsync(UriFactory.CreateDatabaseUri("BankDB2"));
                await client.DeleteDatabaseAsync(UriFactory.CreateDatabaseUri("AccountDB"));

                await client.CreateDatabaseIfNotExistsAsync(new Database { Id = "BankDB2" });
                await client.CreateDatabaseIfNotExistsAsync(new Database { Id = "AccountDB" });
                await client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri("BankDB2"), new DocumentCollection { Id = "Collect1" });
            }
            else
            {
                await client.CreateDatabaseIfNotExistsAsync(new Database { Id = "BankDB2" });
                await client.CreateDatabaseIfNotExistsAsync(new Database { Id = "AccountDB" });
                await client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri("BankDB2"), new DocumentCollection { Id = "Collect1" });
            }

            Bank2 bank2 = new Bank2
            {
                Name = bankID,
                Id = bankKey,
                Key = secretKey
            };

            await CreateBankDocumentIfNotExists("BankDB2", "Collect1", bank2);

            return "Bank created successfully";
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task returns the ID of the created bank.
        /// </summary>
        /// <returns>Returns a the specific ID of the bank as string.</returns>
        static async Task<string> GetBankID(string bankID)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            
            return await Task.FromResult(ExecuteSimpleQuery("BankDB2", "Collect1"));
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task returns the ID of the wealthiest player in the game.
        /// </summary>
        /// <returns>Returns the ID of a specific player as string.</returns>
        static async Task<string> GetWealthiestPlayer()
        {
            client = new DocumentClient(new Uri(endpoint), authKey);

            return await Task.FromResult(GetPlayerWithHighestBalance());
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task creates for each player a new Document-Entry in the Player-Database.
        /// </summary>
        /// <returns>Returns the ID of a specific player as GUID.</returns>
        static async Task<Guid> OpenAccount(string playerID)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
           
            Guid secretKey = Guid.NewGuid();
            
            await client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri("AccountDB"), new DocumentCollection { Id = "PlayerCollection" });

            Player player = new Player
            {
                PlayerID = Guid.Parse(playerID),
                BankKey = secretKey,
                Balance = 1500
            };

            await CreatePlayerDocumentIfNotExists("AccountDB", "PlayerCollection", player);

            String key =  GetKeyFromPlayer(playerID);
            
            return Guid.Parse(key);
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task performs a transaction between a specific player and recepient. 
        /// </summary>
        /// <returns>Returns a Http-Response Message, which contains a JSON-Object, which is needed in the Stub "CloudBank".</returns>
        static async Task<HttpResponseMessage> MakeTransaction(string playerID, string recepientID, string key, double amount)
        {
            AccountStatus status = new AccountStatus();
            try
            {
                double purchasingBalanceBefore = GetAmountFromPlayer(playerID);
                double receivingBalanceBefore = GetAmountFromPlayer(recepientID);

                WithDrawMoney(playerID, key, amount);
                DepositMoney(amount, recepientID);

                double purchasingBalanceAfter = GetAmountFromPlayer(playerID);
                double receivingBalanceAfter = GetAmountFromPlayer(recepientID);

              
                status.Status = true;
                status.PurchasingAccountBefore = purchasingBalanceBefore;
                status.PurchasingAccountAfter = purchasingBalanceAfter;
                status.ReceivingAccountBefore = receivingBalanceBefore;
                status.ReceivingAccountAfter = receivingBalanceAfter;

                var json = JsonConvert.SerializeObject(status, Formatting.Indented);

                return await Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(json, Encoding.UTF8, "application/json")
                });
            }
            catch(Exception ex)
            {
                status.Status = false;

                var json = JsonConvert.SerializeObject(status, Formatting.Indented);

                return await Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(json, Encoding.UTF8, "application/json")
                });
            }
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task performs a Bank-Transaction between a specific player and the bank.
        /// </summary>
        /// <returns>Returns a Http-Response Message, which contains a JSON-Object, which is needed in the Stub "CloudBank".</returns>
        static async Task<HttpResponseMessage> MakeBankTransaction(string playerID, string key, double amount)
        {
            string bankkey = ExecuteSimpleQuery("BankDB2", "Collect1");
            return await MakeTransaction(playerID, bankkey, key, amount);
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task deposits a certain amount in the account of a specific player.
        /// </summary>
        /// <returns>Returns a Http-Response Message, which contains a JSON-Object, which is needed in the Stub "CloudBank".</returns>
        static async Task<HttpResponseMessage> DepositAmount(string playerID, double amount)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            string bankkey = ExecuteSimpleQuery("BankDB2", "Collect1");
            return await MakeTransaction(bankkey, playerID, GetKeyFromBank(), amount);
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task deposits a specific amount in the account of a player, if he has begun a new round in the game.
        /// </summary>
        /// <returns>Returns a Http-Response Message, which contains a JSON-Object, which is needed in the Stub "CloudBank".</returns>
        static async Task<HttpResponseMessage> DepositNewRoundAmount(string playerID)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            return await DepositAmount(playerID, 200.00);
        }

        /// <summary>
        /// Async Task which will be called from the Run-Functions.
        /// This specific Async-Task returns the current account-balance of a specific player.
        /// </summary>
        /// <returns>Returns a Http-Response Message, which contains a JSON-Object, which is needed in the Stub "CloudBank".</returns>
        static async Task<double> GetAccountBalance(string playerID, string key)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            return await Task.FromResult(GetAmountFromPlayer(playerID));
        }

        //*****************************************************************************
        //Implementation of some Bank-Logic
        //*****************************************************************************
        
        /// <summary>
        /// This function queries the Cosmos-DB and withdraws money from a specific player and updates the document-entry in the Player-DB.
        /// </summary>
        /// <param name="playerID">Specific Player-ID from whom the money should be withdrawn.</param>
        /// <param name="key">Bank-Key of the specific player to check if he is permitted to withdraw.</param>
        /// <param name="amount">Specific amount which should be withdrawn from the account.</param>
        static async void WithDrawMoney(string playerID, string key, double amount)
        {

            client = new DocumentClient(new Uri(endpoint), authKey);

            string bankID = ExecuteSimpleQuery("BankDB2", "Collect1");

            if (playerID.Equals(bankID))
            {
                //Withdraw not permitted
                return;
            }
            else if (!(GetKeyFromPlayer(playerID).Equals(key)))
            {
                //Withdraw not permitted
                return;
            }

            if (!(amount > 0)) return;
          
            if(GetAmountFromPlayer(playerID) - amount < 0)
            {
                //Withdraw not permitted
                return;
            }
            
            double newAmount = GetAmountFromPlayer(playerID);
            newAmount -= amount;

            //Update a existing document-entry in the CosmosDB
            //With the help of: https://docs.microsoft.com/en-us/dotnet/api/microsoft.azure.documents.client.documentclient.replacedocumentasync?view=azure-dotnet
            Document doc = client.CreateDocumentQuery<Document>(UriFactory.CreateDocumentCollectionUri("AccountDB", "PlayerCollection"))
                        .Where(r => r.Id == playerID)
                        .AsEnumerable()
                        .SingleOrDefault();

            //Dynamically cast of the document to get required information
            Player player = (dynamic)doc;

            //Update a ceratin property in the document
            player.Balance = newAmount;

            //Update the existing document in the Player-DB
            Document updated = await client.ReplaceDocumentAsync(doc.SelfLink, player);
        }

        /// <summary>
        /// Function to deposit a certain amount of money in a specific player-account.
        /// </summary>
        /// <param name="amount">Specific amount to deposit as double.</param>
        /// <param name="playerID">Specific playerID as string.</param>
        static async void DepositMoney(double amount, string playerID)
        {
            client = new DocumentClient(new Uri(endpoint), authKey);
            double newAmount = 0;

            string bankID = ExecuteSimpleQuery("BankDB2", "Collect1");

            if (playerID.Equals(bankID))
            {
                //Withdraw not permitted
                return;
            }

            if (amount > 0)
            {
                newAmount = GetAmountFromPlayer(playerID) + amount;
            }

            //Update a existing document-entry in the CosmosDB
            //With the help of: https://docs.microsoft.com/en-us/dotnet/api/microsoft.azure.documents.client.documentclient.replacedocumentasync?view=azure-dotnet
            Document doc = client.CreateDocumentQuery<Document>(UriFactory.CreateDocumentCollectionUri("AccountDB", "PlayerCollection"))
                     .Where(r => r.Id == playerID)
                     .AsEnumerable()
                     .SingleOrDefault();
            
            //Dynamically cast of the document to get required information
            Player player = (dynamic)doc;

            //Update a ceratin property in the document
            player.Balance = newAmount;

            //Update the existing document in the Player-DB
            Document updated = await client.ReplaceDocumentAsync(doc.SelfLink, player);
        }

        /// <summary>
        /// Function to deposit a specific amount to a player-account if he begins a new round in the game.
        /// </summary>
        /// <param name="playerID">Specific player-ID as string.</param>
        static void DepositNewRound(string playerID)
        {
            DepositMoney(200, playerID);
        }

        /// <summary>
        /// Function which returns the ID of the wealthiest player in the Player-DB.
        /// </summary>
        /// <returns>The ID of the the player which hast the most money as string.</returns>
        static string GetPlayerWithHighestBalance()
        {
            client = new DocumentClient(new Uri(endpoint), authKey);

            //With the help of:
            //https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
            FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };
            
            IQueryable<Player> playerQuery = client.CreateDocumentQuery<Player>(
            UriFactory.CreateDocumentCollectionUri("AccountDB", "PlayerCollection"),
            "SELECT * FROM PlayerCollection as c", queryOptions);

            double amount = 0;
            double maxAmount = 0;
            Guid playerID = new Guid();

            foreach (Player p in playerQuery)
            {
                amount = p.Balance;
                if(amount > maxAmount)
                {
                    maxAmount = amount;
                    playerID = p.PlayerID;
                }
            }

            return playerID.ToString();
        }

        /// <summary>
        /// Function which returns the secret bank key of a specific player.
        /// </summary>
        /// <param name="playerID">Specific playerID as string.</param>
        /// <returns>Returns the secret bank key of a specific player as string.</returns>
        static string GetKeyFromPlayer(string playerID)
        {
            //With the help of:
            //https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
            FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };

            client = new DocumentClient(new Uri(endpoint), authKey);

            IQueryable<Player> playerQuery = client.CreateDocumentQuery<Player>(
            UriFactory.CreateDocumentCollectionUri("AccountDB", "PlayerCollection"),
            "SELECT * FROM PlayerCollection c where c.id='" + playerID + "'", queryOptions);

            string key = null;

            foreach (Player p in playerQuery)
            {
                key = p.BankKey.ToString();
            }

            return key;
        }

        /// <summary>
        /// Function which returns the specific bank key.
        /// </summary>
        /// <returns>Returns the specific bank-key as string.</returns>
        static string GetKeyFromBank()
        {
            //With the help of:
            //https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
            client = new DocumentClient(new Uri(endpoint), authKey);
            FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };

            IQueryable<Bank2> bankQuery = client.CreateDocumentQuery<Bank2>(
            UriFactory.CreateDocumentCollectionUri("BankDB2", "Collect1"),
            "SELECT * FROM Collect1 as c", queryOptions);

            string key = null;

            foreach (Bank2 b in bankQuery)
            {
                key = b.Key.ToString();
            }

            return key;
        }

        /// <summary>
        /// Function which returns the current balance of a specific player.
        /// </summary>
        /// <param name="playerID">Specific Player-ID as string.</param>
        /// <returns>Returns the current account-balance of a specific player as double.</returns>
        static double GetAmountFromPlayer(string playerID)
        {

            string bankID = ExecuteSimpleQuery("BankDB2", "Collect1");

            if (playerID.Equals(bankID))
            {
                //Withdraw not permitted
                return double.NaN;
            }

            //With the help of:
            //https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
            FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };

            IQueryable<Player> playerQuery = client.CreateDocumentQuery<Player>(
            UriFactory.CreateDocumentCollectionUri("AccountDB", "PlayerCollection"),
            "SELECT * FROM PlayerCollection c where c.id='" + playerID + "'", queryOptions);

            double amount = 0;

            foreach (Player p in playerQuery)
            {
                amount = p.Balance;
            }
            return amount;
        }

        /// <summary>
        /// Function which returns the specific bank-key of a certain player.
        /// </summary>
        /// <param name="playerID">Specific player-ID as string.</param>
        /// <returns>Returns the bank-key of a specific player as string.</returns>
        static Guid GetBankKeyFromPlayer(string playerID)
        {

            client = new DocumentClient(new Uri(endpoint), authKey);

            //With the help of:
            //https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
            FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };

            IQueryable<Player> playerQuery = client.CreateDocumentQuery<Player>(
            UriFactory.CreateDocumentCollectionUri("AccountDB", "PlayerCollection"),
            "SELECT * FROM PlayerCollection c where c.id='" + playerID + "'", queryOptions);

            Guid key = new Guid();

            foreach (Player p in playerQuery)
            {
                key = p.BankKey;
            }
            return key;
        }

        /// <summary>
        /// Async-Task which is important to Replace an existing Document-entry in the PlayerDB.
        /// With the help of: https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
        /// </summary>
        /// <param name="databaseName">Name of the Database, which contains the specific document to be replaced/updated.</param>
        /// <param name="collectionName">Name of the Collection, which contains the specific document to be replaced/updated.</param>
        /// <param name="playerID">Specific Player-ID, which is assigned to the document i.e. the document which should be replaced contains this certain ID.</param>
        /// <returns></returns>
        static async Task ReplacePlayerDocument(string databaseName, string collectionName, string playerID)
        {
            await client.ReplaceDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), playerID);
        }

        /// <summary>
        /// Async-Task to create the Document-entry in the BankDB- for the bank.
        /// With the help of: https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
        /// </summary>
        /// <param name="databaseName">Name of the Database, where the document should be created.</param>
        /// <param name="collectionName">Name of the Collection, where the document should be created.</param>
        /// <param name="bank">Name of the BankID i.e. the ID of the document which will be created.</param>
        /// <returns></returns>
        static async Task CreateBankDocumentIfNotExists(string databaseName, string collectionName, Bank2 bank)
        {
            try
            {
                await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(databaseName, collectionName, bank.Name));
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), bank);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Async-Task to create the Document-entry in the PlayerDB- for a player.
        /// With the help of: https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
        /// </summary>
        /// <param name="databaseName">Name of the Database, where the document should be created.</param>
        /// <param name="collectionName">Name of the Collection, where the document should be created.</param>
        /// <param name="player">PlayerID i.e. the ID of the document which will be created.</param>
        /// <returns></returns>
        static async Task CreatePlayerDocumentIfNotExists(string databaseName, string collectionName, Player player)
        {
            try
            {
                await client.ReadDocumentAsync(UriFactory.CreateDocumentUri(databaseName, collectionName, player.PlayerID.ToString()));
            }
            catch (DocumentClientException de)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), player);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// JSON-Object for the Bank-Document.
        /// Contains all neccessary properties for the Bank itself.
        /// </summary>
        public class Bank2
        {
            [JsonProperty(PropertyName = "id")]
            public string Name { get; set; }
            public Guid Id { get; set; }
            public Guid Key { get; set; }
        }

        /// <summary>
        /// JSON-Object for the Player-Document.
        /// Contains all neccessary properties for each Player.
        /// </summary>
        public class Player
        {
            [JsonProperty(PropertyName = "id")]
            public Guid PlayerID { get; set; }
            public Guid BankKey { get; set; }
            public double Balance { get; set; } 
        }

        /// <summary>
        /// JSON-Object for the Stub- "CloudBank".
        /// Contains all neccessary properties for the View and in addition for the Azure-Function "MakeTransaction".
        /// </summary>
        public class AccountStatus
        {
            public bool Status { get; set; }
            public double PurchasingAccountBefore { get; set; }
            public double PurchasingAccountAfter { get; set; }
            public double ReceivingAccountBefore { get; set; }
            public double ReceivingAccountAfter { get; set; }
            public string ExceptionMessage { get; set; }
        }

        /// <summary>
        /// Function to execute a SQL-Query in the CosmosDB.
        /// In this case the SQL-Query returns essential information of the Bank-Document i.e. the Bank-ID.
        /// With the help of: https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-get-started#CreateColl
        /// </summary>
        /// <param name="databaseName">Name of the Database, which should be queried.</param>
        /// <param name="collectionName">Name of the Collection, which contains the required Document for the query.</param>
        /// <returns>Returns the Bank-ID as string.</returns>
        static string ExecuteSimpleQuery(string databaseName, string collectionName)
        {
            // Set some common query options
            FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };

            // Now execute the same query via direct SQL
            IQueryable<Bank2> bankQueryInSql = client.CreateDocumentQuery<Bank2>(
                    UriFactory.CreateDocumentCollectionUri(databaseName, collectionName),
                    "SELECT bank1.Id FROM bank1 where bank1.id = 'bank1'",
                    queryOptions);

            string id = null;
            Console.WriteLine("Running direct SQL query...");
            foreach (Bank2 bank in bankQueryInSql)
            {
                id = bank.Id.ToString();
                Console.WriteLine("\tRead {0}", bank);
            }
           return id;
        }
        
    }
}