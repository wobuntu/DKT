﻿using Newtonsoft.Json;
using System;

namespace FunctionApp1
{
    internal class Bank
    {
        [JsonProperty(PropertyName = "id")]
        public string name;
        public Guid id;
        public Guid key;

        public Bank(string name, Guid id, Guid key)
        {
            this.name = name;
            this.id = id;
            this.key = key;
        }
    }
}