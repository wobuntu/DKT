FROM mono:latest

# MAINTAINER Jo Shields <jo.shields@xamarin.com>
# MAINTAINER Alexander Köplinger <alkpli@microsoft.com>

RUN apt-get update \
  && apt-get install -y binutils curl mono-devel ca-certificates-mono fsharp mono-vbnc nuget referenceassemblies-pcl \
  && apt-get install -y iputils-ping net-tools \ 
    && rm -rf /var/lib/apt/lists/* /tmp/*


ADD . /src
WORKDIR /src

RUN mono --version
RUN nuget restore

RUN msbuild Fhs.Dkt.sln /p:Configuration=docker

# RUN mono Fhs.Dkt.DockerLauncher/bin/docker/Fhs.Dkt.DockerLauncher.exe

RUN chmod +x docker-startup.sh

RUN cp -R ./Data/ ./Fhs.Dkt.DockerLauncher/bin/docker/

ENTRYPOINT ["mono"]
CMD ["Fhs.Dkt.DockerLauncher/bin/docker/Fhs.Dkt.DockerLauncher.exe"]
