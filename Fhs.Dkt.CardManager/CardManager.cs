﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Extensions;
using Fhs.Dkt.Messaging;

[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.CardManager))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// CardManager handles Bank- and RiskCard decks and provides methods to draw from those decks
    /// Created by Lukas Brugger.
    /// </summary>
    public class CardManager : ICardManager
    {
        /// <summary>
        /// CardManager constructor reads card data from json files, adds them to list representing the card decks
        /// and shuffles these.
        /// Created by Lukas Brugger.
        /// </summary>
        public CardManager()
        {
            // Deserialize cards
            IDataManager dataMgr = DependencyService.Get<IDataManager>();
            this.bankCards = dataMgr.DeserializeList<BankCard>(System.IO.Path.Combine("Data", "BankCards.json"));
            this.riskCards = dataMgr.DeserializeList<RiskCard>(System.IO.Path.Combine("Data", "RiskCards.json"));

            // Shuffle cards
            this.bankCards.Shuffle();
            this.riskCards.Shuffle();

            // Limit cards to 15
            if (this.bankCards.Count > 15)
            {
                this.bankCards.RemoveRange(14, this.bankCards.Count - 15);
            }

            if (this.riskCards.Count > 15)
            {
                this.riskCards.RemoveRange(14, this.riskCards.Count - 15);
            }

            // this.Notify(new LogOnlyMessage("CardManager created."));
        }

        private List<BankCard> bankCards = null;
        private List<RiskCard> riskCards = null;
        private int bankCardIndex = 0;
        private int riskCardIndex = 0;
        private HashSet<Guid> lockedCards = new HashSet<Guid>();

        /// <summary>
        /// Method that draws a card from one of the decks based on the CardType passed to the function 
        /// and returns the drawn card
        /// </summary>
        /// <param name="cardType">Type of the card to be drawn, based on enum CardType</param>
        /// <param name="playerId">Guid of player who draws the card</param>
        public CardBase DrawCard(CardType cardType, Guid playerId)
        {
            if(cardType == CardType.BankCard)
            {
                if (bankCardIndex == this.bankCards.Count)
                {
                    this.bankCards.Shuffle();
                    bankCardIndex = 0;
                }

                if (this.bankCards[bankCardIndex].OwnedByPlayer != new Guid()) // new Guid delivers 000....000
                {
                    bankCardIndex += 1;
                }

                return this.bankCards[bankCardIndex++].GetClone();
            }
            else if(cardType == CardType.RiscCard)
            {
                if (riskCardIndex == this.riskCards.Count)
                {
                    this.riskCards.Shuffle();
                    riskCardIndex = 0;
                }

                if(this.riskCards[riskCardIndex].Action.ActionType == CardActionType.GetOutOfJail)
                {
                    if (lockedCards.Contains(playerId))
                    {
                        riskCardIndex += 1;
                        if (riskCardIndex == this.riskCards.Count)
                        {
                            this.riskCards.Shuffle();
                            riskCardIndex = 0;
                        }

                    }
                    else
                    {
                        lockedCards.Add(playerId);
                    }
                }

                return this.riskCards[riskCardIndex++].GetClone();
            }

            throw new ArgumentException();
            
        }

        /// <summary>
        /// Helper method to draw card of the type "BankCard"
        /// Calls DrawCard with the cardType BankCard passed directly
        /// </summary>
        /// <param name="playerId">Guid of player who draws the card</param>
        public BankCard DrawBankCard(Guid playerId)
        {
            // The method returns already a clone, no need to clone twice
            return (BankCard)DrawCard(CardType.BankCard, playerId);
        }

        /// <summary>
        /// Helper method to draw card of the type "RiskCard"
        /// Calls DrawCard with the cardType RiscCard passed directly
        /// </summary>
        /// <param name="playerId">Guid of player who draws the card</param>
        public RiskCard DrawRiskCard(Guid playerId)
        {
            // The method returns already a clone, no need to clone twice
            return (RiskCard)DrawCard(CardType.RiscCard, playerId);
        }

        /// <summary>
        /// Method to check if a player currently holds the "Get Free From Jail" card
        /// </summary>
        /// <param name="playerId">Guid of player to check</param>
        public bool PlayerHasJailCard(Guid playerId)
        {
            return lockedCards.Contains(playerId);
        }

        /// <summary>
        /// Method to remove a card from the "lockedCards" list
        /// This is used when a player uses a "Get Free From Jail" Card, so it is put back into the stack
        /// to be drawn again
        /// </summary>
        /// <param name="playerId">Guid of player in locked cards list</param>
        public void UnlockCard(Guid playerId)
        {
            if(!lockedCards.Contains(playerId))
            {
                throw new ArgumentException();
            }

            lockedCards.Remove(playerId);
        }
    }
}
