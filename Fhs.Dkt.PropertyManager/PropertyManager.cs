﻿// CREATED BY BORIS BRANKOVIC
using System;
using System.Collections.Generic;
using System.Linq;

using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Messaging;

[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.PropertyManager))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The component "PropertyManager" manages all required properties for the game. 
    /// It holds the very important "ownerships"-list, where every player is asigned to its belongings, after purchasing it.
    /// Also other particulary important methods e.g. checking if the current player is allowed to buy a property/house/hotel, are part of this component.
    /// Created by Boris Brankovic
    /// </summary>
    public class PropertyManager : IPropertyManager
    {
        /** Private variables of the class PropertyManager
         * ownerships -> Dictionary with all ownership-relations
         * properties -> List of all Properties, which are defined in a Json-File
         * housingList -> List of all Properties of the type HousingProperty
         * bank -> Reference on the Interfance "Bank" to get the required BankID for initiliazing all properties at the beginning of the game
         * */
        private Dictionary<Guid, List<byte>> ownerships = new Dictionary<Guid, List<byte>>();
        private List<PropertyBase> properties = null;
        private List<HousingProperty> housingList = null;
        private IBank bank = DependencyService.Get<IBank>();

        /// <summary>
        /// Constructor of the class "PropertyMangager"
        /// Calls the method "InitProperties()", on creation. This method initializes all properties with the Bank-ID. 
        /// </summary>
        public PropertyManager()
        {
            InitProperties(); //initialize Bank with IDs

            this.Notify(new LogOnlyMessage("PropertyManager created."));
        }

        /// <summary>
        /// The method "GetClone" returns the cloned property-objects based on the propertyID. 
        /// This method is called by the component "PlayerManager" everytime, a property is required by the view e.g. if a property is bought, 
        /// or if the property is affected by changes i.e. a house/hotel is bought. Therefor it is important to clone the original objects, because the view gets only copies of them 
        /// and the orignal ones stay at the component itself. 
        /// </summary>
        /// <param name="propertyId">By handing over this parameter, the property with the given ID will be cloned</param>
        /// <returns>The method returns the cloned object, based on the given property ID</returns> //--> Reference to the Clone Method itself
        public PropertyBase GetClone(byte propertyId)
        {
            PropertyBase property = this.properties.Where((prop) => prop.ID == propertyId).FirstOrDefault();
            if (property == null)
                return null;
            
            return property.Clone();
        }

        /// <summary>
        /// This method returns all TraffficLineProperties as a list.
        /// </summary>
        /// <returns>The return value is a list which contains all TrafficLineProperties</returns>
        private List<TrafficLineProperty> GetTrafficLineProperty()
        {
            return this.properties.OfType<TrafficLineProperty>().ToList();
        }

        /// <summary>
        /// This method returns all HousingProperties as a list.
        /// </summary>
        /// <returns>The return value is a list which contains all HousingProperties</returns>
        private List<HousingProperty> GetHousingProperties()
        {
            return this.properties.OfType<HousingProperty>().ToList();
        }

        /// <summary>
        /// This method returns all CompanyProperties as a list.
        /// </summary>
        /// <returns>The return value is a list which contains all CompanyProperties</returns>
        private List<CompanyProperty> GetCompanyProperty()
        {
            return this.properties.OfType<CompanyProperty>().ToList();
        }

        /// <summary>
        /// The method "GetNameOfProp" returns the name of a specific HousingProperty, by checking the given ID.
        /// </summary>
        /// <param name="id">This parameter specifies a certain ID in the Properties. Each ID is connected to a Property.</param>
        /// <returns>The return value is a string, which represents a specific Property of the type "HousingProperty".</returns>
        public string GetNameOfProp(int id)
        {
            housingList = this.properties.OfType<HousingProperty>().ToList();
          
            foreach(var info in housingList)
            {
                if (id.Equals(info.ID))
                    return info.Name;
            }

            return null;
        }

        /// <summary>
        /// GetCityOfProp returns the name of a certain HousingProperty. 
        /// </summary>
        /// <param name="id">The name of the city is found, based on the given ID</param>
        /// <returns>The return value is of the type "City",  which contains all the Citys specified in a Json-File. If no city is found -> null will be returned</returns>
        public City? GetCityOfProp(int id)
        {
            housingList = this.properties.OfType<HousingProperty>().ToList();
            
            foreach (var info in housingList)
            {
                if (id.Equals(info.ID))
                    return info.City;
            }

            return null;
        }


      
        /// <summary>
        /// Method to initialize all Properties with the BankID at the beginning of the game.
        /// This functionality is important, because the bank is the onwer of all properties at the beginning of the game.
        /// </summary>
        private void InitProperties()
        {
            // Deserialize properties
            IDataManager dataMgr = DependencyService.Get<IDataManager>();
            this.properties = dataMgr.DeserializeList<PropertyBase>(System.IO.Path.Combine("Data", "Properties.json"));

            foreach (var prop in properties.OfType<HousingProperty>())
                prop.PropertyChanged += (changedProp, description) => this.Notify(new PropertyChangedMessage(changedProp, description));

            List<byte> tempList = new List<byte>();
            int i = 0;

            Guid ownerID = bank.GetBankAccountId();

            foreach (var x in properties)
            {
                tempList.Add(x.ID);
                if(!ownerships.ContainsKey(ownerID))
                {
                    //add new element
                    ownerships.Add(ownerID, new List<byte>());
                }
                ownerships[ownerID].Add(tempList[i]);
                i++;
            }
        }

      
        /// <summary>
        /// AssignPlayerToProp assigns a property to a specific player. 
        /// This is done by checking the Ownership-Dictionary. If the player is not in the List a new entry will be made
        /// and the "ID" of the Property will be assigned to the Players-ID. 
        /// </summary>
        /// <param name="ownerID">The specific ID, which each player gets on creation i.e. when the game starts.</param>
        /// <param name="propertyID">A certain ID, which is connected to a property.</param>
        public void AssignPlayerToProp(Guid ownerID, byte propertyID)
        {
            List<byte> tempList = new List<byte>();
            tempList.Add(propertyID);

            //check if PlayerID is not in the ownership list
            if(!ownerships.ContainsKey(ownerID))
            {
                ownerships.Add(ownerID, new List<byte>());
            }
            ownerships[ownerID].Add(tempList[0]);

          //  return ownerships;
        }


        /// <summary>
        /// Remove a property from a specific player.
        /// This is done by checking the Ownership-Dictionary - if the given "ownerID" contains the property which should be removed,
        /// then exactly this property will be immediately removed from the Owner.
        /// </summary>
        /// <param name="ownerID">The ownerID specifies the Player to whom the property belongs to.</param>
        /// <param name="propertyID">The propertyID reprents the property, which should be removed.</param>
        /// <returns>The method returns a boolean-"true", if the property was successfully removed, otherwise a boolean-"false".</returns>
        public bool RemovePropFromOwner(Guid ownerID, byte propertyID)
        {
            foreach(var x in ownerships.Where((i) => i.Value.Contains(propertyID)).ToList())
            {
                ownerships[ownerID].Remove(propertyID);
                return true;
            }
            return false;
        }

        /// <summary>
        /// GetNumberOwnedProperties checks how many properties a certian has in his possession. 
        /// </summary>
        /// <param name="playerId">The specific Player-ID which is important for checking the number of properties.</param>
        /// <returns>The number (as byte) of all properties, which belong to specific Player.</returns>
        public byte GetNumberOwnedProperties(Guid playerId)
        {
            if (!ownerships.ContainsKey(playerId))
                return 0;

            return (byte)ownerships[playerId].Count;
        }

        /// <summary>
        /// GetNumberOwnedProperties checks how many properties of a specific type
        /// e.g. HousingProperty, TrafficLineProperty or CompanyProperty a certian has in his possession. 
        /// </summary>
        /// <param name="playerId">The specific Player-ID which is important for checking the number of properties.</param>
        /// <param name="filter">The filter has to be handed over to the method to specify a certain property i.e. HousingProperty, TrafficLineProperty, CompanyProperty.</param>
        /// <returns>The number (as byte) of a specific property-type, which belongs to a certain player.</returns>
        public byte GetNumberOwnedProperties(Guid playerId, PropertyType filter)
        {
            if (!ownerships.ContainsKey(playerId))
                // No properties owned by the player
                return 0;

            byte cnt = 0;
            foreach (PropertyBase p in this.properties)
            {
                if (ownerships[playerId].Contains(p.ID) && p.PropertyType == filter)
                    cnt++;
            }

            return cnt;
        }

        /// <summary>
        /// GetownedProperties returns a IReadOnlyList of all properties, which are assigned to a certian player.
        /// The properties will only be represent as IDs of the type byte.
        /// </summary>
        /// <param name="playerID">The specific Player-ID for checking the owned properties in the ownership-dictionary.</param>
        /// <returns>Returns a IReadOnlyList with the PropertyIDs, assigned to a certain player.</returns>
        public IReadOnlyList<byte> GetOwnedProperties(Guid playerID)
        {
            List<byte> clone = new List<byte>();

            if (!ownerships.ContainsKey(playerID))
                return clone; // empty list

            foreach (byte propId in ownerships[playerID])
                clone.Add(propId);

            return clone;
        }

        /// <summary>
        /// GetNumberOwnedHouses checks how many houses a certain player has bought in total.
        /// </summary>
        /// <param name="playerId">The specific player ID, for checking which property is assigned to it.</param>
        /// <returns>The total number (in byte) of all houses from a certain Player</returns>
        public byte GetNumberOwnedHouses(Guid playerId)
        {
            if (!ownerships.ContainsKey(playerId))
                // player has no properties at all, therefore no hotels/houses too
                return 0;

            List<byte> playerPropsIds = ownerships[playerId];

            int numHouses = 0;
            foreach (byte curPropId in playerPropsIds)
            {
                PropertyBase curProp = this.properties.Where((x) => x.ID == curPropId).FirstOrDefault();
                if (curProp != null && curProp is HousingProperty curHousingProp)
                {
                    numHouses += curHousingProp.NumHouses;
                }
            }

            return (byte)numHouses;
        }

        /// <summary>
        /// GetNumberOwnedHotels checks how many hotels a certain player has bought in total.
        /// </summary>
        /// <param name="playerId">The specific player ID, for checking which property is assigned to it.</param>
        /// <returns>The total number (in byte) of all hotels from a certain Player</returns>
        public byte GetNumberOwnedHotels(Guid playerId)
        {
            if (!ownerships.ContainsKey(playerId))
                // player has no properties at all, therefore no hotels/houses too
                return 0;

            List<byte> playerPropsIds = ownerships[playerId];

            int numHotels = 0;
            foreach (byte curPropId in playerPropsIds)
            {
                PropertyBase curProp = this.properties.Where((x) => x.ID == curPropId).FirstOrDefault();
                if (curProp != null && curProp is HousingProperty curHousingProp)
                {
                    numHotels += curHousingProp.NumHotels;
                }
            }

            return (byte)numHotels;
        }
        
        /// <summary>
        /// This method is only for testing purposes. It adds a specified number of Houses or Hotels to a certain player.
        /// </summary>
        /// <param name="playerID">Specific Player ID</param>
        /// <returns>Returns a boolean-"True" if the house/hotel was successfully added, otherwise boolean-"False".</returns>
        public bool AddHouse(Guid playerID)
        {

            var x = ownerships.Where((b) => b.Key == playerID).ToList();

            foreach(var i in GetHousingProperties())
            {
                if (x[0].Value.Contains(i.ID))
                {
                    i.NumHouses = 3;
                    //i.NumHotels = 1;
                }
            }
            return true;
        }

        /// <summary>
        /// Gets the relative ownership of a certain player in connection to a property ID,
        /// i.e. it checks whether the property can be bought by the current Player, if the property belongs to the current player, 
        /// if the bank is owner of the property, or if the property belongs to another player.
        /// This functionality is decisve for the game logic, it makes it possible to decide which step will be executed next. 
        /// </summary>
        /// <param name="playerId">The ID of the current player.</param>
        /// <param name="propertyID">The ID of the property, where the player currently is located in the game.</param>
        /// <returns>Returns an Enum of Relative-Ownerships</returns> <see cref="OwnerShip"/>
        public OwnerShip GetRelativeOwnership(Guid playerId, byte propertyID)
        {
            var property = properties.Where((prop) => prop.ID == propertyID).FirstOrDefault();
            if (property == null)
                // not contained in the set of loaded properties (must equal a risk, bank or special field)
                return OwnerShip.PropertyNotBuyable;

            if (ownerships.ContainsKey(playerId) && ownerships[playerId].Contains(propertyID))
                return OwnerShip.OwnerIsPlayer;
           
            if (ownerships.ContainsKey(bank.GetBankAccountId()) && ownerships[bank.GetBankAccountId()].Contains(propertyID))
                return OwnerShip.OwnerIsBank;

            return OwnerShip.OwnerIsOtherPlayer;
        }

        /// <summary>
        /// This method gets the owner of an specific property, by cheking the ownership of a given property ID.
        /// </summary>
        /// <param name="propertyID">Specific property ID, which is assigned to a certain player.</param>
        /// <returns>Returns the ID of the owner (type Guid), if no owner is specified then the return value is null.</returns>
        public Guid? GetOwnerOfProperty(byte propertyID)
        {
            foreach (var owner in this.ownerships.Keys)
            {
                if (ownerships[owner].Contains(propertyID))
                {
                    return owner;
                }
            }

            return null;
        }

        /// <summary>
        /// This method determines whether the current player is the owner of a certain property.
        /// The verification is done by checking if the ownership-dictionary contains the given player ID
        /// --> if this step is true then the further verification is done by checking whether this player contains the given property ID. 
        /// </summary>
        /// <param name="playerId">The ID of a specific player. Important for the verification of an ownership.</param>
        /// <param name="propertyId">The ID of a specific property, which has to be checked, if it belongs to a player.</param>
        /// <returns>Returns a boolean-"True" if the Player is the owner of the current property, otherwise boolean-"False".</returns>
        public bool PlayerIsOwner(Guid playerId, byte propertyId)
        {
            if (!ownerships.ContainsKey(playerId))
                // Player does not own any properties
                return false;
            else
                // Player has properties, check if he owns the given one
                return ownerships[playerId].Contains(propertyId);
        }

        /// <summary>
        /// This method checks in combination with different steps, whether the current player is allowed to buy a house on a certain property.
        /// Different steps will be verifcated and finally if the player is the owner of all streets from a specific city and if he has no hotels, or less than 4 houses built,
        /// then the player is allowed to buy a house.
        /// </summary>
        /// <param name="playerId">The ID of the current Player which has to be checked in combination with the given property ID.</param>
        /// <param name="propertyId">The ID of a certain property, which needs to be verificated by different steps.</param>
        /// <returns>Returns a boolean-"true" if the current Player is allowed to buy a house on a certain property, otherwise a boolean-"false".</returns>
        public bool PlayerAllowedToBuyHouse(Guid playerId, byte propertyId)
        {
            if (!PlayerIsOwner(playerId, propertyId))
                return false;

            // Get the property with the given ID, check in the same step if it is a h housing property
            var property = properties.OfType<HousingProperty>().Where((prop) => prop.ID == propertyId).FirstOrDefault();
            if (property == null)
                return false;

            // No more houses allowed if a hotel is already placed on the property
            if (property.NumHotels != 0)
                return false;

            // Max 4 houses allowed
            if (property.NumHouses == 4)
                return false;

            // Get a list of properties in the city of the current property
            var sameCityProps = this.properties.OfType<HousingProperty>().Where((prop) => prop.City == property.City).ToList();

            // Now check each property if it is owned by the current player
            // In the same step, check the number of houses of each property (houses can only be bought if all other fields have h+1
            // houses)
            for (int i = 0; i < sameCityProps.Count; i++)
            {
                // Skip the own house
                if (sameCityProps[i].ID == property.ID)
                    continue;

                // Check if the player is the owner
                if (!PlayerIsOwner(playerId, sameCityProps[i].ID))
                    return false;

                // Check if the property has the required number of houses
                if (property.NumHouses > sameCityProps[i].NumHouses)
                    return false;
            }

            // If this line is reached, the property is a housing property, is owned by the cur. player and the player owns all other
            // properties of this street.
            return true;
        }

        /// <summary>
        /// This method checks in combination with different steps, whether the current player is allowed to buy a hotel on a certain property.
        /// Different steps will be verifcated and finally if the player has 4 houses built on the given property, and no hotels built on it, 
        /// then the player is allowed to buy a hotel.
        /// </summary>
        /// <param name="playerId">The ID of the current Player which has to be checked in combination with the given property ID.</param>
        /// <param name="propertyId">The ID of a certain property, which needs to be verificated by different steps.</param>
        /// <returns>Returns a boolean-"true" if the current Player is allowed to buy a hotel on a certain property, otherwise a boolean-"false".</returns>
        public bool PlayerAllowedToBuyHotel(Guid playerId, byte propertyId)
        {
            if (!PlayerIsOwner(playerId, propertyId))
                return false;

            // Get the property with the given ID, check in the same step if it is a h housing property
            var property = properties.OfType<HousingProperty>().Where((prop) => prop.ID == propertyId).FirstOrDefault();
            if (property == null)
                return false;

            // No hotel allowed if the current property has not 4 houses on it or already a hotel
            if (property.NumHouses != 4 || property.NumHotels != 0)
                return false;

            // Get a list of properties in the city of the current property
            var sameCityProps = this.properties.OfType<HousingProperty>().Where((prop) => prop.City == property.City).ToList();

            // Now check each property if it is owned by the current player
            // In the same step, check the number of houses of each property (hotels can only be bought if all fields in the street have 4
            // houses)
            for (int i = 0; i < sameCityProps.Count; i++)
            {
                // Skip the own house
                if (sameCityProps[i].ID == property.ID)
                    continue;

                // Check if the player is the owner
                if (!PlayerIsOwner(playerId, sameCityProps[i].ID))
                    return false;

                // Check if the property has the required number of houses
                if (sameCityProps[i].NumHouses != 4)
                    return false;
            }

            // If this line is reached, the property is a housing property, is owned by the cur. player, the player owns all other
            // properties of this street and has build 4 houses on it.
            return true;
        }

        /// <summary>
        /// This method is called if the player is allowed to buy a hotel on a certain property. 
        /// This will be checked by calling the previous defined methods "PlayerIsOWner" and "PlayerAllowedToBuyHotel".
        /// The desired property will be get by searching for it in the list of all properties of type "HousingProperty". 
        /// If the hotel was added to the property -> the view will get immediately a notification about a change: 
        /// a hotel will be drawn and the number of owned hotels will be increased.
        /// </summary>
        /// <param name="playerId">The ID of the current player who wants to buy a hotel on his property.</param>
        /// <param name="propertyId">The ID of a certain property, where the hotel should be built on it.</param>
        /// <returns>Returns a boolean-"True" if the hotel was successfully added to the given property, otherwise boolean-"False".</returns>
        public bool TryAddHotel(Guid playerId, byte propertyId)
        {
            if (!PlayerIsOwner(playerId, propertyId))
                return false;

            if (!PlayerAllowedToBuyHotel(playerId, propertyId))
                return false;

            // Get the desired property
            var curProp = this.properties.OfType<HousingProperty>().Where((x) => x.ID == propertyId).FirstOrDefault();
            if (curProp == null)
                return false;

            try
            {
                curProp.NumHotels++;
                this.Notify(new PropertyChangedMessage(curProp, $"Hotel added to property {propertyId}"));
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This method is called if the player is allowed to buy a house on a certain property. 
        /// This will be checked by calling the previous defined methods "PlayerIsOWner" and "PlayerAllowedToBuyHouse".
        /// The desired property will be get by searching for it in the list of all properties of type "HousingProperty". 
        /// If the house was added to the property -> the view will get immediately a notification about a change: 
        /// a house will be drawn and the number of owned houses will be increased.
        /// </summary>
        /// <param name="playerId">The ID of the current player who wants to buy a house on his property.</param>
        /// <param name="propertyId">The ID of a certain property, where the house should be built on it.</param>
        /// <returns>Returns a boolean-"True" if the house was successfully added to the given property, otherwise boolean-"False".</returns>
        public bool TryAddHouse(Guid playerId, byte propertyId)
        {
            if (!PlayerIsOwner(playerId, propertyId))
                return false;

            if (!PlayerAllowedToBuyHouse(playerId, propertyId))
                return false;

            // Get the desired property
            var curProp = this.properties.OfType<HousingProperty>().Where((x) => x.ID == propertyId).FirstOrDefault();
            if (curProp == null)
                return false;

            try
            {
                curProp.NumHouses++;
                this.Notify(new PropertyChangedMessage(curProp, $"House added to property {propertyId}"));
                return true;
            }
            catch
            {
                return false;
            }
        }

        
        /// <summary>
        /// This method is called if a player is allowed to buy a certain property.
        /// First the relative-ownership will be checked,
        /// i.e. if the bank is the owner of the desired property, or if it is not buyable because the current player owns it already.
        /// If the property has been bought, the view will be immediately informed about this change and the number of owned properties will be increased.
        /// </summary>
        /// <param name="playerId">The ID of the current player who wants to buy a certain property.</param>
        /// <param name="propertyId">The ID of a certain property, which should be bought.</param>
        /// <returns>Returns a boolean-"True" if the property was successfully bought, otherwise boolean-"False".</returns>
        public bool TryBuyProperty(Guid playerId, byte propertyId)
        {
           
            // check the ownerships for this property, to buy it, it must not be owned by anyone
            // TODO we actually double check this right now in here and in player, so we could leave it out somewhere I think
            OwnerShip ownerShip = this.GetRelativeOwnership(playerId, propertyId);

            if ((ownerShip != OwnerShip.OwnerIsBank) || (ownerShip == OwnerShip.PropertyNotBuyable))
                return false;

            // Did not find an owner for the property --> he can buy it
            //if (!this.ownerships.ContainsKey(playerId))
            //{
                RemovePropFromOwner(bank.GetBankAccountId(),propertyId);
                AssignPlayerToProp(playerId, propertyId);
            //this.ownerships.Add(playerId, new List<byte>());
            //}

            // Finally, register the property in the ownership table
            // this.ownerships[playerId].Add(propertyId);
            PropertyBase property = this.properties.Where((prop) => prop.ID == propertyId).FirstOrDefault();
            this.Notify(new PropertyChangedMessage(property, $"Player with ID {propertyId} now owns the property #{propertyId}"));
            return true;
        }

        /// <summary>
        /// GetFieldType checks on which field-type the current player is located.
        /// This verification is done by comparing the given property ID with a defined field-type.
        /// If the property ID is invalid i.e. the ID number is out of a specific range -> an ArgumentException will be thrown. 
        /// </summary>
        /// <param name="propertyId">The ID of the property, where the player is located.</param>
        /// <returns>Returns an Enum (FieldType).</returns> <see cref="FieldType"/>
        public FieldType GetFieldType(byte propertyId)
        {
            if (propertyId > 40 || propertyId < 1)
                throw new ArgumentException($"Invalid property ID: {propertyId}");

            if (propertyId == 3 || propertyId == 23 || propertyId == 38)
                return FieldType.Risk;
            if (propertyId == 9 || propertyId == 28)
                return FieldType.Bank;
            if (propertyId == 11)
                return FieldType.GotoJail;
            if (propertyId == 21)
                return FieldType.PropertyTax;
            if (propertyId == 31)
                return FieldType.Jail;
            if (propertyId == 33)
                return FieldType.FixedTax;
            if (propertyId == 1)
                return FieldType.Start;
            else
                return FieldType.Property;
        }
    }
}
