﻿using System;
using Fhs.Dkt.Interfaces;
using System.Net;
using Fhs.Dkt.Messaging;
using System.IO;

// Uncomment the following to use the CloudDice as the standard dice and comment the line similar to this out in the Dice project
//[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.CloudDice))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The stub for the CloudDice. It is used to roll a random number with a virtual dice.
    /// Created by Boris Brankovic, Gabriel Dax and Mathias Lackner.
    /// </summary>
    public class CloudDice : IDice
    {
        /// <summary>Rolls a virtual dice and returns the resulting number.</summary>
        /// <returns>The generated number</returns>
        public byte RollDice()
        {
            const string serverUrl = "http://localhost:7071/api/RollDice";

            try
            {
                WebRequest request = WebRequest.Create(serverUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("Error in RollDice");
                StreamReader reader = new StreamReader(response.GetResponseStream());
                
                string result = reader.ReadToEnd().Trim('"');

                Console.WriteLine(result);
                
                return byte.Parse(result);
            }
            catch
            {
                this.Notify(new ConnectionErrorMessage(nameof(CloudDice)));
                throw;
            }
        }
    }
}
