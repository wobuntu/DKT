﻿using System;

using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Messaging;


/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This class is responsible to provide data and methods to handle the User/AI actions (all actions that handles the dealing with properties and bank).
    /// Created by Gabriel Dax.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Id for the <see cref="Player{T}"/>
        /// </summary>
        public Guid ID { get; private set; }

        /// <summary>
        /// Name for the <see cref="Player{T}"/>
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Private bank key from the player. It is used to make bank transactions.
        /// </summary>
        private Guid _bankKey;

        /// <summary>
        /// Variable to check if the player is played by a humen or computer.
        /// </summary>
        public bool IsAi;

        private IDice myDice;
        private IBank _bank;
        private ICardManager _cardManager;
        private IPropertyManager _propertyManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player{T}"/> class.
        /// </summary>
        /// <param name="playerId">The unique id for the player.</param>
        /// <param name="name">The name for the player (displayed in UI).</param>
        /// <param name="isAi"><c>true</c> if the player is a AI; otherwise, <c>false</c></param>
        public Player(Guid playerId, string name, bool isAi)
        {
            // Gather the required components from our dependency service
            this._bank = DependencyService.Get<IBank>();
            this._cardManager = DependencyService.Get<ICardManager>();
            this._propertyManager = DependencyService.Get<IPropertyManager>();

            // Initialize own properties / fields and open a bank account
            this.ID = playerId;
            this.Name = name;
            this.IsAi = isAi;
            this._bankKey = _bank.OpenAccount(ID);
            this.myDice = DependencyService.Get<IDice>();
        }

        /// <summary>
        /// This method generates two random numbers between one and six (using an external .dll).
        /// </summary>
        /// <seealso cref="Dice"/>
        /// <seealso cref="Assembly"/>
        /// <returns>The two random numbers using the datatype <c>Tuple</c></returns>
        public Tuple<byte,byte> RollDice()
        {
            Tuple<byte, byte> result = new Tuple<byte, byte>(myDice.RollDice(), myDice.RollDice());
            this.Notify(new DicesRolledMessage((byte)result.Item1, (byte)result.Item2));
            return result;
        }

        /// <summary>
        /// Check if a specific amount if available on the players bank account
        /// </summary>
        /// <param name="amount">Amount of money to check</param>
        /// <returns><c>true</c> if the player has enough money; otherwise, <c>false</c></returns>
        public bool CheckBalance(double amount)
        {
            return _bank.GetAccountBalance(ID, _bankKey) >= amount ? true : false;
        }

        /// <summary>
        /// Method to buy a property
        /// </summary>
        /// <param name="cardID">Property id</param>
        public void BuyProperty(int cardID)
        {
            PropertyBase property = _propertyManager.GetClone((byte)cardID);

            OwnerShip ownerShip = _propertyManager.GetRelativeOwnership(ID, property.ID);

            if ((ownerShip != OwnerShip.OwnerIsBank) || (ownerShip == OwnerShip.PropertyNotBuyable))
                return;

            Guid? ownerId = _propertyManager.GetOwnerOfProperty(property.ID);

            if(ownerId != null)
            {
               // _propertyManager.AssignPlayerToProp(ID, property.ID); //TODO: wait for bank key getter

                //zuerst bezahlen -> wenn bezahlt dann Grundstück überschreiben
                //bool propertyBought = _bank.MakeTransaction(ID, (Guid)ownerId, _bankKey, property.BuyingPrice);
                bool propertyBought = TransferMoney(property.BuyingPrice, (Guid)ownerId);


                if (propertyBought)
                {
                    _propertyManager.TryBuyProperty(ID, property.ID);
                    //_propertyManager.AddHouse(ID);
                }
            }
        }

        /// <summary>
        /// Method to buy a property
        /// </summary>
        /// <param name="cardID">Property id</param>
        public void SellProperty(int cardID)
        {
            PropertyBase property = _propertyManager.GetClone((byte)cardID);

            OwnerShip ownerShip = _propertyManager.GetRelativeOwnership(ID, property.ID);
            if (ownerShip != OwnerShip.OwnerIsPlayer)
                return;

            bool res = _propertyManager.RemovePropFromOwner(ID, (byte)cardID);
            _propertyManager.AssignPlayerToProp(_bank.GetBankAccountId(), (byte)cardID);
            if (res)
                _bank.DepositAmount(ID, property.SellingPrice);
        }

        /// <summary>
        /// Method to buy a house for a property
        /// </summary>
        /// <param name="cardID">Property id</param>
        /// <return><c>true</c> if buy House was sucessfull; otherwise, <c>false</c></return>
        public bool BuyHouse(int cardID)
        {
            HousingProperty property = _propertyManager.GetClone((byte)cardID) as HousingProperty;
            OwnerShip ownerShip = _propertyManager.GetRelativeOwnership(ID, property.ID);

            if ((property.PropertyType != PropertyType.Housing) || (ownerShip != OwnerShip.OwnerIsPlayer))
                return false;

            Guid? ownerId = _propertyManager.GetOwnerOfProperty(property.ID);

            //bool propertyBought = _bank.MakeTransaction(ID, (Guid)ownerId, _bankKey, property.HousePrice);
            bool propertyBought = TransferMoney(property.HousePrice, (Guid)ownerId);
            bool returnVal = false;

            if (propertyBought)
            {
              returnVal = _propertyManager.TryAddHouse(ID, property.ID);
            }

            if (!returnVal)
                this.Notify(new LogOnlyMessage("Buy House on Property: " + property.Name + " (player id: " + ID + ") went wrong"));

            return returnVal;
        }

        /// <summary>
        /// Method to buy a Hotel for a property
        /// </summary>
        /// <param name="cardID">Property id</param>
        /// <return><c>true</c> if buy Hotel was sucessfull; otherwise, <c>false</c></return>
        public bool BuyHotel(int cardID)
        {          
            HousingProperty property = _propertyManager.GetClone((byte)cardID) as HousingProperty;
            OwnerShip ownerShip = _propertyManager.GetRelativeOwnership(ID, property.ID);

            if ((property.PropertyType != PropertyType.Housing) || (ownerShip != OwnerShip.OwnerIsPlayer))
                return false;

            Guid? ownerId = _propertyManager.GetOwnerOfProperty(property.ID);

            //bool propertyBought = _bank.MakeTransaction(ID, (Guid)ownerId, _bankKey, property.HotelPrice);
            bool propertyBought = TransferMoney(property.HotelPrice, (Guid)ownerId);

            bool returnVal = false;

            if (propertyBought)
            {
                returnVal = _propertyManager.TryAddHotel(ID, property.ID);
            }

            if (!returnVal)
                this.Notify(new LogOnlyMessage("Buy Hotel on Property: " + property.Name + " (player id: " + ID + ") went wrong"));

            return returnVal;
        }

        /// <summary>
        /// Transfers a specific amount of money to another bank account.
        /// </summary>
        /// <param name="amount">The amount of money to transfer.</param>
        /// <param name="receiverID">Id of the receiver (<see cref="Player{T}"/> id).</param>
        /// <returns><c>true</c> if the transaction was successful; otherwise, <c>false</c></returns>
        private bool TransferMoney(double amount, Guid receiverID)
        {
            Boolean check = _bank.MakeTransaction(ID, receiverID, _bankKey, amount);
            if (!check)
            {
                this.Notify(new LogOnlyMessage("Transfer Money of player (with id: " + ID + ") went wrong"));

            }
            return check;
        }

        /// <summary>
        /// Checks the ownership of the property card.
        /// </summary>
        /// <param name="cardID">Property id</param>
        /// <returns><c>true</c> if the propertycard is owned by the player; otherwise, <c>false</c></returns>
        public bool IsOwnProperty(byte cardID)
        {
            return _propertyManager.PlayerIsOwner(ID, cardID);
        }

        /// <summary>
        /// Draws a new bank card.
        /// </summary>
        /// <returns>Returns the drawn bank card.</returns>
        private BankCard DrawBankCard()
        {
            BankCard drawnCard = _cardManager.DrawBankCard(ID);
            this.Notify(new CardDrawnMessage(ID, drawnCard));
            return drawnCard;
        }


        /// <summary>
        /// Draws a new risk card.
        /// </summary>
        /// <returns>Returns the drawn risk card.</returns>
        private RiskCard DrawRiskCard()
        {
            RiskCard drawnCard = _cardManager.DrawRiskCard(ID);
            this.Notify(new CardDrawnMessage(ID, drawnCard));
            return drawnCard;
        }

        /// <summary>
        /// Method to draw a bank/risk card.
        /// </summary>
        /// <param name="cardType">Type of card to draw.</param>
        /// <returns>Returns the drawn card.</returns>
        public CardBase DrawCard(CardType cardType)
        {
            if (cardType.Equals(CardType.BankCard))
            {
                return DrawBankCard();
            }
            else if(cardType.Equals(CardType.RiscCard))
            {
                return DrawRiskCard();
            }

            throw new ArgumentException("Invalid CardType passed");
        }

        /// <summary>
        /// Transfers a specific amount of money to other players account.
        /// </summary>
        /// <param name="amount">The amount of money to transfer.</param>
        /// <param name="recipientId">The id of the player to whose bank account the money should be transferred</param>
        /// <returns><c>true</c> if the transaction was successful; otherwise, <c>false</c></returns>
        public bool MakePlayerPayment(double amount, Guid recipientId)
        {
            return _bank.MakeTransaction(ID, recipientId, _bankKey, amount);
        }

        /// <summary>
        /// Transfers a specific amount of money to bank account.
        /// </summary>
        /// <param name="amount">The amount of money to transfer.</param>
        /// <returns><c>true</c> if the transaction was successful; otherwise, <c>false</c></returns>
        public bool MakeBankPayment(double amount)
        {
           return  _bank.MakeBankTransaction(ID, _bankKey, amount);
        }

        /// <summary>
        /// Method to pay capital tax.
        /// </summary>
        public void MakeLevyPayment()
        {
            double cashAmount = _bank.GetAccountBalance(ID, _bankKey);
            double amount = Math.Round(cashAmount / 10);

            if (amount > 350)
                amount = 350.00;

            _bank.MakeBankTransaction(ID, _bankKey, amount);
        }

        /// <summary>
        /// Method to recieve card Deposit (bank/risk cards).
        /// </summary>
        /// <param name="card">The bank/risk card</param>
        public void RecieveCardDeposit(CardBase card)
        {
            double amount = card.Action.Amount.Value;
            _bank.DepositAmount(ID, amount);
        }

        /// <summary>
        /// Method to recieve the money by crossing the start field.
        /// </summary>
        public void RecieveNewRoundDeposit()
        {
            _bank.DepositNewRoundAmount(ID);
        }

//       /// <summary>
//       /// Method to handle the relative card payment (bank/risk card).
//       /// </summary>
//       /// <param name="card">The bank/risk card</param>
//       public bool MakeRelativeCardPayment(CardBase card)
//       {
//           // TODO IM PLAYER MGR REGELN
//           /*
//           // todo get number of properties
//
//           switch (bc.Action)
//           {
//               case Types.CardActionType.PropertyFixedPayment:
//                   // make fixed payment for number of hotels and houses
//                   break;
//               case Types.CardActionType.PropertyPayment:
//                   // make payment based on % of buying price of all properties
//                   break;
//               default:
//                   throw new Exception("Invalid card passed");
//           }
//           */
//           return true;
//       }
//       
    }
}