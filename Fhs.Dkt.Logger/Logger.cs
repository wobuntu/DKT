﻿// CREATED BY MATHIAS LACKNER
using System;

using Fhs.Dkt.Messaging;
using Fhs.Dkt.Interfaces;

[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.Logger))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This class is responsible for writing logs. Use an overload of
    /// <see cref="Report(string, LogLvl)"/> to add a new entry to the logs.
    /// Created by Mathias Lackner.
    /// </summary>
    public class Logger : ILogger
    {
        /// <summary>
        /// Creates a new Instance of the <see cref="Logger"/>, which is responsible to write logs.
        /// </summary>
        public Logger()
        {
            // Subscribe to all ILogMessages automatically
            this.SubscribeTo<ILogMessage>((msg) => report(msg));
        }


        /// <summary>
        /// Private variable which stores a reference to the NLog class logger used to write logs.
        /// </summary>
        private NLog.Logger nlog = NLog.LogManager.GetCurrentClassLogger();


        /// <summary>
        /// Reports the given <paramref name="message"/> to the logging component.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        /// <param name="progressPercentage">A percentage indicating a the current progress.</param>
        /// <param name="logLevel">The log level to use while writing the given information to the log.</param>
        public void Report(string message, double progressPercentage, LogLvl logLvl = LogLvl.Info)
        {
            report($"{message} (Percentage: {progressPercentage})", logLvl);
        }


        /// <summary>
        /// Reports the given <paramref name="message"/> to the logging component.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        /// <param name="logLevel">The log level to use while writing the the given information to the log.</param>
        public void Report(string message, LogLvl logLvl = LogLvl.Info)
        {
            report(message, logLvl);
        }


        /// <summary>
        /// Reports the given <paramref name="message"/> to the logging component.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        /// <param name="exception">An exception object which will be included in the log output.</param>
        /// <param name="logLevel">The log level to use while writing the given information to the log.</param>
        public void Report(string message, Exception exception, LogLvl logLvl = LogLvl.Error)
        {
            report($"{message}, Exception: {exception}", logLvl);
        }


        /// <summary>
        /// Reports the given <paramref name="exception"/> to the logging component.
        /// </summary>
        /// <param name="exception">The exception which will be written into the log.</param>
        /// <param name="logLevel">The log level to use while writing the given information to the log.</param>
        public void Report(Exception exception, LogLvl logLvl = LogLvl.Error)
        {
            report($"Exception: {exception}", logLvl);
        }


        /// <summary>
        /// Private method which does all the logging. It is used by all the public overloads of <see cref="Report(string, LogLvl)"/>.
        /// </summary>
        /// <param name="message">The message to report.</param>
        /// <param name="lvl">The log level which shall appear in the log for the given message.</param>
        private void report(string message, LogLvl lvl)
        {
            switch (lvl)
            {
                case LogLvl.Error:
                    nlog.Error(message);
                    break;
                case LogLvl.Warn:
                    nlog.Warn(message);
                    break;
                case LogLvl.Info:
                    nlog.Info(message);
                    break;
                case LogLvl.Debug:
                    nlog.Debug(message);
                    break;
            }
        }


        /// <summary>
        /// Private method which does all the logging for <see cref="ILogMessage"/>s.
        /// </summary>
        /// <param name="logMsg">The <see cref="ILogMessage"/> which shall be written to the logs.</param>
        private void report(ILogMessage logMsg) => report(logMsg.MessageDescription, logMsg.LogLevel);
    }
}
