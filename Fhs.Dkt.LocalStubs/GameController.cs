﻿// CREATED BY MATHIAS LACKNER
using System;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.LocalStubs;
using Fhs.Dkt.Interfaces;

[assembly: Dependency(typeof(GameController))]

namespace Fhs.Dkt.LocalStubs
{
    public class GameController : IGameController
    {
        public bool IsRunning()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IGameController),
                targetName: nameof(IsRunning),
                typeParams: null,
                returnExpected: true,
                parameters: null);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public string GetPlayerName(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IGameController),
                targetName: nameof(GetPlayerName),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<string>();
        }

        public void Start(int rounds)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IGameController),
                targetName: nameof(Start),
                typeParams: null,
                returnExpected: false,
                parameters: rounds);

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public void Stop()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IGameController),
                targetName: nameof(Stop),
                typeParams: null,
                returnExpected: false,
                parameters: null);

            RemoteConnector.RemoteInvokeAsync(info);
        }
    }
}
