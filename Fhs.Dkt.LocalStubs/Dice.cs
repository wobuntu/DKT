﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.LocalStubs;

[assembly: Dependency(typeof(Dice))]

namespace Fhs.Dkt.LocalStubs
{
    public class Dice : IDice
    {
        public byte RollDice()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IDice),
                targetName: nameof(RollDice),
                typeParams: null,
                returnExpected: true,
                parameters: null);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<byte>();
        }
    }
}
