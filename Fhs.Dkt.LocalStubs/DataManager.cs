﻿// CREATED BY MATHIAS LACKNER
using System.Collections.Generic;

using Fhs.Dkt;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Types;
using Fhs.Dkt.LocalStubs;


[assembly: Dependency(typeof(DataManager))]

namespace Fhs.Dkt.LocalStubs
{
    public class DataManager : IDataManager
    {
        public List<T> DeserializeList<T>(string source = null)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IDataManager),
                targetName: nameof(DeserializeList),
                typeParams: new[] { typeof(T) },
                returnExpected: true,
                parameters: source);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<List<T>>();
        }

        public T DeserializeObject<T>(string source = null)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IDataManager),
                targetName: nameof(DeserializeObject),
                typeParams: new[] { typeof(T) },
                returnExpected: true,
                parameters: source);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<T>();
        }


        public void SerializeObject<T>(T toSerialize, string target = null, bool overwriteExisting = true)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IDataManager),
                targetName: nameof(SerializeObject),
                typeParams: new[] { typeof(T) },
                returnExpected: false,
                parameters: new object[] { toSerialize, target, overwriteExisting });

            RemoteConnector.RemoteInvokeAsync(info);
        }
    }
}
