﻿// CREATED BY MATHIAS LACKNER
using System;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.LocalStubs;


//[assembly: Dependency(typeof(Logger))]

namespace Fhs.Dkt.LocalStubs
{
    public class Logger : ILogger
    {
        public void Report(string message, double progressPercentage, LogLvl logLevel = LogLvl.Info)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ILogger),
                targetName: nameof(Report),
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { message, progressPercentage, logLevel });

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public void Report(string message, LogLvl logLevel = LogLvl.Info)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ILogger),
                targetName: nameof(Report),
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { message, logLevel });

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public void Report(string message, Exception exception, LogLvl logLevel = LogLvl.Error)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ILogger),
                targetName: nameof(Report),
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { message, exception, logLevel });

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public void Report(Exception exception, LogLvl logLevel = LogLvl.Error)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ILogger),
                targetName: nameof(Report),
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { exception, logLevel });

            RemoteConnector.RemoteInvokeAsync(info);
        }
    }
}
