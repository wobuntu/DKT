﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.LocalStubs;


[assembly: Dependency(typeof(PropertyManager))]

namespace Fhs.Dkt.LocalStubs
{
    public class PropertyManager : IPropertyManager
    {
        public bool AddHouse(Guid playerID)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(AddHouse),
                typeParams: null,
                returnExpected: true,
                parameters: playerID);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public void AssignPlayerToProp(Guid ownerID, byte propertyID)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(AssignPlayerToProp),
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { ownerID, propertyID });

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public City? GetCityOfProp(int id)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetCityOfProp),
                typeParams: null,
                returnExpected: true,
                parameters: id);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<City?>();
        }

        public PropertyBase GetClone(byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetClone),
                typeParams: null,
                returnExpected: true,
                parameters: propertyId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<PropertyBase>();
        }

        public FieldType GetFieldType(byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetFieldType),
                typeParams: null,
                returnExpected: true,
                parameters: propertyId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<FieldType>();
        }

        public string GetNameOfProp(int id)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetNameOfProp),
                typeParams: null,
                returnExpected: true,
                parameters: id);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<string>();
        }

        public byte GetNumberOwnedHotels(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetNumberOwnedHotels),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<byte>();
        }

        public byte GetNumberOwnedHouses(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetNumberOwnedHouses),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<byte>();
        }

        public byte GetNumberOwnedProperties(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetNumberOwnedProperties),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<byte>();
        }

        public byte GetNumberOwnedProperties(Guid playerId, PropertyType filter)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetNumberOwnedProperties),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, filter });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<byte>();
        }

        public IReadOnlyList<byte> GetOwnedProperties(Guid playerID)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetOwnedProperties),
                typeParams: null,
                returnExpected: true,
                parameters: playerID);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<List<byte>>();
        }

        public Guid? GetOwnerOfProperty(byte propertyID)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetOwnerOfProperty),
                typeParams: null,
                returnExpected: true,
                parameters: propertyID);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<Guid?>();
        }

        public OwnerShip GetRelativeOwnership(Guid ownerID, byte propertyID)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(GetRelativeOwnership),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { ownerID, propertyID });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<OwnerShip>();
        }

        public bool PlayerAllowedToBuyHotel(Guid playerId, byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(PlayerAllowedToBuyHotel),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, propertyId });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool PlayerAllowedToBuyHouse(Guid playerId, byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(PlayerAllowedToBuyHouse),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, propertyId });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool PlayerIsOwner(Guid playerId, byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(PlayerIsOwner),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, propertyId });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool RemovePropFromOwner(Guid ownerID, byte propertyID)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(RemovePropFromOwner),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { ownerID, propertyID });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool TryAddHotel(Guid playerId, byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(TryAddHotel),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, propertyId });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool TryAddHouse(Guid playerId, byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(TryAddHouse),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, propertyId });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool TryBuyProperty(Guid playerId, byte propertyId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IPropertyManager),
                targetName: nameof(TryBuyProperty),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, propertyId });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }
    }
}