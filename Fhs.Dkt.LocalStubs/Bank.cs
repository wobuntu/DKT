﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.LocalStubs;


[assembly:Dependency(typeof(Bank))]

namespace Fhs.Dkt.LocalStubs
{
    public class Bank : IBank
    {
        public bool CheckAccountPermission(Guid playerId, Guid key)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(CheckAccountPermission),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, key });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool DepositAmount(Guid playerId, double amount)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(DepositAmount),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, amount });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool DepositNewRoundAmount(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(DepositNewRoundAmount),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public double GetAccountBalance(Guid playerId, Guid key)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(GetAccountBalance),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, key });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<double>();
        }

        public Guid GetBankAccountId()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(GetBankAccountId),
                typeParams: null,
                returnExpected: true,
                parameters: null);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<Guid>();
        }

        public List<string> GetTransactionList()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(GetTransactionList),
                typeParams: null,
                returnExpected: true,
                parameters: null);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<List<string>>();
        }

        public Guid GetWealthiestPlayer()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(GetWealthiestPlayer),
                typeParams: null,
                returnExpected: true,
                parameters: null);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<Guid>();
        }

        public bool MakeBankTransaction(Guid playerId, Guid key, double amount)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(MakeBankTransaction),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, key, amount });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public bool MakeTransaction(Guid playerId, Guid recipientId, Guid key, double amount)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(MakeTransaction),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { playerId, recipientId, key, amount });

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public Guid OpenAccount(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IBank),
                targetName: nameof(OpenAccount),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<Guid>();
        }
    }
}
