﻿// CREATED BY MATHIAS LACKNER
using System;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.LocalStubs;


[assembly: Dependency(typeof(CardManager))]

namespace Fhs.Dkt.LocalStubs
{
    public class CardManager : ICardManager
    {
        public BankCard DrawBankCard(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ICardManager),
                targetName: nameof(DrawBankCard),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<BankCard>();
        }

        public CardBase DrawCard(CardType cardType, Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ICardManager),
                targetName: nameof(DrawCard),
                typeParams: null,
                returnExpected: true,
                parameters: new object[] { cardType, playerId});

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<CardBase>();
        }

        public RiskCard DrawRiskCard(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ICardManager),
                targetName: nameof(DrawRiskCard),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<RiskCard>();
        }

        public bool PlayerHasJailCard(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ICardManager),
                targetName: nameof(PlayerHasJailCard),
                typeParams: null,
                returnExpected: true,
                parameters: playerId);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<bool>();
        }

        public void UnlockCard(Guid playerId)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(ICardManager),
                targetName: nameof(UnlockCard),
                typeParams: null,
                returnExpected: false,
                parameters: playerId);

            RemoteConnector.RemoteInvokeAsync(info);
        }
    }
}
