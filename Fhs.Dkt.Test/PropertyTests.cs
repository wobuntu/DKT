﻿// CREATED BY BORIS BRANKOVIC
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Fhs.Dkt.Types;

namespace Fhs.Dkt.Test
{
    [TestClass]
    public class PropertyTests
    {
        [TestMethod]
        public void IsPlayerOwner()
        {
            PropertyManager prm = new PropertyManager();
            Guid testID = new Guid();
            byte propertyID = 40;
            
          //  prm.AssignPlayerToProp(testID, propertyID);
            Assert.IsTrue(prm.PlayerIsOwner(testID, propertyID));
        }
    }
}
