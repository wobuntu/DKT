﻿// CREATED BY LUKAS BRUGGER
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Fhs.Dkt.Test
{
    [TestClass]
    public class BankTest
    {
        [TestMethod]
        public void Balance_WhenAccountJustCreated()
        {
            Bank bank = new Bank();

            Guid fakePlayerId = Guid.NewGuid();
            Guid fakePlayerSecretKey = bank.OpenAccount(fakePlayerId);

            Assert.AreEqual(bank.GetAccountBalance(fakePlayerId, fakePlayerSecretKey), 1500.00);
        }

        [TestMethod]
        public void Balance_WhenMakingTransaction()
        {
            Bank bank = new Bank();

            Guid fakePlayerId = Guid.NewGuid();
            Guid fakePlayerSecretKey = bank.OpenAccount(fakePlayerId);

            bank.MakeBankTransaction(fakePlayerId, fakePlayerSecretKey, 100.00);

            Assert.AreEqual(bank.GetAccountBalance(fakePlayerId, fakePlayerSecretKey), 1400.00);
        }

        [TestMethod]
        public void Balance_TooHighTransaction()
        {
            Bank bank = new Bank();

            Guid fakePlayerId = Guid.NewGuid();
            Guid fakePlayerSecretKey = bank.OpenAccount(fakePlayerId);

            Assert.IsFalse(bank.MakeBankTransaction(fakePlayerId, fakePlayerSecretKey, 2000.00));
        }

        [TestMethod]
        public void Balance_GetWealthiestPlayer()
        {
            Bank bank = new Bank();

            Guid fakePlayerId1 = Guid.NewGuid();
            Guid fakePlayerSecretKey1 = bank.OpenAccount(fakePlayerId1);

            Guid fakePlayerId2 = Guid.NewGuid();

            bank.MakeTransaction(fakePlayerId1, fakePlayerId2, fakePlayerSecretKey1, 200.00);

            Guid wealthiestPlayer = bank.GetWealthiestPlayer();

            Assert.AreEqual(fakePlayerId2, wealthiestPlayer);
        }

        [TestMethod]
        [ExpectedException(typeof(Exceptions.BankActionNotPermittedException))]
        public void Transaction_WrongSecretKey()
        {
            Bank bank = new Bank();

            Guid fakePlayerId = Guid.NewGuid();
            Guid fakePlayerSecretKey = bank.OpenAccount(fakePlayerId);

            Guid wrongPlayerSecretKey = Guid.NewGuid();

            bank.MakeBankTransaction(fakePlayerId, wrongPlayerSecretKey, 2000.00);
        }
    }
}
