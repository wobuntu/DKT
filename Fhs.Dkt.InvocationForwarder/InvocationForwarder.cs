﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Fhs.Dkt.Messaging;

namespace Fhs.Dkt
{
    public static class InvocationForwarder
    {
        public static object DynamicInvoke(RemoteInvocationInfo info)
        {
            return DynamicInvoke(info.ComponentName, info.TargetName, info.TypeParams, info.Params);
        }

        public static object DynamicInvoke(string componentIdentifier, string targetName, Type[] genericTypeParams, params object[] parameters)
        {
            // Notifications are the only type which are handled not dynamically:
            if (componentIdentifier == "___custom___"
                && targetName == "___notify___"
                && parameters != null
                && parameters.Length > 0
                && parameters[0] is IMessage msg)
            {
                Console.WriteLine("Notification received from remote is about to be distributed...");
                var type = msg.GetType();
                componentIdentifier.Notify(msg); // just using any object to send the notification
                return null;
            }

            if (parameters == null)
                parameters = new object[0]; // will be easier for the following code

            Type interfaceType = null;
            var registeredType = DependencyService.GetAllRegisteredTypes()
                .Where((c) => (interfaceType = c.GetInterface(componentIdentifier)) != null)
                .FirstOrDefault();
            if (registeredType == null)
                throw new Exception("Unknown component requested (Not yet registered via the dependency service?).");

            IDktComponent component = DependencyService.Get(interfaceType);
            if (component == null)
                throw new Exception($"No component was found for type '{registeredType}'");

            Type concreteType = component.GetType();

            // I ♥♥♥♥♥ reflection
            IEnumerable<MethodInfo> methodInfos = concreteType.GetMethods().Where((x)=>x.Name == targetName);

            if (methodInfos == null)
                throw new Exception($"A target with name '{targetName}' could not be found for the component of type '{concreteType}'");

            // Found matching methods, now find the correct overload
            MethodInfo matchingMethodInfo = null;
            ParameterInfo[] matchingParams = null;
            foreach (var curMethodInfo in methodInfos)
            {
                matchingParams = curMethodInfo.GetParameters();
                if (matchingParams.Length != parameters.Length)
                    continue;

                bool found = true;
                for (int i = 0; i < matchingParams.Length; i++)
                {
                    if (parameters[i] != null && matchingParams[i].ParameterType == parameters[i].GetType())
                        continue;
                    else if (parameters[i] == null)
                        // may be ok
                        continue;

                    found = false;
                    break;
                }

                if (found)
                {
                    Console.WriteLine("Found matching method using Reflection");
                    matchingMethodInfo = curMethodInfo;
                    break;
                }
            }

            if (matchingMethodInfo == null)
                throw new Exception($"Did not find a matching method named '{targetName}' in instance of type '{componentIdentifier}'");

            if (matchingMethodInfo.IsGenericMethod)
            {
                Console.WriteLine("Requested method is generic, transforming it to concrete types...");
                matchingMethodInfo = matchingMethodInfo.MakeGenericMethod(genericTypeParams);
            }

            // Note: The return type may also be System.Void
            if (matchingParams.Length == 0)
            {
                Console.WriteLine("Calling method without arguments...");
                return matchingMethodInfo.Invoke(component, null);
            }
            else
            {
                Console.WriteLine($"Calling method with {parameters.Length} arguments");
                return matchingMethodInfo.Invoke(component, parameters);
            }
        }
    }
}
