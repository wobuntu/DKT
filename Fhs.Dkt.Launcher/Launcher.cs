﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Windows;

using Fhs.Dkt.Messaging;
using Fhs.Dkt.Interfaces;


/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The <see cref="Launcher"/> is the starting component of this solution. When it is executed, it registers itself at the
    /// <see cref="DependencyService"/> and triggers the service to load all components dynamically. Note that the
    /// <see cref="DependencyService"/> stores all dynamically instantiated references and returns those instead of new instances for
    /// new requests. This ensures, that all components use the instances requested by the <see cref="Launcher"/>.
    /// Created by Mathias Lackner.
    /// </summary>
    public class Launcher : Application, ILauncher
    {
        /// <summary>The entry point of the <see cref="Launcher"/> which will continue with loading all other components.</summary>
        /// <param name="args">Command line arguments passed to this program. Note that they are ignored.</param>
        [STAThread]
        public static int Main(string[] args)
        {
            Launcher engine = new Launcher();
            return engine.Run();
        }

        /// <summary>An empty object used as a lock for multi threading access protection.</summary>
        private Object _lock = new Object();

        /// <summary>
        /// A private boolean to indicate if the game engine was requested to perform an application wide shutdown and is now in the state
        /// between finishing the cleanup and closing the application.
        /// </summary>
        private bool shutdownDone = false;

        /// <summary>
        /// The default constructor of this class, which is triggered by the Run-method inherited from the Application class.
        /// This component registers itself at the <see cref="DependencyService"/> and triggers the service to load all required components
        /// dynamically. Note that the <see cref="DependencyService"/> stores all dynamically instantiated references and returns those
        /// instead of new instances for new requests. This ensures, that all components use the instances requested by the
        /// <see cref="Launcher"/>.
        /// </summary>
        public Launcher()
        {
            RemoteConnectionSettings.Host = "localhost";
            RemoteConnectionSettings.ServerPort = 12345;
            RemoteConnectionSettings.LocalPort = 54321;

            DependencyService.Register<ILauncher>(this);
            
            // Initialize all components via our dependency service. This ensures, that all components can run their startup routines to
            // work properly. Our dependency service will return exactly those instances instead of creating new ones for every Get<..>
            // request.
            var logger = DependencyService.Get<ILogger>(new DependencyService.ForceLoadSettings()
            {
                // This ensures loading the assemblies before they are needed so that the DependencyService finds our registration
                // attributes
                WildcardPatterns = new[] { "Fhs.Dkt*.dll", "*.exe" }
            });

            // Start the server after the DLLs were loaded (otherwise required DLLs might not have been loaded yet)
            Server.StartAsyncListenerIfNotActive(RemoteConnectionSettings.LocalPort);

            // this.Notify(new LogOnlyMessage("GameEngine started. Initializing components..."));

            DependencyService.Get<IDataManager>();
            DependencyService.Get<IBank>();
            DependencyService.Get<ICardManager>();
            DependencyService.Get<IPropertyManager>();
            DependencyService.Get<IGameController>(allowToBeMissing: true);
            
            IGameField gameField = DependencyService.Get<IGameField>(allowToBeMissing: true);

            this.Notify(new LogOnlyMessage("All components initialized."));

            // If the game field is null, it wouldn't make sense to run the application in our scenario right now
            if (gameField == null)
            {
                this.Notify(new LogOnlyMessage("No component registered for IGameField. Shutting down."));
                MessageBox.Show("Keine Komponente für die Darstellung (GameField) registriert. Beende.");
                Application.Current.Shutdown();
            }
            else
                gameField.Show();
        }


        /// <summary>
        /// The <see cref="SoftShutdown"/> method may be called at any time from any thread to request the engine and all other components
        /// to shutdown. Note that this will result in firing an <see cref="ApplicationShutdownMessage"/>.
        /// </summary>
        public void SoftShutdown()
        {
            // Use a lock here since we work on multiple threads and cannot be sure that the shutdown will be called only from one
            // component at once
            lock (_lock)
            {
                if (shutdownDone)
                    return;

                // Unsubscribe from all messages, while sending an application-wide shutdown message
                this.UnsubscribeAll();
                this.Notify(new ApplicationShutdownMessage());

                // The game has ended and the different components perform cleanup tasks where required (may run async as well as sync,
                // depending on the components themselfes). All triggered by the sending of the ApplicationShutdownMessage();

                // Get the player with the most money
                Guid winnerId;
                string winnerName = null;
                try
                {
                    winnerId = DependencyService.Get<IBank>().GetWealthiestPlayer();
                    winnerName = DependencyService.Get<IGameController>().GetPlayerName(winnerId);

                    MessageBox.Show($"Das Spiel ist zu Ende. Der Gewinner ist {winnerName}, "
                    + $"sie/er hat nach Verkauf aller Immobilien das meiste Vermögen!");
                }
                catch
                {
                    MessageBox.Show("Das Spiel ist zu Ende. Aber ohje, es gibt keinen eindeutigen gewinner...");
                }
                

                this.Dispatcher.InvokeShutdown();
                shutdownDone = true;
            }
        }
    }
}
