@echo off
echo ============================================================
echo   1/14: Bank
echo ============================================================
cd Fhs.Dkt.Bank
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.Bank\make.bat
cd ..
echo ============================================================
echo   2/14: CardManager
echo ============================================================
cd Fhs.Dkt.CardManager
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.CardManager\make.bat
cd ..
echo ============================================================
echo   3/14: CloudDice
echo ============================================================
cd Fhs.Dkt.CloudDice
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.CloudDice\make.bat
cd ..
echo ============================================================
echo   4/14: Core
echo ============================================================
cd Fhs.Dkt.Core
..\doxygen doxyfile.txt
\Dokumentation\Fhs.Dkt.Core\make.bat
cd ..
echo ============================================================
echo   5/14: DataManager
echo ============================================================
cd Fhs.Dkt.DataManager
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.DataManager\make.bat
cd ..
echo ============================================================
echo   6/14: DependencyService
echo ============================================================
cd Fhs.Dkt.DependencyService
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.DependencyService\make.bat
cd ..
echo ============================================================
echo   7/14: Dice
echo ============================================================
cd Fhs.Dkt.Dice
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.Dice\make.bat
cd ..
echo ============================================================
echo   8/14: GameController
echo ============================================================
cd Fhs.Dkt.GameController
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.GameController\make.bat
cd ..
echo ============================================================
echo   9/14: GameField
echo ============================================================
cd Fhs.Dkt.GameField
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.GameField\make.bat
cd ..
echo ============================================================
echo   10/14: Launcher
echo ============================================================
cd Fhs.Dkt.Launcher
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.Launcher\make.bat
cd ..
echo ============================================================
echo   11/14: Logger
echo ============================================================
cd Fhs.Dkt.Logger
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.Logger\make.bat
cd ..
echo ============================================================
echo   12/14: Player
echo ============================================================
cd Fhs.Dkt.Player
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.Player\make.bat
cd ..
echo ============================================================
echo   13/14: PropertyManager
echo ============================================================
cd Fhs.Dkt.PropertyManager
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.PropertyManager\make.bat
cd ..
echo ============================================================
echo   14/14: NumGen
echo ============================================================
cd NumGen
..\doxygen doxyfile.txt
..\Dokumentation\Fhs.Dkt.NumGen\make.bat
cd ..
echo ============================================================
echo   Done.
echo ============================================================