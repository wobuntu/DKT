﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Shapes;
using System.Windows.Controls;

using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Converters;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// A WPF Control which is used to represent a property on the <see cref="GameView"/>.
    /// Created by Mathias Lackner.
    /// </summary>
    public partial class FieldControl : UserControl
    {
        /// <summary>
        /// Creates a new instance of the <see cref="FieldControl"/>, which is sued to represent a property.
        /// </summary>
        public FieldControl()
        {
            InitializeComponent();
            this.DataContext = this;
#if DEBUG
            // This small code is required to ensure that no errors are displayed during the design time regarding the deserialization
            // done in the property manager (VS would show exceptions in the designer then).
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
#endif
            propertyMgr = DependencyService.Get<IPropertyManager>();
        }

        /// <summary>
        /// Private variable to store the instance of the <see cref="IPropertyManager"/> implementation for updating property informations.
        /// </summary>
        private IPropertyManager propertyMgr = null;


        #region Properties
        /// <summary>
        /// An optional image which can be shown in the background of the property.
        /// </summary>
        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        /// <summary>
        /// The ID of the property which is displayed.
        /// </summary>
        public int Id
        {
            get { return (int)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        /// <summary>
        /// The background color of the upper part of the <see cref="FieldControl"/>.
        /// </summary>
        public Brush ColorTop
        {
            get { return (Brush)GetValue(ColorTopProperty); }
            set { SetValue(ColorTopProperty, value); }
        }

        /// <summary>
        /// The background color of the lower part of the <see cref="FieldControl"/>.
        /// </summary>
        public Brush ColorBottom
        {
            get { return (Brush)GetValue(ColorBottomProperty); }
            set { SetValue(ColorBottomProperty, value); }
        }

        /// <summary>
        /// The displayed name of the field.
        /// </summary>
        public string PropertyName
        {
            get { return (string)GetValue(PropertyNameProperty); }
            set { SetValue(PropertyNameProperty, value); }
        }

        /// <summary>
        /// Gets or sets the rotation of the <see cref="FieldControl"/>.
        /// </summary>
        public double Rotation
        {
            get { return (double)GetValue(RotationProperty); }
            set { SetValue(RotationProperty, value); }
        }
        #endregion


        #region Methods
        /// <summary>
        /// Hides the player with the given internal player ID (used solely by the <see cref="GameField"/> module) from the current
        /// field.
        /// </summary>
        /// <param name="internalPlayerId">The internal ID of the player, used soley by the <see cref="GameField"/> module.</param>
        public void HidePlayer(byte internalPlayerId)
        {
            var playerEllipse = this.gridPlayers.Children.OfType<Ellipse>().ToArray()[internalPlayerId];
            playerEllipse.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Shows the player with the given internal player ID (used solely by the <see cref="GameField"/> component) on the current field.
        /// </summary>
        /// <param name="internalPlayerId">The internal ID of the player, used soley by the <see cref="GameField"/> module.</param>
        public void ShowPlayer(byte internalPlayerId)
        {
            var playerEllipse = this.gridPlayers.Children.OfType<Ellipse>().ToArray()[internalPlayerId];
            playerEllipse.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Private variable to store the number of houses on the field.
        /// </summary>
        private byte numHouses = 0;

        /// <summary>
        /// Gets or sets the number of houses on the field. Note that changing this value won't trigger any changes anywhere else.
        /// </summary>
        public byte NumHouses
        {
            get => numHouses;
            set
            {
                if (value > 4)
                    throw new Exception($"The number of houses must be a value between 0 and 4.");

                byte nextHouse = 0;
                for (; nextHouse < value; nextHouse++)
                    this.gridHouses.Children[nextHouse].Visibility = Visibility.Visible;

                for (int i = nextHouse; i < 4; i++)
                    this.gridHouses.Children[i].Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Private variable to store the number of hotels on the field. Note that changing this value won't trigger any changes anywhere else.
        /// </summary>
        private byte numHotels = 0;
        public byte NumHotels
        {
            get => numHotels;
            set
            {
                if (value > 1)
                    throw new Exception($"The number of hotels must be either 0 or 1");

                if (value == 0)
                    this.gridHouses.Children[4].Visibility = Visibility.Hidden;
                else
                    this.gridHouses.Children[4].Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// A private instance of an <see cref="PlayerIdToBrushConverter"/>, which is used to give the players different colors.
        /// </summary>
        private PlayerIdToBrushConverter idToBrushConv = new PlayerIdToBrushConverter();

        public PropertyBase PropertyClone { get; private set; } = null;

        /// <summary>
        /// Requests the field information from the <see cref="IPropertyManager"/> implementation and updates the visual representation
        /// according to the received data. If null is passed for parameter <paramref name="fromProperty"/>, the implementation of
        /// <see cref="IPropertyManager"/> is called and used to update the current view. Otherwise, the given object will be used to 
        /// update the data.
        /// </summary>
        public void UpdateData(PropertyBase fromProperty = null)
        {
            byte id = (byte)this.Id;

            if (fromProperty == null)
                PropertyClone = propertyMgr.GetClone(id);
            else
                PropertyClone = fromProperty;

            // Not all fields are known by the property manager (as "Start", etc.), therefore skip unknown properties
            if (PropertyClone == null)
            {
                this.NumHouses = this.NumHotels = 0;
                return;
            }

            this.PropertyName = PropertyClone.Name;
            if (PropertyClone is HousingProperty house)
            {
                this.NumHouses = house.NumHouses;
                this.NumHotels = house.NumHotels;
            }
            else
            {
                this.NumHouses = this.NumHotels = 0;
            }

            // Get the player owning the property
            //Guid? playerId = propertyMgr.GetOwnerOfProperty(id); // ---PROBLEM HERE
            int? internalPlayerId = null; //GameView.LastInstance?.GetInternalPlayerId(playerId);
            if (internalPlayerId != null)
            {
                border.BorderBrush = (Brush)idToBrushConv.Convert(internalPlayerId.Value, typeof(Brush), null, System.Globalization.CultureInfo.CurrentCulture);
                border.BorderThickness = new Thickness(4);
            }
            else
            {
                border.BorderThickness = new Thickness(0.5);
                border.BorderBrush = Brushes.Transparent;
            }
        }
        #endregion


        #region Dependency Properties
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="Image"/> property.
        /// </summary>
        static readonly DependencyProperty ImageProperty = DependencyProperty.Register(nameof(Image), typeof(ImageSource), typeof(FieldControl));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="Id"/> property.
        /// </summary>
        static readonly DependencyProperty IdProperty = DependencyProperty.Register(nameof(Id), typeof(int), typeof(FieldControl));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="ColorTop"/> property.
        /// </summary>
        static readonly DependencyProperty ColorTopProperty = DependencyProperty.Register(nameof(ColorTop), typeof(Brush), typeof(FieldControl));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="ColorBottom"/> property.
        /// </summary>
        static readonly DependencyProperty ColorBottomProperty = DependencyProperty.Register(nameof(ColorBottom), typeof(Brush), typeof(FieldControl));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="PropertyName"/> property.
        /// </summary>
        static readonly DependencyProperty PropertyNameProperty = DependencyProperty.Register(nameof(PropertyName), typeof(string), typeof(FieldControl));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="Rotation"/> property.
        /// </summary>
        static readonly DependencyProperty RotationProperty = DependencyProperty.Register(nameof(RotationProperty), typeof(double), typeof(FieldControl));
        #endregion
    }
}
