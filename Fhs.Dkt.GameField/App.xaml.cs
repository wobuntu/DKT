﻿using System.Windows;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>The application class of the GameField.</summary>
    public partial class App : Application { }
}
