﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

/// <summary>This namespace contains several converters used within the XAML of the UI in the GameField assembly.</summary>
namespace Fhs.Dkt.Converters
{
    /// <summary>
    /// Allows the conversion of a boolean input to its inverted value.
    /// Created by Mathias Lackner.
    /// </summary>
    public class NotConverter : IValueConverter
    {
        /// <summary>
        /// Converts the given boolean source value to its inverted value.
        /// </summary>
        /// <param name="value">The boolean value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool b)
                return !b;

            return value;
        }

        /// <summary>
        /// Converts the given boolean source value to its inverted value.
        /// </summary>
        /// <param name="value">The boolean value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool b)
                return !b;

            return value;
        }
    }
}
