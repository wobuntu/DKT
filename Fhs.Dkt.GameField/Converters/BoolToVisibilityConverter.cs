﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

/// <summary>This namespace contains several converters used within the XAML of the UI in the GameField assembly.</summary>
namespace Fhs.Dkt.Converters
{
    /// <summary>
    /// Allows the conversion of a boolean input to a <see cref="Visibility"/>.
    /// Created by Mathias Lackner.
    /// </summary>
    public class BoolToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Converts the given boolean source value to a visibility for the target binding.
        /// </summary>
        /// <param name="value">The boolean value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            bool b = (bool)value;
            if (b)
                return Visibility.Visible;

            return Visibility.Hidden;
        }

        /// <summary>
        /// Converts the given visibilty target value to a boolean for the source of the binding.
        /// </summary>
        /// <param name="value">The visibilty value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility vis)
            {
                if (vis == Visibility.Visible)
                    return true;

                return false;
            }

            return value;
        }
    }

    /// <summary>
    /// Allows the conversion of a boolen input to a <see cref="Visibility"/>.
    /// Created by Mathias Lackner.
    /// </summary>
    public class InvertedBoolToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Converts the given boolean source value to a visibility for the target binding.
        /// </summary>
        /// <param name="value">The boolean value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            bool b = (bool)value;
            if (b)
                return Visibility.Hidden;

            return Visibility.Visible;
        }

        /// <summary>
        /// Converts the given visibilty target value to a boolean for the source of the binding.
        /// </summary>
        /// <param name="value">The visibilty value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility vis)
            {
                if (vis == Visibility.Visible)
                    return false;

                return true;
            }

            return value;
        }
    }
}
