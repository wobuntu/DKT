﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

/// <summary>This namespace contains several converters used within the XAML of the UI in the GameField assembly.</summary>
namespace Fhs.Dkt.Converters
{
    /// <summary>
    /// Allows the conversion of a property ID to a brush.
    /// Created by Mathias Lackner.
    /// </summary>
    public class PropertyIdToBrushConverter : IValueConverter
    {
        /// <summary>
        /// Converts the given source property ID value to a brush.
        /// </summary>
        /// <param name="value">The property ID value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int internalPlayerId)
            {
                switch (internalPlayerId)
                {
                    case 2:
                    case 39:
                    case 40:
                        return new SolidColorBrush(Color.FromRgb(142, 143, 164));
                    case 5:
                    case 6:
                    case 7:
                        return new SolidColorBrush(Color.FromRgb(218, 182, 85));
                    case 10:
                    case 12:
                        return new SolidColorBrush(Color.FromRgb(236, 172, 110));
                    case 15:
                    case 16:
                    case 17:
                        return new SolidColorBrush(Color.FromRgb(216, 54, 51));
                    case 19:
                    case 20:
                    case 22:
                        return new SolidColorBrush(Color.FromRgb(127, 140, 58));
                    case 25:
                    case 26:
                    case 27:
                        return new SolidColorBrush(Color.FromRgb(237,212,49));
                    case 29:
                    case 30:
                    case 32:
                        return new SolidColorBrush(Color.FromRgb(200, 127, 50));
                    case 35:
                    case 36:
                    case 37:
                        return new SolidColorBrush(Color.FromRgb(80, 142, 177));
                    default:
                        return Brushes.Transparent;
                }
            }

            throw new ArgumentException("Input value must be an integer.", nameof(value));
        }

        /// <summary>
        /// Converts the given target brush value to a property ID. Note: This converter is no used within our solution,
        /// therefore it is not implemented and will throw an <see cref="NotImplementedException"/>.
        /// </summary>
        /// <param name="value">The brush value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Not required in our app
            throw new NotImplementedException();
        }
    }
}
