﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

/// <summary>This namespace contains several converters used within the XAML of the UI in the GameField assembly.</summary>
namespace Fhs.Dkt.Converters
{
    /// <summary>
    /// Allows the conversion of a visibility to a number, which must be specified via the <see cref="Number"/> property.
    /// This can be used to control e.g. the blur radius of an effect while binding to a visibility.
    /// Created by Mathias Lackner.
    /// </summary>
    public class VisibilityToNumberConverter : DependencyObject, IValueConverter
    {
        /// <summary>
        /// The number to which the <see cref="Visibility.Visible"/> value shall be converted.
        /// </summary>
        public int Number
        {
            get => (int)GetValue(NumberProperty);
            set { SetValue(NumberProperty, value); }
        }

        /// <summary>Private dependency property to keep track of changes regarding the <see cref="Number"/> property.</summary>
        static readonly DependencyProperty NumberProperty = DependencyProperty.Register(nameof(Number), typeof(int), typeof(VisibilityToNumberConverter));


        /// <summary>
        /// Converts the given source value, which is visibility, to a number given via the <see cref="Number"/> property.
        /// Visibilities which are not <see cref="Visibility.Visible"/> will be converted to 0.
        /// </summary>
        /// <param name="value">The visibility value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility vis)
            {
                if (vis == Visibility.Visible)
                    return Number;
                else
                    return 0;
            }

            throw new ArgumentException("The value must be a Visibility.");
        }


        /// <summary>
        /// Converts the given number to a visibility. Note: This converter is no used within our solution,
        /// therefore it is not implemented and will throw an <see cref="NotImplementedException"/>.
        /// </summary>
        /// <param name="value">The numeric integer value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Not required for our implementation
            throw new NotImplementedException();
        }
    }
}
