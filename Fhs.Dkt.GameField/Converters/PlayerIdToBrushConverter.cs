﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

/// <summary>This namespace contains several converters used within the XAML of the UI in the GameField assembly.</summary>
namespace Fhs.Dkt.Converters
{
    /// <summary>
    /// Allows the conversion of an internal player ID (used solely by the <see cref="GameField"/> module) to a brush.
    /// Created by Mathias Lackner.
    /// </summary>
    public class PlayerIdToBrushConverter : IValueConverter
    {
        /// <summary>
        /// Converts the given source value, which is an internally used player ID, to a brush.
        /// </summary>
        /// <param name="value">The internal player ID value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int internalPlayerId)
            {
                switch (internalPlayerId)
                {
                    case 0:
                        return Brushes.Green;
                    case 1:
                        return Brushes.Crimson;
                    case 2:
                        return Brushes.Yellow;
                    case 3:
                        return Brushes.DarkBlue;
                    case 4:
                        return Brushes.WhiteSmoke;
                    case 5:
                        return Brushes.LightBlue;
                    default:
                        throw new ArgumentException("Invalid internal player ID (values from 0 to 5 allowed)", nameof(value));
                }
            }
            throw new ArgumentException("Input value must be an integer.", nameof(value));
        }


        /// <summary>
        /// Converts the given target brush value to an internally used player ID. Note: This converter is no used within our solution,
        /// therefore it is not implemented and will throw an <see cref="NotImplementedException"/>.
        /// </summary>
        /// <param name="value">The brush value to convert.</param>
        /// <param name="targetType">The resulting type of the conversion.</param>
        /// <param name="parameter">An additional conversion parameter, which is not used for this converter.</param>
        /// <param name="culture">The culture which shall be used during the conversion. This is not used for this converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Only present to fulfill the interface requirements, but the conversion back is never required, therefore ignored here.
            throw new NotImplementedException();
        }
    }
}
