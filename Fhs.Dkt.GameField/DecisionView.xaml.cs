﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Types;
using System.Windows;
using System.Windows.Controls;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The <see cref="DecisionView"/> is an UserControl used to ask the player for decisions (e.g. if a house shall be buyed) and is
    /// used within the <see cref="GameView"/>.
    /// Created by Mathias Lackner.
    /// </summary>
    public partial class DecisionView : UserControl
    {
        /// <summary>
        /// Creates a new instance of a <see cref="DecisionView"/>. Use
        /// <see cref="Show(string, string, PropertyBase, UserChoice, string, string, string)"/> to display it and to define, what data
        /// is shown on it and which choices the user has.
        /// </summary>
        public DecisionView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        /// <summary>
        /// Shows the <see cref="DecisionView"/> on the <see cref="GameView"/>. Note that a property may be passed, which can be used
        /// to display a <see cref="FieldControl"/> within the <see cref="DecisionView"/>, showing information about it.
        /// The dicision the user took can be gathered via <see cref="GameField.LastDialogResult"/>.
        /// </summary>
        /// <param name="message">The message to be displayed.</param>
        /// <param name="caption">The caption which shall be displayed.</param>
        /// <param name="property">An optional property which is then displayed within the <see cref="DecisionView"/>.</param>
        /// <param name="buttons">This flagged value represents the choices the user can make.</param>
        /// <param name="textYes">An optional custom text for the 'Yes'-Button.</param>
        /// <param name="textNo">An optional custom text for the 'No'-Button.</param>
        /// <param name="textCancel">An optinal custom text for the 'Cancel'-Button.</param>
        public void Show(string message, string caption = null,
            PropertyBase property = null, UserChoice buttons = UserChoice.Yes,
            string textYes = "Ja", string textNo = "Nein", string textCancel = "Cancel")
        {
            // Set simple texts
            this.Caption = caption;
            this.Message = message;

            // Set the details for the property control
            if (property != null)
            {
                this.propertyView.Visibility = Visibility.Visible;
                this.propertyView.PropertyName = property.Name;
                this.propertyView.Id = property.ID;
                if (property is HousingProperty housingProp)
                {
                    this.propertyView.NumHouses = housingProp.NumHouses;
                    this.propertyView.NumHotels = housingProp.NumHouses;
                }
                else
                {
                    this.propertyView.NumHouses = 0;
                    this.propertyView.NumHotels = 0;
                }
            }
            else
            {
                this.propertyView.Visibility = Visibility.Collapsed;
            }
            
            // Set the visibility according to the chosen button types
            if (buttons.HasFlag(UserChoice.Yes))
                this.btnFirst.Visibility = Visibility.Visible;
            else
                this.btnFirst.Visibility = Visibility.Collapsed;

            if (buttons.HasFlag(UserChoice.No))
                this.btnSecond.Visibility = Visibility.Visible;
            else
                this.btnSecond.Visibility = Visibility.Collapsed;

            if (buttons.HasFlag(UserChoice.Cancel))
                this.btnThird.Visibility = Visibility.Visible;
            else
                this.btnThird.Visibility = Visibility.Collapsed;

            // Set button texts
            this.btnFirst.Content = textYes;
            this.btnSecond.Content = textNo;
            this.btnThird.Content = textCancel;

            // Ensure the current instance is visible
            this.Visibility = Visibility.Visible;
        }


        #region Properties
        /// <summary>
        /// Gets or sets the caption to display for the <see cref="DecisionView"/>.
        /// </summary>
        public string Caption
        {
            get => (string)GetValue(CaptionProperty);
            set { SetValue(CaptionProperty, value); }
        }

        /// <summary>
        /// Gets or sets the message text to display for the <see cref="DecisionView"/>.
        /// </summary>
        public string Message
        {
            get => (string)GetValue(MessageProperty);
            set { SetValue(MessageProperty, value); }
        }
        #endregion


        #region Dependency properties
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="Caption"/> property.
        /// </summary>
        static readonly DependencyProperty CaptionProperty = DependencyProperty.Register(nameof(Caption), typeof(string), typeof(DecisionView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="Message"/> property.
        /// </summary>
        static readonly DependencyProperty MessageProperty = DependencyProperty.Register(nameof(Message), typeof(string), typeof(DecisionView));
        #endregion

        /// <summary>
        /// Private event handler invoked if the 'Yes'-button was pressed.
        /// </summary>
        /// <param name="sender">The button instance, which sent the event.</param>
        /// <param name="e">The arguments of the event.</param>
        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            GameField.LastDialogResult = UserChoice.Yes;
            GameField.UiActionMade = true;
        }

        /// <summary>
        /// Private event handler invoked if the 'No' button was pressed.
        /// </summary>
        /// <param name="sender">The button instance, which sent the event.</param>
        /// <param name="e">The arguments of the event.</param>
        private void btnSecond_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            GameField.LastDialogResult = UserChoice.No;
            GameField.UiActionMade = true;
        }

        /// <summary>
        /// Private event handler invoked if the 'Cancel'-button was pressed.
        /// </summary>
        /// <param name="sender">The button instance, which sent the event.</param>
        /// <param name="e">The arguments of the event.</param>
        private void btnThird_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            GameField.LastDialogResult = UserChoice.Cancel;
            GameField.UiActionMade = true;
        }
    }
}
