﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Threading;

using Fhs.Dkt.Messaging;
using Fhs.Dkt.Interfaces;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The PlayerInfoView is a UserControl designed to display data about the remaining money of the player, her/his owned properties
    /// and further information. It also indicates if the player is currently active.
    /// Created by Mathias Lackner.
    /// </summary>
    public partial class PlayerInfoView : UserControl
    {
        /// <summary>
        /// Generates a new <see cref="PlayerInfoView"/> requiring the ID of the player, the internal player ID used by the view and
        /// if the player is an AI.
        /// </summary>
        /// <param name="playerGuid">The ID of the player.</param>
        /// <param name="internalPlayerId">The internal ID of the player, which is solely used within the <see cref="GameField"/>
        /// component.</param>
        /// <param name="playerName">The name of the player to be displayed in the <see cref="PlayerInfoView"/>.</param>
        /// <param name="isAI">Indicates if the player is an AI. This will cause another image to be displayed.</param>
        public PlayerInfoView(Guid playerGuid, int internalPlayerId, string playerName, bool isAI)
        {
            InitializeComponent();
            // Most of the following properties will also be gathered by receiving the messages regarding the player or the properties,
            // however, they are set anyway for the display in the design time view.
            this.DataContext = this;
            this.NumHouses = this.NumHotels = 0;
            this.PlayerPosition = 1;
            this.PlayerName = playerName;
            this.PlayerId = playerGuid;
            this.InternalPlayerId = internalPlayerId;
            this.IsAI = isAI;
            this.Balance = 1500;

            // Subscribe to all required messages
            this.SubscribeTo<PlayerMovedMessage>(onPlayerMoved);
            this.SubscribeTo<AccountBalanceChangedMessage>(onBalanceChanged);
            this.SubscribeTo<PropertyChangedMessage>(onPropertyChanged);
            this.SubscribeTo<ActivePlayerChangedMessage>(onActivePlayerChanged);
        }

        /// <summary>
        /// Private method which is invoked after <see cref="ActivePlayerChangedMessage"/>s were received. This changes the players to be
        /// displayed as active / inactive.
        /// </summary>
        /// <param name="msg">The received <see cref="ActivePlayerChangedMessage"/> containing all necessary information.</param>
        private void onActivePlayerChanged(ActivePlayerChangedMessage msg)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                if (this.PlayerId == msg.PlayerId)
                    this.BorderBrush = Brushes.YellowGreen;
                else
                    this.BorderBrush = Brushes.LightGray;
            }));
        }

        /// <summary>
        /// Private method which is invoked after <see cref="PlayerMovedMessage"/>s were received. This does also change the visual
        /// positions of the players on the <see cref="GameView"/>. Here, it is used to display the position as text in the
        /// <see cref="PlayerInfoView"/>.
        /// </summary>
        /// <param name="msg">The received <see cref="PlayerMovedMessage"/> containing all necessary information.</param>
        private void onPlayerMoved(PlayerMovedMessage msg)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                if (this.PlayerId == msg.PlayerId)
                    this.PlayerPosition = msg.NewFieldId;
            }));
        }

        /// <summary>
        /// Private method which is invoked after <see cref="AccountBalanceChangedMessage"/>s were received. It is used to display the
        /// balance of the player in the <see cref="PlayerInfoView"/>.
        /// </summary>
        /// <param name="msg">The received <see cref="AccountBalanceChangedMessage"/> containing all necessary information.</param>
        private void onBalanceChanged(AccountBalanceChangedMessage msg)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                if (this.PlayerId == msg.PlayerId)
                    this.Balance = msg.NewBalance;
            }));
        }

        /// <summary>
        /// Private method which is invoked after <see cref="PropertyChangedMessage"/>s were received. It is used to update the text of
        /// owned Properties/Houses/Hotels in the <see cref="PlayerInfoView"/>.
        /// </summary>
        /// <param name="msg">The received <see cref="PropertyChangedMessage"/> containing all necessary information.</param>
        private void onPropertyChanged(PropertyChangedMessage msg)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                var mgr = DependencyService.Get<IPropertyManager>();
                this.NumHotels = mgr.GetNumberOwnedHotels(this.PlayerId);
                this.NumHouses = mgr.GetNumberOwnedHouses(this.PlayerId);
                this.NumProperties = mgr.GetNumberOwnedProperties(PlayerId);
            }));
        }

        #region Properties
        /// <summary>
        /// Gets the ID of the player, which is represented by the current instance of the <see cref="PlayerInfoView"/>.
        /// </summary>
        public Guid PlayerId { get; private set; }

        /// <summary>
        /// Gets the internal player ID, which is used solely by the <see cref="GameField"/> component.
        /// </summary>
        public int InternalPlayerId { get; private set; }

        /// <summary>
        /// Gets or sets the displayed name of the player. Note that setting it does only change the displayed value in the view,
        /// changes are not applied anywhere else.
        /// </summary>
        public string PlayerName
        {
            get { return (string)GetValue(PlayerNameProperty); }
            set { SetValue(PlayerNameProperty, value); }
        }

        /// <summary>
        /// Gets or sets the displayed Balance of the player. Note that setting it does only change the displayed value in the view,
        /// changes are not applied anywhere else. In addition, it will be automatically updated after
        /// <see cref="AccountBalanceChangedMessage"/>s are received.
        /// </summary>
        public double Balance
        {
            get { return (double)GetValue(BalanceProperty); }
            set { SetValue(BalanceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the position of the player. Note that setting it does only change the displayed value in the view,
        /// changes are not applied anywhere else. In addition, it will be automatically updated after <see cref="PlayerMovedMessage"/>s
        /// are received.
        /// </summary>
        public int PlayerPosition
        {
            get { return (int)GetValue(PlayerPositionProperty); }
            set { SetValue(PlayerPositionProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of hotels owned by the player. Note that setting it does only change the displayed value in the view,
        /// changes are not applied anywhere else. In addition, it will be automatically updated after
        /// <see cref="PropertyChangedMessage"/>s are received.
        /// </summary>
        public int NumHotels
        {
            get { return (int)GetValue(NumHotelsProperty); }
            set { SetValue(NumHotelsProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of houses owned by the player. Note that setting it does only change the displayed value in the view,
        /// changes are not applied anywhere else. In addition, it will be automatically updated after
        /// <see cref="PropertyChangedMessage"/>s are received.
        /// </summary>
        public int NumHouses
        {
            get { return (int)GetValue(NumHotelsProperty); }
            set { SetValue(NumHousesProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of properties owned by the player. Note that setting it does only change the displayed value in the
        /// view, changes are not applied anywhere else. In addition, it will be automatically updated after
        /// <see cref="PropertyChangedMessage"/>s are received.
        /// </summary>
        public int NumProperties
        {
            get { return (int)GetValue(NumPropertiesProperty); }
            set { SetValue(NumPropertiesProperty, value); }
        }

        /// <summary>
        /// Gets or sets if the represented player is an AI. Note that setting it does only change the displayed value in the view,
        /// changes are not applied anywhere else.
        /// </summary>
        public bool IsAI
        {
            get { return (bool)GetValue(IsAIProperty); }
            set { SetValue(IsAIProperty, value); }
        }
        #endregion


        #region Dependency Properties
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="IsAI"/> property.
        /// </summary>
        static readonly DependencyProperty IsAIProperty = DependencyProperty.Register(nameof(IsAI), typeof(bool), typeof(PlayerInfoView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="PlayerPosition"/> property.
        /// </summary>
        static readonly DependencyProperty PlayerPositionProperty = DependencyProperty.Register(nameof(PlayerPosition), typeof(int), typeof(PlayerInfoView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="NumHouses"/> property.
        /// </summary>
        static readonly DependencyProperty NumHousesProperty = DependencyProperty.Register(nameof(NumHouses), typeof(int), typeof(PlayerInfoView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="NumHotels"/> property.
        /// </summary>
        static readonly DependencyProperty NumHotelsProperty = DependencyProperty.Register(nameof(NumHotels), typeof(int), typeof(PlayerInfoView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="NumProperties"/> property.
        /// </summary>
        static readonly DependencyProperty NumPropertiesProperty = DependencyProperty.Register(nameof(NumProperties), typeof(int), typeof(PlayerInfoView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="Balance"/> property.
        /// </summary>
        static readonly DependencyProperty BalanceProperty = DependencyProperty.Register(nameof(Balance), typeof(double), typeof(PlayerInfoView));
        /// <summary>
        /// Private dependency property to keep track of changes regarding the <see cref="PlayerName"/> property.
        /// </summary>
        static readonly DependencyProperty PlayerNameProperty = DependencyProperty.Register(nameof(PlayerName), typeof(string), typeof(PlayerInfoView));
        #endregion
    }
}
