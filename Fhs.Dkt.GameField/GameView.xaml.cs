﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Linq;
using System.Windows;
using System.Reflection;
using System.Windows.Threading;
using System.Collections.Generic;

using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Messaging;
using Fhs.Dkt.Interfaces;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    // Only used for our implementation of a remote method invocation
    internal interface IGameView : IDktComponent { }

    /// <summary>
    /// The game view is the visual reprentation of the game, controlled by the <see cref="GameField"/>. It displays the players,
    /// properties, the visual game field itself and other visual components.
    /// Created by Mathias Lackner.
    /// </summary>
    public partial class GameView : Window, IGameView
    {
        /// <summary>
        /// Creates a new instance of the <see cref="GameView"/> Window, which is controlled by the <see cref="GameField"/> and displays
        /// the players, properties, the visual game field itself and other visual components.
        /// </summary>
        public GameView()
        {
            DependencyService.Register<GameView>(this);

            if (Assembly.GetEntryAssembly() == Assembly.GetAssembly(typeof(GameView)))
            {
                // The application was NOT started from the game view
                this.Notify(new LogOnlyMessage("The GameView was not started via the GameEngine, but directly. Shutting down."));
                MessageBox.Show("Zum Starten des Spiels bitte Fhs.Dkt.exe ausführen.");
                Application.Current.Shutdown();
            }

            InitializeComponent();
            this.DataContext = this;

            this.Notify(new LogOnlyMessage("GameView is updating properties via the property manager..."));

            this.SubscribeTo<DicesRolledMessage>(onDicesRolled);
            this.SubscribeTo<PlayerMovedMessage>(onPlayerMoved);
            this.SubscribeTo<PropertyChangedMessage>(onPropertyChanged);
            this.SubscribeTo<CardDrawnMessage>(onCardDrawn);
            this.SubscribeTo<PlayerCreatedMessage>(onPlayerCreated);
            
            this.Loaded += (sender, args) =>
            {
                var propCtrls = this.mainGrid.Children.OfType<FieldControl>().ToList();
                foreach (var prop in propCtrls)
                    // The following will update the values via the datamanager instance
                    prop.UpdateData();

                this.Notify(new LogOnlyMessage("Initial update of displayed game view properties done."));
            };

            LastInstance = this;

            this.Notify(new LogOnlyMessage("GameView created."));
        }

        /// <summary>
        /// Internal variable which stores the last instance of the <see cref="GameView"/>.
        /// </summary>
        internal static GameView LastInstance = null;
        
        /// <summary>
        /// Private variable to store the last internal player ID.
        /// </summary>
        private sbyte lastPlayerInternalId = -1;
        /// <summary>
        /// Private dictionary which represents the relation between the internally used IDs and the actual player IDs of the players
        /// held by the <see cref="IGameController"/> implementation (The players cannot be accessed from outside the <see cref="IGameController"/>).
        /// </summary>
        private Dictionary<Guid, byte> playerRelation = new Dictionary<Guid, byte>();

        /// <summary>
        /// Stores the IDs of the players as keys and the position of the players as values. It is used to keep track of their position
        /// on the view.
        /// </summary>
        private Dictionary<Guid, byte> playerPositions = new Dictionary<Guid, byte>();
        

        /// <summary>
        /// Private method which is invoked everytime a <see cref="PlayerCreatedMessage"/> was received. It does some basic setup, like
        /// adding a <see cref="PlayerInfoView"/> to the <see cref="GameView"/>, or displaying a figure for the player on the field.
        /// </summary>
        /// <param name="msg">The received message, containing all necessary data.</param>
        private void onPlayerCreated(PlayerCreatedMessage msg)
        {
            if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
            {
                (DependencyService.Get<ILauncher>()).SoftShutdown();
                return;
            }

            this.Dispatcher.Invoke(new Action(() =>
            {
                Console.WriteLine("UI THREAD IS THREAD " + System.Threading.Thread.CurrentThread.ManagedThreadId);

                try
                {
                    lastPlayerInternalId++;

                    this.playerRelation.Add(msg.Guid, (byte)lastPlayerInternalId);
                    this.playerPositions.Add(msg.Guid, 1);

                    // Show the player on the start field
                    this.startProperty.ShowPlayer((byte)lastPlayerInternalId);
                    this.stackPlayer.Children.Add(new PlayerInfoView(msg.Guid, lastPlayerInternalId, msg.Name, msg.IsAI));
                }
                catch (System.Threading.Tasks.TaskCanceledException)
                {
                    // Application was exited before the message was processed
                    (DependencyService.Get<ILauncher>()).SoftShutdown();
                }
            }));
        }

        public bool GetUiActionMade()
        {
            // just used for our RMI impl
            return GameField.UiActionMade;
        }

        public UserChoice GetLastDialogResult()
        {
            // just used for our RMI impl
            return GameField.LastDialogResult;
        }


        /// <summary>
        /// Private method which is invoked everytime a <see cref="CardDrawnMessage"/> was received. It triggers the UI to display
        /// the card information on the view.
        /// </summary>
        /// <param name="msg">The received message, containing all necessary data.</param>
        private void onCardDrawn(CardDrawnMessage msg)
        {
            try
            {
                if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
                {
                    (DependencyService.Get<ILauncher>()).SoftShutdown();
                    return;
                }

                this.Dispatcher.Invoke(new Action(() =>
                {
                    Fhs.Dkt.Types.CardType cardType = msg.CardType;

                    CardControl cardField =
                        this.mainGrid.Children.OfType<CardControl>().Where((prop) => prop.CardType == cardType).FirstOrDefault();

                    cardField.ShowCard(msg.CardText);
                }));
            }
            catch (System.Threading.Tasks.TaskCanceledException)
            {
                // Application was exited before the message was processed
                (DependencyService.Get<ILauncher>()).SoftShutdown();
            }
        }

        /// <summary>
        /// Private method which is invoked everytime a <see cref="PropertyChangedMessage"/> was received. It updates the visual
        /// representation of the properties in the UI.
        /// </summary>
        /// <param name="msg">The received message, containing all necessary data.</param>
        private void onPropertyChanged(PropertyChangedMessage msg)
        {
            try
            {
                if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
                {
                    (DependencyService.Get<ILauncher>()).SoftShutdown();
                    return;
                }

                this.Dispatcher.Invoke(new Action(() =>
                {
                    // Get the property view
                    FieldControl concerningField =
                        this.mainGrid.Children.OfType<FieldControl>().Where((prop) => prop.Id == msg.Property.ID).FirstOrDefault();

                    if (concerningField == null)
                        throw new ArgumentException($"Invalid Property ID contained in the received {nameof(PropertyChangedMessage)}.");

                    concerningField.UpdateData(msg.Property);
                }));
            }
            catch (System.Threading.Tasks.TaskCanceledException)
            {
                // Application was exited before the message was processed
                (DependencyService.Get<ILauncher>()).SoftShutdown();
            }
        }

        /// <summary>
        /// Internal method to convert the given player ID to its internal ID.
        /// </summary>
        /// <param name="playerId">The ID of the player which shall be converted to the internal ID.</param>
        internal int? GetInternalPlayerId(Guid? playerId)
        {
            if (playerId == null)
                return null;

            if (!playerRelation.ContainsKey(playerId.Value))
                return null;

            return playerRelation[playerId.Value];
        }

        /// <summary>
        /// Private method which is invoked everytime a <see cref="PlayerMovedMessage"/> was received. It updates the player's position
        /// on the view.
        /// </summary>
        /// <param name="msg">The received message, containing all necessary data.</param>
        private void onPlayerMoved(PlayerMovedMessage msg)
        {
            try
            {
                if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
                {
                    (DependencyService.Get<ILauncher>()).SoftShutdown();
                    return;
                }

                this.Dispatcher.Invoke(new Action(() =>
                {
                // Get the internal player id from the player id
                Guid playerId = msg.PlayerId;
                    if (!playerRelation.ContainsKey(playerId))
                        throw new ArgumentException($"Invalid Player ID contained in the received {nameof(PlayerMovedMessage)}");

                    byte internalPlayerId = playerRelation[playerId];

                // Get the old field and remove the player
                FieldControl oldField = this.mainGrid.Children.OfType<FieldControl>().Where((x) => x.Id == msg.OldFieldId).FirstOrDefault();
                    FieldControl newField = this.mainGrid.Children.OfType<FieldControl>().Where((x) => x.Id == msg.NewFieldId).FirstOrDefault();

                    if (oldField == null || newField == null)
                        throw new ArgumentException($"Invalid Property ID contained in the received {nameof(PlayerMovedMessage)}.");

                    oldField.HidePlayer(internalPlayerId);
                    newField.ShowPlayer(internalPlayerId);

                    playerPositions[msg.PlayerId] = (byte)newField.Id;
                }));
            }
            catch (System.Threading.Tasks.TaskCanceledException)
            {
                // Application was exited before the message was processed
                (DependencyService.Get<ILauncher>()).SoftShutdown();
            }
        }

        /// <summary>
        /// Private method which is invoked everytime a <see cref="DicesRolledMessage"/> was received. It updates the displayed values
        /// of the dices on the UI.
        /// </summary>
        /// <param name="msg">The received message, containing all necessary data.</param>
        private void onDicesRolled(DicesRolledMessage msg)
        {
            try
            {
                if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
                {
                    (DependencyService.Get<ILauncher>()).SoftShutdown();
                    return;
                }

                this.Dispatcher.Invoke(new Action(() =>
                {
                    foreach (var cc in this.mainGrid.Children.OfType<CardControl>())
                    {
                        cc.HideCard();
                    }

                    this.dice1.Number = msg.FirstDiceNumber;
                    this.dice2.Number = msg.SecondDiceNumber;
                    this.inFieldMessageText = "onDicesRolled";
                }));
            }
            catch (System.Threading.Tasks.TaskCanceledException)
            {
                // Application was exited before the message was processed
                (DependencyService.Get<ILauncher>()).SoftShutdown();
            }
        }

        /// <summary>
        /// Asks the user for a decision. The result of the decision can be found in <see cref="GameField.LastDialogResult"/>.
        /// </summary>
        /// <param name="msg">A message for the user, which describes what she/he shall decide.</param>
        /// <param name="caption">A caption for the decision.</param>
        /// <param name="propertyId">If a valid property ID is passed here, a short description of a property will be shown in addition
        /// to the message given in <paramref name="msg"/>.</param>
        /// <param name="btnType">The decision possibilities for the user. Predefined decision combinations are available in
        /// <see cref="UserChoice"/>, however a logical OR can be used to select arbitrary permutations.</param>
        /// <param name="textYes">The text for the 'Yes' decision (Depending on the implementation e.g. displayed on a button).</param>
        /// <param name="textNo">The text for the 'No' decision (Depending on the implementation e.g. displayed on a button).</param>
        /// <param name="textCancel">The text for the 'Cancel' decision (Depending on the implementation e.g. displayed on a button).</param>
        public void AskForPropertyDecision(string msg, string caption = null, byte propertyId = 0, UserChoice btnType = UserChoice.Yes,
            string textYes = "Ja", string textNo = "Nein", string textCancel = "Abbrechen")
        {
            if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
            {
                (DependencyService.Get<ILauncher>()).SoftShutdown();
                return;
            }

            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                Console.WriteLine("##### AskForPropertyDecision start #####");

                var view = this.mainGrid.Children.OfType<FieldControl>().Where((x) => x.Id == propertyId).FirstOrDefault();

                this.decisionView.Show(msg,
                    caption: caption,
                    property: view?.PropertyClone,
                    buttons: btnType,
                    textYes: textYes,
                    textNo: textNo,
                    textCancel: textCancel);

                Console.WriteLine("##### AskForPropertyDecision end #####");
            }));
        }

        /// <summary>
        /// Asks the user to acknowledge to a informative message.
        /// </summary>
        /// <param name="visible"></param>
        /// <param name="msg">A message to display.</param>
        /// <param name="ackBtnText">The text on the acknowledge button which will be shown.</param>
        public void SetAcknowledgeVisuals(bool visible, string msg = null, string ackBtnText = null)
        {
            GameField.UiActionMade = false;
            if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
            {
                (DependencyService.Get<ILauncher>()).SoftShutdown();
                return;
            }

            // Ensure that the UI access occurrs only on the UI thread:
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                Console.WriteLine("##### SetAcknowledgeVisuals started #####");

                if (visible)
                {
                    this.inFieldMessageText = msg;
                    this.btnBetweenDice.Content = ackBtnText;
                    this.btnBetweenDice.Visibility = Visibility.Visible;
                }
                else
                {
                    this.inFieldMessageText = "";
                    this.btnBetweenDice.Visibility = Visibility.Hidden;
                }

                this.btnBetweenDice.InvalidateVisual();

                Console.WriteLine("##### SetAcknowledgeVisuals ended #####");
            }));
        }

        /// <summary>
        /// Asks the user to acknowledge that a dice will be rolled after confirming. The dice will not be rolled by this method,
        /// the caller is responsible to do so afterwards.
        /// <param name="show">Indicates if the button shall be shown or shall be hidden.</param>
        /// <param name="text">The text of the button.</param>
        public void ShowRollDiceButton(bool show, string text = null)
        {
            GameField.UiActionMade = false;
            if (Dispatcher.HasShutdownStarted || Dispatcher.HasShutdownFinished)
            {
                (DependencyService.Get<ILauncher>()).SoftShutdown();
                return;
            }

            // Ensure that the UI access occurrs only on the UI thread:
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
            {
                Console.WriteLine("##### ShowrolldiceButton started #####");


                if (show)
                {
                    this.btnBetweenDice.Visibility = Visibility.Visible;
                    this.btnBetweenDice.Content = "Würfeln!";
                    inFieldMessageText = text;
                }
                else
                {
                    this.btnBetweenDice.Visibility = Visibility.Hidden;
                    inFieldMessageText = string.Empty;
                }

                this.btnBetweenDice.InvalidateVisual();
                Console.WriteLine("##### ShowrolldiceButton ended #####");
            }));
        }

        /// <summary>
        /// Private event handler which is invoked after the button shown between the dices in the UI is clicked.
        /// </summary>
        /// <param name="sender">The button (shown between the dices in the UI) which sent the event.</param>
        /// <param name="e">The event arguments.</param>
        private void onBtnBetweenDicesClicked(object sender, RoutedEventArgs e)
        {
            // This method will ever be called from the UI thread
            GameField.UiActionMade = true;
            this.btnBetweenDice.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Gets or sets the 'infield' text displayed directly on the visual representation of the game field.
        /// </summary>
        string inFieldMessageText
        {
            get => (string)GetValue(bigMessageTextProperty);
            set
            {
                SetValue(bigMessageTextProperty, value);
            }
        }

        /// <summary>
        /// Private dependency property, which is used to keep track of changes regarding the <see cref="inFieldMessageText"/>.
        /// </summary>
        static readonly DependencyProperty bigMessageTextProperty = DependencyProperty.Register(nameof(inFieldMessageText), typeof(string), typeof(GameView));

        /// <summary>
        /// Private event handler which is invoked if the window is closed. Note that this will also request the other components to
        /// shutdown (calling <see cref="ILauncher.SoftShutdown"/>.
        /// </summary>
        /// <param name="sender">The closing window which triggered the event.</param>
        /// <param name="e">The event args.</param>
        private void Window_Closing(object sender, EventArgs e)
        {
            this.Dispatcher.InvokeShutdown();
            DependencyService.Get<ILauncher>().SoftShutdown();
        }

        /// <summary>
        /// Private event handler which is invoked if the window is closed by clicking the 'Quit Game' button. Note that this will also
        /// request the other components to shutdown (calling <see cref="ILauncher.SoftShutdown"/>.
        /// </summary>
        /// <param name="sender">The button which triggered the event.</param>
        /// <param name="e">The event args.</param>
        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.InvokeShutdown();
            DependencyService.Get<ILauncher>().SoftShutdown();
        }
    }
}
