﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Windows;
using System.Threading;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Controls;
using System.Collections.ObjectModel;

using Fhs.Dkt.Messaging;
using Fhs.Dkt.Interfaces;
using System.Windows.Media;


/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// WPF Control which is responsible for the initial process of creating / adding players. Note that those players are just local
    /// pseudo players, the real players are held by the <see cref="IGameController"/> instance and cannot be accessed from outside it.
    /// Created by Mathias Lackner.
    /// </summary>
    public partial class CreatePlayersControl : UserControl
    {
        /// <summary>
        /// Creates a new <see cref="CreatePlayersControl"/> to be displayed in the UI. It is used in the <see cref="GameView"/> to 
        /// create new players. Note that those players are just local pseudo players, the real players are held by the
        /// <see cref="IGameController"/> implementation and cannot be accessed from out it.
        /// </summary>
        public CreatePlayersControl()
        {
            InitializeComponent();
            this.DataContext = this;

            this.tbRounds.TextChanged += updateStartGameEnabledAndHighlighting;
            this.cbUseRoundLimit.Checked += updateStartGameEnabledAndHighlighting;
            this.cbUseRoundLimit.Unchecked += updateStartGameEnabledAndHighlighting;

            this.btnStartGame.Click += (sender, args) =>
            {
                foreach (PseudoPlayer p in this.PlayersToCreate)
                    this.Notify(new PlayerCreatedMessage(p.PlayerName, Guid.NewGuid(), p.IsAI));

                this.Visibility = Visibility.Collapsed;

                Thread workerThread = new Thread(new ThreadStart(() =>
                {
                    IGameController mgr = DependencyService.Get<IGameController>();
                    mgr.Start(this.rounds);
                }));
                workerThread.Start();
            };
        }

        /// <summary>
        /// Private field to temporarily store the number of rounds to play. This value is submitted to the <see cref="IGameController"/>
        /// via its <see cref="IGameController.Start(int)"/> method.
        /// </summary>
        int rounds = 20;

        /// <summary>
        /// This class represents the pseudo players in the UI. The real players are held by the <see cref="IGameController"/> implementation
        /// and cannot be accessed from outside it.
        /// </summary>
        public class PseudoPlayer : DependencyObject, INotifyPropertyChanged
        {
            /// <summary>
            /// Gets or sets the players name.
            /// </summary>
            public string PlayerName
            {
                get => (string)GetValue(PlayerNameProperty);
                set
                {
                    SetValue(PlayerNameProperty, value);
                    onPropertyChanged(nameof(PlayerName));
                }
            }

            /// <summary>
            /// Gets or sets if the player is an AI.
            /// </summary>
            public bool IsAI
            {
                get => (bool)GetValue(IsAIProperty);
                set
                {
                    SetValue(IsAIProperty, value);
                    onPropertyChanged(nameof(IsAI));
                }
            }

            /// <summary>
            /// Private method to fire PropertyChanged events for the <see cref="PseudoPlayer"/> class.
            /// </summary>
            /// <param name="prop">The name of the property which changed.</param>
            void onPropertyChanged(string prop)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }

            /// <summary>
            /// The event handler which fires if a property of the <see cref="PseudoPlayer"/> class changes.
            /// </summary>
            public event PropertyChangedEventHandler PropertyChanged;

            /// <summary>
            /// Private dependency property to keep track of changes regarding the <see cref="PlayerName"/> property.
            /// </summary>
            static readonly DependencyProperty PlayerNameProperty = DependencyProperty.Register(nameof(PlayerName), typeof(string), typeof(CreatePlayersControl));

            /// <summary>
            /// Private dependency property to keep track of changes regarding the <see cref="IsAI"/> property.
            /// </summary>
            static readonly DependencyProperty IsAIProperty = DependencyProperty.Register(nameof(IsAI), typeof(bool), typeof(CreatePlayersControl));

        }

        /// <summary>
        /// An observable collection holding all the players which are created/modified via the <see cref="CreatePlayersControl"/>.
        /// </summary>
        public ObservableCollection<PseudoPlayer> PlayersToCreate { get; } = new ObservableCollection<PseudoPlayer>()
        {
            new PseudoPlayer() { PlayerName = "I'm human!", IsAI = false },
            new PseudoPlayer() { PlayerName = "Wall-E", IsAI = true },
            new PseudoPlayer() { PlayerName = "Terminator", IsAI = true }
        };


        /// <summary>
        /// Private event handler which is invoked after the 'delete' image was clicked.
        /// </summary>
        /// <param name="sender">The sending control (In this case, an Image-Control).</param>
        /// <param name="e">The arguments of the event.</param>
        private void imgDelete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.lvPlayers.SelectedItem is PseudoPlayer selected)
                this.PlayersToCreate.Remove(selected);

            updateVisibilities();
        }

        /// <summary>
        /// Private event handler which is invoked after the 'add' image was clicked.
        /// </summary>
        /// <param name="sender">The sending control (In this case, an Image-Control).</param>
        /// <param name="e">The arguments of the event.</param>
        private void imgAdd_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.PlayersToCreate.Add(new PseudoPlayer() { PlayerName = "Neuer Spieler", IsAI = false });
            updateVisibilities();
        }


        /// <summary>
        /// Private method to update the visibilty of the controls to remove/add players.
        /// </summary>
        private void updateVisibilities()
        {
            if (this.PlayersToCreate.Count > 5)
                this.imgAdd.Visibility = Visibility.Collapsed;
            else
                this.imgAdd.Visibility = Visibility.Visible;

            if (this.PlayersToCreate.Count < 3 || this.lvPlayers.SelectedIndex == -1)
                this.imgDelete.Visibility = Visibility.Collapsed;
            else
                this.imgDelete.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Private event handler which is invoked after the selected player changed.
        /// </summary>
        /// <param name="sender">The sending control (In this case, a ListView).</param>
        /// <param name="e">The arguments of the event.</param>
        private void lvPlayers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateVisibilities();
        }

        /// <summary>
        /// Private event handler which is invoked after the number of rounds to play or the assiciated checkbox changed. It is used
        /// to indicate if the input is erroneous and highlight the appropriate controls accordingly. 
        /// </summary>
        /// <param name="sender">The sender of the event (in this case a checkbox or a textbox).</param>
        /// <param name="args">The arguments of the event.</param>
        private void updateStartGameEnabledAndHighlighting(object sender, EventArgs args)
        {
            bool isChecked = this.cbUseRoundLimit.IsChecked.Value;

            if (isChecked)
                this.tbRounds.IsEnabled = true;
            else
                this.tbRounds.IsEnabled = false;

            void update(bool ok, int roundsToSet)
            {
                this.rounds = roundsToSet;

                if (ok)
                {
                    this.tbRounds.Background = Brushes.White;
                    this.tbRounds.Foreground = Brushes.Black;

                    this.btnStartGame.IsEnabled = true;
                }
                else
                {
                    this.tbRounds.Background = Brushes.Crimson;
                    this.tbRounds.Foreground = Brushes.White;

                    this.btnStartGame.IsEnabled = false;
                }
            }

            if (isChecked && ushort.TryParse(this.tbRounds.Text, out ushort parseResult))
                update(true, parseResult);
            else if (isChecked)
                update(false, -1);
            else
                update(true, -1);
        }
    }
}