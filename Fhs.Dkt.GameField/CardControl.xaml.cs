﻿// CREATED BY LUKAS BRUGGER
using System.Windows;
using System.Windows.Controls;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// UserControl to show information on the GameField about a card that was drawn by the user.
    /// There are 2 instances of this UserControl on the Field, one for bank cards and the other for risk cards. 
    /// The type of card is passed to the component through a property in the xaml. 
    /// The UserControl contains a TextBlock, which is used to show the card description to the user by setting the text content 
    /// and showing or hiding it from the user on the GameField.
    /// Created by Lukas Brugger.
    /// </summary>
    public partial class CardControl : UserControl
    {
        /// <summary>
        /// Instantiates a CardControl object. 
        /// Declares and sets all properties of the UserControl to a default value
        /// </summary>
        public CardControl()
        {
            InitializeComponent();
            this.DataContext = this;
            this.TypeLabel = "";
            this.CardText = "";
            this.CardType = Types.CardType.BankCard;
        }

        /// <summary>
        /// CardType property, that determines, which type of cards the UserControl shows to the user.
        /// Value is set by parameters in the xaml markup when including the CardControl in a layout
        /// </summary>
        public Fhs.Dkt.Types.CardType CardType
        {
            get => (Fhs.Dkt.Types.CardType)GetValue(CardTypeProperty);
            set => SetValue(CardTypeProperty, value);
        }

        /// <summary>
        /// TypeLabel property is used to show, which type of cards are shown in a used CardControl
        /// and is set by a parameter in the xaml markup when the CardControl is included in a layout.
        /// This value is shown to the user in the user interface
        /// </summary>
        public string TypeLabel
        {
            get => (string)GetValue(TypeLabelProperty);
            set => SetValue(TypeLabelProperty, value);
        }

        /// <summary>
        /// CardText property, which is set when a card is drawn to show the text of the drawn card
        /// to the user in the user interface
        /// </summary>
        public string CardText
        {
            get => (string)GetValue(CardTextProperty);
            set => SetValue(CardTextProperty, value);
        }

        /// <summary>
        /// Method sets the passed string as message to the Card UserControl "CardText" property and then shows the card by setting the
        /// Visibility of the "CardTextElement" in the Control to visible
        /// </summary>
        /// <param name="cardMessage">Text to be shown to the user in the CardControl</param>
        public void ShowCard(string cardMessage)
        {
            CardText = cardMessage;
            cardTextElement.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Method clears the "CardText" property and hides the CardControl layer by setting the Visibility of the "CardTextElement" in 
        /// the Control to hidden
        /// </summary>
        public void HideCard()
        {
            CardText = "";
            cardTextElement.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// CardType property, that determines, which type of cards the UserControl shows to the user.
        /// Value is set by parameters in the xaml markup when including the CardControl in a layout
        /// </summary>
        /// <remarks><para>This dependency property can be accessed via the <see cref="CardType"/> property.</para></remarks>
        private static readonly DependencyProperty CardTypeProperty = DependencyProperty.Register(nameof(CardType), typeof(Fhs.Dkt.Types.CardType), typeof(CardControl));

        /// <summary>
        /// TypeLabel property is used to show, which type of cards are shown in a used CardControl
        /// and is set by a parameter in the xaml markup when the CardControl is included in a layout.
        /// This value is shown to the user in the user interface
        /// </summary>
        /// <remarks><para>This dependency property can be accessed via the <see cref="CardText"/> property.</para></remarks>
        private static readonly DependencyProperty CardTextProperty = DependencyProperty.Register(nameof(CardText), typeof(string), typeof(CardControl));

        /// <summary>
        /// CardText property, which is set when a card is drawn to show the text of the drawn card
        /// to the user in the user interface
        /// </summary>
        /// <remarks><para>This dependency property can be accessed via the <see cref="TypeLabel"/> property.</para></remarks>
        private static readonly DependencyProperty TypeLabelProperty = DependencyProperty.Register(nameof(TypeLabel), typeof(string), typeof(CardControl));
    }
}
