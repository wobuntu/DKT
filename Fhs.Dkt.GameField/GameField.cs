﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Messaging;
using System.Threading;
using System.Windows.Threading;
using Fhs.Dkt.Types;

[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.GameField))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This class is the implementation for the <see cref="IGameField"/> component of this solution.
    /// The game field is responsible to control the visual output for the user and also to allow the other components the request of user input.
    /// Created by Mathias Lackner.
    /// </summary>
    public class GameField : IGameField
    {
        public GameField()
        {
            this.SubscribeTo<ApplicationShutdownMessage>((msg) =>
            {
                this.UnsubscribeAll();
                this.shuttingDown = true;
                this.Close();
            });
            //this.Notify(new LogOnlyMessage("Current thread: " + Thread.CurrentThread.ManagedThreadId, LogLvl.Debug));
        }

        private GameView view = null;

        #region IGameField Implementation
        public void Close()
        {
            if (view == null || view.Dispatcher.HasShutdownStarted || view.Dispatcher.HasShutdownFinished)
                return;

            view.Dispatcher.BeginInvoke(new Action(()=>
            {
                shuttingDown = true;

                view.Close();
                view = null;
            }));
        }

        /// <summary>
        /// Requests the game field to start the visual reprenstation for the user. In this implementation, this triggers a WPF window
        /// to be shown (<see cref="GameView"/>), which is controlled via the <see cref="GameField"/>.
        /// </summary>
        public void Show()
        {
            if (view != null)
                return;

            shuttingDown = false;

            // Since this component was called from the GameEngine, we have to ensure that blocking operations won't block the UI.
            // The simplest approach here is to create a new thread on which the UI runs.
            Thread uiThread = new Thread(new ThreadStart(() =>
            {
                SynchronizationContext.SetSynchronizationContext(SynchronizationContext.Current);

                view = new GameView();
                view.Closed += (sender, args) =>
                {
                    Dispatcher.CurrentDispatcher.InvokeShutdown();
                };
                view.Show();

                //this.Notify(new LogOnlyMessage("Current thread: " + Thread.CurrentThread.ManagedThreadId, LogLvl.Debug));
                Dispatcher.Run();
            }));

            uiThread.SetApartmentState(ApartmentState.STA);
            uiThread.Start();
        }

        /// <summary>
        /// Internal variable to keep track of user inputs. It is required for blocking calls from the game loop or other components.
        /// </summary>
        public static bool  UiActionMade = false;
        /// <summary>
        /// Internal variable to store the last user decision.
        /// </summary>
        internal static UserChoice LastDialogResult = UserChoice.Cancel;
        /// <summary>
        /// Internal variable to indicate, that the <see cref="GameField"/> is shutting down (used for a soft-shutdown of the different
        /// Threads).
        /// </summary>
        private bool shuttingDown = false;

        /// <summary>
        /// Constant value in milliseconds for pausing threads.
        /// </summary>
        const int _pause = 200;

        /// <summary>
        /// Asks the user for a decision. It forwards the request to the <see cref="GameView"/>, which then opens a
        /// <see cref="DecisionView"/> inside it in which the user can make a choice.
        /// </summary>
        /// <param name="msg">A message for the user, which describes what she/he shall decide.</param>
        /// <param name="caption">A caption for the decision.</param>
        /// <param name="propertyId">If a valid property ID is passed here, a short description of a property will be shown in addition
        /// to the message given in <paramref name="msg"/>.</param>
        /// <param name="buttons">The decision possibilities for the user. Predefined decision combinations are available in
        /// <see cref="UserChoice"/>, however a logical OR can be used to select arbitrary permutations.</param>
        /// <param name="textYes">The text for the 'Yes' decision (Depending on the implementation e.g. displayed on a button).</param>
        /// <param name="textNo">The text for the 'No' decision (Depending on the implementation e.g. displayed on a button).</param>
        /// <param name="textCancel">The text for the 'Cancel' decision (Depending on the implementation e.g. displayed on a button).
        /// </param>
        public UserChoice WaitForDecision(string msg, string caption = null, byte propertyId = 0, UserChoice buttons = UserChoice.Yes, 
            string textYes = "Ja", string textNo = "Nein", string textCancel = "Abbrechen")
        {
            UiActionMade = false;
            
            // Note: The view runs on a different thread then this one. AskForPropertyDecision ensures itself that it executes only on the
            // UI thread, so we can call it without using a dispatcher.
            view.AskForPropertyDecision(msg,
                caption: caption,
                propertyId: propertyId,
                btnType: buttons,
                textYes: textYes,
                textNo: textNo,
                textCancel: textCancel);

            while (!UiActionMade)
            {
                Thread.Sleep(_pause);
                if (shuttingDown)
                    return UserChoice.Cancel;
            }

            return LastDialogResult;
        }


        /// <summary>
        /// Asks the user to respond to a informative message. It forwards the request to the <see cref="GameView"/>, which is then
        /// displaying a message directly on the visual representation of the game along with an OK-button to acknowledge.
        /// </summary>
        /// <param name="text">A message to display during waiting for the user confirmation.</param>
        /// <param name="maxTimeout">The maximum amount of milliseconds to wait for a user input. Pass -1 to wait indefinitely long.
        /// </param>
        public void WaitForAcknowledge(string msg, int maxTimeout = -1)
        {
            if (maxTimeout > -1 && maxTimeout % 1000 != 0)
                throw new Exception($"Only multiples of 1000 (or -1) are allowed to pass to {nameof(maxTimeout)}");

            if (view == null)
                throw new Exception($"The view must be shown before the method {nameof(WaitForAcknowledge)} can be called.");

            // Note: The view runs on a different thread then this one. WaitForUserAcknowledge ensures itself that it executes only on the
            // UI thread, so we can call it without using a dispatcher.
            view.SetAcknowledgeVisuals(true, msg, "OK" + (maxTimeout != -1 ? $" [{maxTimeout / 1000}]" : ""));

            UiActionMade = false;

            int remainingMs = maxTimeout;

            while (!UiActionMade)
            {
                Thread.Sleep(_pause);

                if (shuttingDown)
                    return;

                if (maxTimeout > -1)
                {
                    remainingMs -= _pause;

                    if (remainingMs <= 0)
                        break;

                    if (remainingMs % 1000 == 0)
                        view.SetAcknowledgeVisuals(true, msg, $"OK [{remainingMs / 1000}]");
                }
            }

            if (shuttingDown)
                return;

            view.SetAcknowledgeVisuals(false);
        }

        bool GetUiActionMade()
        {
            return UiActionMade;
        }

        UserChoice GetLastDialogResult()
        {
            return LastDialogResult;
        }

        /// <summary>
        /// Asks the user to acknowledge that a dice will be rolled after confirming. This will be forwarded to the <see cref="GameView"/>,
        /// which displays the message. The dice will not be rolled by this method or the <see cref="GameView"/>, the caller is
        /// responsible to do so after this method returned.
        /// </summary>
        /// <param name="maxTimeout">The maximum amount of milliseconds to wait for a user input. Pass -1 to wait indefinitely long.
        /// </param>
        public void WaitForRollDiceAcknowledge(int maxTimeout = -1)
        {
            if (maxTimeout > -1 && maxTimeout % 1000 != 0)
                throw new Exception($"Only multiples of 1000 (or -1) are allowed to pass to {nameof(maxTimeout)}");

            if (view == null)
                throw new Exception($"The view must be shown before the method {nameof(WaitForRollDiceAcknowledge)} can be called.");

            // Note: The view runs on a different thread then this one. ShowRollDiceButton ensures itself that it executes only on the
            // UI thread, so we can call it without using a dispatcher.
            view.ShowRollDiceButton(true);

            int remainingMs = maxTimeout;

            while (!UiActionMade)
            {
                Thread.Sleep(_pause);

                if (shuttingDown)
                    return;

                if (maxTimeout > -1)
                {
                    remainingMs -= _pause;

                    if (remainingMs <= 0)
                        // Max timeout ended, the dices shall disappear automatically
                        break;

                    if (remainingMs % 1000 == 0)
                        view.ShowRollDiceButton(true, (remainingMs / 1000).ToString());
                }
            }

            if (shuttingDown)
                return;

            view.ShowRollDiceButton(false);
        }
        #endregion
    }
}
