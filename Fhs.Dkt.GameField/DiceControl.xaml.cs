﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Controls;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>UserControl used to represent a visual dice. Created by Mathias Lackner.</summary>
    public partial class DiceControl : UserControl
    {
        /// <summary>
        /// Creates a new instance of the DiceControl, which is a visual representation of a dice for the user interface.
        /// </summary>
        public DiceControl()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Number = 5;
        }

        /// <summary>
        /// This value represents the displayed eyes for the <see cref="DiceControl"/>.
        /// </summary>
        public int Number
        {
            get { return (int)GetValue(NumberProperty); }
            set
            {
                switch (value)
                {
                    case 1:
                        setVisibility(4); break;
                    case 2:
                        setVisibility(0, 8); break;
                    case 3:
                        setVisibility(0, 4, 8); break;
                    case 4:
                        setVisibility(0, 2, 6, 8); break;
                    case 5:
                        setVisibility(0, 2, 4, 6, 8); break;
                    case 6:
                        setVisibility(0, 2, 3, 5, 6, 8); break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(Number), "The dice number must be a value between 1 and 6.");
                }
                
                SetValue(NumberProperty, value);
            }
        }

        /// <summary>
        /// Private method to set the visibility of the eyes on the dice based on the index of the regarding ellipse. Note that no error
        /// checks are done here, since the method is only called from within this class (throws exceptions).
        /// </summary>
        /// <param name="visibleEyes">An array with the eye indexes, which shall be set visible.</param>
        private void setVisibility(params int[] visibleEyes)
        {
            foreach (Ellipse ellipse in gridDice.Children)
                ellipse.Visibility = Visibility.Hidden;

            foreach (int index in visibleEyes)
            {
                gridDice.Children[index].Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// The dependency property for updating the number of visible eyes.
        /// </summary>
        static readonly DependencyProperty NumberProperty = DependencyProperty.Register(nameof(Number), typeof(int), typeof(FieldControl));
    }
}
