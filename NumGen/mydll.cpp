#define MYDLL_EXPORTS
#include "mydll.h"
#include <iostream>
#include <random>

// Created by Boris Brankovic and Gabriel Dax

//declaring namespace std
using namespace std;

//initializing "random_device" and mt19937 for use in the function numGenUniformUINT
std::random_device rd;
std::mt19937 mt(rd());

/**
* @brief function to check the dll
* @return always true
*/
bool DllTestFunktion()
{
	return true;
}

/**
* @brief function to check the dll
*/
void Test()
{
	
}

/**
* @brief function to check which flag is set.
*
* @param bitsToCheck represents the bits to check and is from type Alphabet
* @param flag is from type integer
* @return true if the flag is checkt in the bitsToCheck
*/
bool checkTypeFlag(Alphabet bitsToCheck, int flag)
{
	return (bitsToCheck & flag) == flag;
}

/**
* @brief chooses random Characters of a user-defined alphabet
*
* The user defines a customized alphabet and based on a uniform distribution
* a random order will be generated.
* Additional futures are: Choosing how often the random order should be generated.
*
* The princip relays on creating random numbers with a chosen distribution.
* An Random Engine in combination with a Random Device creates random numbers in a chosen range.
* The benefit of this mechanism is the big range of the Engine. In this case it is 2^32.
*
* @param stringBuffer is a char* array and the buffer for the new generated characters / random order.
* @param stringLength is an integer and the buffer length.
* @param tempString is the user-defined alphabet which is passed.
* @param templength is the length of the alphabet and is used for the range of the random order.
* @return integer as error handler -> 0 means Success | --> -1 Error
*/
int alphabetGen(char * stringBuffer, int stringLength, char * tempString, int tempLength)
{
	try
	{

		std::random_device rd;
		std::mt19937 mt(rd());

		std::uniform_int_distribution<int> dist(0, tempLength - 1);

		for (int i = 0; i<stringLength; i++)
		{
			stringBuffer[i] = tempString[dist(mt)];
		}
		stringBuffer[stringLength] = '\0';
		return 0;
	}
	catch (const std::exception e)
	{
		return -1;
	}
}

/**
* @brief chooses random Characters of a pre-defined alphabet
*
* Generates random order of different predefined alphabets.
* The pre-defined alphabets consists of 26 Characters of the latin-alphabet.
* They are two possibilities: lower and upper case.
*
* The principe of generating random orders (i.e. random numbers which are in between the range of the
* array of the pre-defined alphabet) relays on the same principe as in the function "alphabetGen2".
*
* @param stringBuffer is a char* array and the buffer for the new generated characters / random order.
* @param stringLength is an integer and the buffer length.
* @param alpha is an integer which handles the choice of the user (which alphabet to choose).
* @return integer as error handler -> 0 means Success | --> -1 Error
*/
int alphabetGen2(char * stringBuffer, int stringLength, int alpha)
{
	try
	{
		char newArr[] = { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','\0' };
		char newArr2[] = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','\0' };
		int length1 = std::strlen(newArr);
		int length2 = std::strlen(newArr2);

		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_int_distribution<int> dist1(0, length1 - 1);

		std::uniform_int_distribution<int> dist2(0, length2 - 1);

		switch (alpha)
		{
		case 1:
			for (int i = 0; i<stringLength; i++)
			{
				stringBuffer[i] = newArr[dist1(mt)];
			}
			break;
		case 2:
			for (int i = 0; i<stringLength; i++)
			{
				stringBuffer[i] = newArr2[dist2(mt)];
			}
			break;
		}
		stringBuffer[stringLength] = '\0';
		return 0;
	}
	catch (exception e)
	{
		return -1;
	}
}

/**
* @brief creates random numbers based on a uniform distribution
*
* Generates random numbers based on a uniform distribution.
* The values are Integers and the user can choose a range, to create a random number in between these values.
* An additional feature is that the user also can choose how many random numbers should be generated.
*
*
* @param bufferArray is an integer pointer-Array. The generated random numbers are stored inside this buffer.
* @param min represents the minimum of the range for the generated random numbers.
* @param max represents the maximum of the range for the generated random numbers.
* @param data - this value has the information, how many random numbers should be generated.
* @return integer as error handler -> 0 means Success | --> -1 Error
*/
int numGenUniform(int* bufferArray, int min, int max, int data)
{
	try
	{
		std::random_device rd;
		std::mt19937 mt(rd());

		std::uniform_int_distribution<int> dist1(min, max);
		for (int i = 0; i<data; i++)
		{
			*(bufferArray + i) = dist1(mt);
		}

		bufferArray[data] = '\0';
		return 0;
	}
	catch (std::exception ex)
	{
		return -1;
	}
}

/**
* @brief creates random numbers based on a uniform distribution
*
* Generates random numbers based on a uniform distribution.
* The values are Unsinged Integers and the user can choose a range, to create a random number in between these values.
* An additional feature is that the user also can choose how many random numbers should be generated.
*
*
* @param bufferArray is an unsigned integer pointer-Array. The generated random numbers are stored inside this buffer.
* @param min represents the minimum of the range for the generated random numbers.
* @param max represents the maximum of the range for the generated random numbers.
* @param data - this value has the information, how many random numbers should be generated.
* @return integer as error handler -> 0 means Success | --> -1 Error
*/
uint numGenUniformUINT(uint* bufferArray, uint min, uint max, int data)
{
	try
	{
		std::uniform_int_distribution<uint> dist1(min,max);

		for (int i = 0; i<data; i++)
		{
			*(bufferArray + i) = dist1(mt);
		}
		bufferArray[data] = '\0';
		return 0;
	}
	catch (std::exception ex)
	{
		return -1;
	}
}

/**
* @brief creates random numbers based on a normal distribution.
*
* Generates random numbers based on a normal distribution.
* The values are Double value. the user can set the expected value (my), and the standard deviation (sigma).
* Random numbers based on the principe of a normal distribution will be generated.
*
*
* @param buffer is an double pointer-Array. The generated random numbers are stored inside this buffer.
* @param my represents the expected value.
* @param sigma represents the standard deviation.
* @param data - this value has the information, how many random numbers should be generated.
* @return integer as error handler -> 0 means Success | --> -1 Error
*/
int numGenNormal(double* buffer, double my, double sigma, int data)
{
	try
	{
		std::random_device rd;
		std::mt19937 mt(rd());

		std::normal_distribution<double> dist1(my, sigma);
		for (int i = 0; i<data; i++)
		{
			*(buffer + i) = dist1(mt);
		}
		buffer[data] = '\0';
		return 0;
	}
	catch (exception ex)
	{
		return -1;
	}
}

/**
* @brief Generates a password
*
* Generates a password with diffrent predefined alphabets.
*
* @param stringBuffer is a char* array and the password buffer.
* @param stringLength is an integer and the buffer length.
* @param type is from type Alphabet and the passw. character type
* @return integer as error handler
*/
int PasswordGenerator(char * stringBuffer, int stringLength, Alphabet type)
{
	if (stringLength <= 0)
		return -1;
	else if (!stringBuffer)
		return -2;
	else if (((int)type <= 0) && (type == NULL))
		return -3;
	
	string numbers		= "0123456789";
	string lower		= "abcdefghijklmnopqrstuvwxyz";
	string brackets		= "()[]{}<>";
	string additionCa	= "!�$%&/=?+~*#|@�";
	string upper		= "";
	string randStr		= "";
	
	for (size_t i = 0; i < lower.length(); i++)
		upper += toupper(lower.at(i));

	if (checkTypeFlag(type, Alphabet::Uppercase))
		randStr += upper;
	if (checkTypeFlag(type, Alphabet::Lowercase))
		randStr += lower;
	if (checkTypeFlag(type, Alphabet::Numbers))
		randStr += numbers;
	if (checkTypeFlag(type, Alphabet::Brackets))
		randStr += brackets;
	if (checkTypeFlag(type, Alphabet::AdditionalChar))
		randStr += additionCa;
	
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(0, (randStr.length() - 1));
	
	for (int i = 0; i<stringLength; i++)
	{
		stringBuffer[i] = randStr[dist(mt)];
	}
	stringBuffer[stringLength] = '\0';
	return 0;
}