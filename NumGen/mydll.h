#pragma once
// mydll.h
// Created by Boris Brankovic and Gabriel Dax

/**
* @brief Export Definitions for the DLL
*
* @param __declspec(export) keyword for exporting functions from a DLL to other applications
* @param __declspec(import) keyword for importing functions into an application
*
*/
#ifdef MYDLL_EXPORTS  
#define MYDLL_API __declspec(dllexport)   
#else  
#define MYDLL_API __declspec(dllimport)   
#endif 

/**
* @brief Function definitions
*
* in this section the functions will be marked as "extern C".
* this has the functionality to "represent" the functions as C interface for other applications, which are not C/C++ apps, i.e. C# Assemblys.
*/
#ifdef __cplusplus  
extern "C" { 
#endif  

	typedef unsigned int uint;

	/**
	* @brief Predefined alphabets for the password generation
	*
	* Enum to set the predefined alphabets for the passwort generator
	*/
	enum Alphabet
	{
		Uppercase		= 1,
		Lowercase		= Uppercase << 1,
		Numbers			= Uppercase << 2,
		Brackets		= Uppercase << 3,
		AdditionalChar	= Uppercase << 4
	};

	/** 
	* @brief function to check the dll
	* @return always true
	*/
	MYDLL_API bool DllTestFunktion();
	
	/**
	* @brief function to check the dll
	*/
	MYDLL_API void Test();

	/**
	* @brief function to check which flag is set.
	*
	* @param bitsToCheck represents the bits to check and is from type Alphabet
	* @param flag is from type integer
	* @return true if the flag is checkt in the bitsToCheck
	*/
	MYDLL_API bool checkTypeFlag(Alphabet bitsToCheck, int flag);

	/**
	* @brief chooses random Characters of a user-defined alphabet
	*
	* The user defines a customized alphabet and based on a uniform distribution
	* a random order will be generated.
	* Additional futures are: Choosing how often the random order should be generated.
	*
	* The princip relays on creating random numbers with a chosen distribution.
	* An Random Engine in combination with a Random Device creates random numbers in a chosen range.
	* The benefit of this mechanism is the big range of the Engine. In this case it is 2^32.
	*
	* @param stringBuffer is a char* array and the buffer for the new generated characters / random order.
	* @param stringLength is an integer and the buffer length.
	* @param tempString is the user-defined alphabet which is passed.
	* @param templength is the length of the alphabet and is used for the range of the random order.
	* @return integer as error handler -> 0 means Success | --> -1 Error
	*/
	MYDLL_API int alphabetGen(char* stringBuffer, int stringLength, char* tempString, int tempLength);

	/**
	* @brief chooses random Characters of a pre-defined alphabet
	*
	* Generates random order of different predefined alphabets.
	* The pre-defined alphabets consists of 26 Characters of the latin-alphabet.
	* They are two possibilities: lower and upper case.
	*
	* The principe of generating random orders (i.e. random numbers which are in between the range of the
	* array of the pre-defined alphabet) relays on the same principe as in the function "alphabetGen2".
	*
	* @param stringBuffer is a char* array and the buffer for the new generated characters / random order.
	* @param stringLength is an integer and the buffer length.
	* @param alpha is an integer which handles the choice of the user (which alphabet to choose).
	* @return integer as error handler -> 0 means Success | --> -1 Error
	*/
	MYDLL_API int alphabetGen2(char* stringBuffer, int stringLength, int alpha);

	/**
	* @brief creates random numbers based on a uniform distribution
	*
	* Generates random numbers based on a uniform distribution.
	* The values are Integers and the user can choose a range, to create a random number in between these values.
	* An additional feature is that the user also can choose how many random numbers should be generated.
	*
	*
	* @param bufferArray is an integer pointer-Array. The generated random numbers are stored inside this buffer.
	* @param min represents the minimum of the range for the generated random numbers.
	* @param max represents the maximum of the range for the generated random numbers.
	* @param data - this value has the information, how many random numbers should be generated.
	* @return integer as error handler -> 0 means Success | --> -1 Error
	*/
	MYDLL_API int numGenUniform(int* bufferArray, int min, int max, int data);

	/**
	* @brief creates random numbers based on a uniform distribution
	*
	* Generates random numbers based on a uniform distribution.
	* The values are Unsinged Integers and the user can choose a range, to create a random number in between these values.
	* An additional feature is that the user also can choose how many random numbers should be generated.
	*
	*
	* @param bufferArray is an unsigned integer pointer-Array. The generated random numbers are stored inside this buffer.
	* @param min represents the minimum of the range for the generated random numbers.
	* @param max represents the maximum of the range for the generated random numbers.
	* @param data - this value has the information, how many random numbers should be generated.
	* @return integer as error handler -> 0 means Success | --> -1 Error
	*/
	MYDLL_API uint numGenUniformUINT(uint* bufferArray, uint min, uint max, int data);

	/**
	* @brief creates random numbers based on a normal distribution.
	*
	* Generates random numbers based on a normal distribution.
	* The values are Double value. the user can set the expected value (my), and the standard deviation (sigma).
	* Random numbers based on the principe of a normal distribution will be generated.
	*
	*
	* @param buffer is an double pointer-Array. The generated random numbers are stored inside this buffer.
	* @param my represents the expected value.
	* @param sigma represents the standard deviation.
	* @param data - this value has the information, how many random numbers should be generated.
	* @return integer as error handler -> 0 means Success | --> -1 Error
	*/
	MYDLL_API int numGenNormal(double* buffer, double my, double sigma, int data);

	/**
	* @brief Generates a password
	*
	* Generates a password with diffrent predefined alphabets.
	*
	* @param stringBuffer is a char* array and the password buffer.
	* @param stringLength is an integer and the buffer length.
	* @param type is from type Alphabet and the passw. character type
	* @return integer as error handler
	*/
	MYDLL_API int PasswordGenerator(char* stringBuffer, int stringLength, Alphabet type);

#ifdef __cplusplus  
}
#endif  