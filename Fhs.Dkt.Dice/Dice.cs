﻿using Fhs.Dkt.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fhs.Dkt.Wrappers;
using Fhs.Dkt.Interfaces;

// [assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.Dice))]

/// <summary>
/// Specifies the top level namespace of this application.
/// Creared by Gabriel Dax.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This class is responsible to provide methods to generate a random number for the dices (User/AI actions).
    /// Created by Gabriel Dax.
    /// </summary>
    public class Dice : IDice
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Dice{T}"/> class.
        /// </summary>
        public Dice() { }

        /// <summary>
        /// This method generates a random number between one and six (as rolled dice).
        /// </summary>
        /// <exception cref="ParameterError">Throws an exeption if the generation process went wrong</exception>
        /// <returns>The number as a <c>byte</c></returns>
        public byte RollDice()
        {
            const byte min = 1;
            const byte max = 6;

            uint[] numberArray = new uint[1];

            try
            {
                numberArray = RndWrapper.NumberGeneratorUniformUINT(min, max, 1);

                if (numberArray.Length <= 0)
                    throw new ParameterError("Error while Generating numbers");
            }
            catch (ParameterError e)
            {
                //TODO: Logging
                Console.WriteLine(e);
                return 0;
            }

            return (byte)numberArray[0];
        }
    }
}
