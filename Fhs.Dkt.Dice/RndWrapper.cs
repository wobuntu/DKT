﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace Fhs.Dkt.Wrappers
{

    /// <summary>
    /// Exception class for catch errors of the dll.
    /// Created by Gabriel Dax.
    /// </summary>
    public class ParameterError : Exception
    {
        public ParameterError()
        {
        }

        public ParameterError(string message)
        : base(message)
        {
        }

        public ParameterError(string message, Exception inner)
        : base(message, inner)
        {
        }
    }

    /// <summary>
    /// Assembly class to wrap and import the DLL functions. e.g. DllTestFunktion
    /// Created by Gabriel Dax.
    /// </summary>
    class RndWrapper
    {
        /// <summary>
        /// Enum to set the predefined alphabets for the passwort generator (Must be/Is copied from the file NumGen.dll).
        /// </summary>
        /// <value>Predefined alphabets for the password generation</value>
        public enum Alphabet
        {
            Uppercase       = 0x0001,
            Lowercase       = Uppercase << 1,
            Numbers         = Uppercase << 2,
            Brackets        = Uppercase << 3,
            AdditionalChar  = Uppercase << 4
        };


        /// <summary>
        /// Load function DllTestFunktion from the NumGen.dll.
        /// </summary>
        [DllImport("NumGen.dll", EntryPoint = "DllTestFunktion", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool DllTestFunktion();
        /// <summary>
        /// Function to test the DLL status
        /// </summary>
        /// <returns>Bool to show status</returns>
        /// <example>This sample shows how to call the CheckDllStatus function.</example>
        /// <code>
        ///     bool status = myAssebly.CheckDllStatus();
        /// </code>
        public static bool CheckDllStatus()
        {
            return DllTestFunktion();
        }

        /// <summary>
        /// Load function alphabetGen from the NumGen.dll with.
        /// </summary>
        /// <param name="stringBuffer">buffer from type char[] for the password</param>
        /// <param name="stringLength">Length from the buffer as integer</param>
        /// <param name="tempString">buffer from type char[] for the alphabet</param>
        /// <param name="tempLength">Length from the alphabet buffer as integer</param>
        /// <returns>integer as error handler</returns>
        [DllImport("NumGen.dll", EntryPoint = "alphabetGen", CallingConvention = CallingConvention.Cdecl)]
        public static extern int alphabetGen([Out] char[] stringBuffer, int stringLength, char[] tempString, int tempLength);
        /// <summary>
        /// Function to generate a password out of a user defined alphabet (alphabet as parameter).
        /// </summary>
        /// <param name="passLength">Password length</param>
        /// <param name="passAlphabet">Password alphabet</param>
        /// <returns>Generated passwort as string</returns>
        /// <exception cref="ParameterError">Throws an exeption if the parameter are wrong</exception> 
        /// <example>This sample shows how to call the UserDefinedPassGen function.</example>
        /// <code>
        ///     myAssebly assem = new myAssebly();
        ///     string passwd = assem.CheckDllStatus(10, "HelloWorld");
        /// </code>
        public string UserDefinedPassGen(int passLength, string passAlphabet)
        {
            char[] charBuffer = new char[passLength + 1];

            int error = alphabetGen(charBuffer, passLength, passAlphabet.ToCharArray(), passAlphabet.Length);

            if (error == -1)
                throw new ParameterError("Buffer not accessible");

            return new string(charBuffer);
        }

        /// <summary>
        /// Load function alphabetGen2 from the NumGen.dll with.
        /// </summary>
        /// <param name="stringBuffer">Buffer from type char[] for the password</param>
        /// <param name="stringLength">Length from the buffer as integer</param>
        /// <param name="alpha">Alphabet type as Integer (1 = Uppercase, 1 = Lowercase)</param>
        /// <returns>integer as error handler</returns>
        [DllImport("NumGen.dll", EntryPoint = "alphabetGen2", CallingConvention = CallingConvention.Cdecl)]
        public static extern int alphabetGen2([Out] char[] stringBuffer, int stringLength, int alpha);
        /// <summary>
        /// Function to generate a password out of a predefined alphabet.
        /// </summary>
        /// <param name="passLength">Password length</param>
        /// <param name="type">Alphabet type as myAssebly.Alphabet</param>
        /// <returns>Generated passwort as string</returns>
        /// <exception cref="ParameterError">Throws an exeption if the parameter are wrong</exception> 
        /// <example>This sample shows how to call the PreDefinedPassGen function.</example>
        /// <code>
        ///     myAssebly assem = new myAssebly();
        ///     string passwd = assem.PreDefinedPassGen(10, 1);
        /// </code>
        public string PreDefinedPassGen(int passLength, int type)
        {
            char[] charBuffer = new char[passLength + 1];

            int error = alphabetGen2(charBuffer, passLength, (int)type);

            if (error == -1)
                throw new ParameterError("Buffer not accessible");

            return new string(charBuffer);
        }

        /// <summary>
        /// Load function numGenUniform from the NumGen.dll with.
        /// </summary>
        /// <param name="bufferArray">Buffer from type int[] for the password</param>
        /// <param name="min">Minimum random number as Integer</param>
        /// <param name="max">Maximum random number as Integer</param>
        /// <param name="data">Length from the buffer as integer</param>
        /// <returns>integer as error handler</returns>
        [DllImport("NumGen.dll", EntryPoint = "numGenUniform", CallingConvention = CallingConvention.Cdecl)]
        public static extern int numGenUniform([Out] int[] bufferArray, int min, int max, int data);
        /// <summary>
        /// Function to generate a random number with Uniform distribution
        /// </summary>
        /// <param name="min">Minimum number as Integer</param>
        /// <param name="max">Maximum number as Integer</param>
        /// <param name="numLength">Password length</param>
        /// <returns>Numbers as string</returns>
        /// <exception cref="ParameterError">Throws an exeption if the parameter are wrong</exception>
        /// <example>This sample shows how to call the NumberGeneratorUniform function.</example>
        /// <code>
        ///     myAssebly assem = new myAssebly();
        ///     string passwd = assem.NumberGeneratorUniform(1, 10, 25);
        /// </code>
        public string NumberGeneratorUniform(int min, int max, int numLength)
        {
            int[] charBuffer = new int[numLength + 1];

            int error = numGenUniform(charBuffer, min, max, numLength);

            if (error == -1)
                throw new ParameterError("Buffer not accessible");

            return string.Join("", charBuffer);
        }

        /// <summary>
        /// Load function numGenUniformUINT from the NumGen.dll with.
        /// </summary>
        /// <param name="bufferArray">Buffer from type uint[] for the password</param>
        /// <param name="min">Minimum random number as Unsigned Integer</param>
        /// <param name="max">Maximum random number as Unsigned Integer</param>
        /// <param name="data">Length from the buffer as integer</param>
        /// <returns>integer as error handler</returns>
        [DllImport("NumGen.dll", EntryPoint = "numGenUniformUINT", CallingConvention = CallingConvention.Cdecl)]
        public static extern int numGenUniformUINT([Out] uint[] bufferArray, uint min, uint max, int data);
        /// <summary>
        /// Function to generate a random number with Uniform distribution (Unsigned Integer)
        /// </summary>
        /// <param name="min">Minimum number as Integer</param>
        /// <param name="max">Maximum number as Integer</param>
        /// <param name="numLength">Password length</param>
        /// <returns>Numbers as string</returns>
        /// <exception cref="ParameterError">Throws an exeption if the parameter are wrong</exception>
        /// <example>This sample shows how to call the NumberGeneratorUniformUINT function.</example>
        /// <code>
        ///     myAssebly assem = new myAssebly();
        ///     string passwd = assem.NumberGeneratorUniformUINT(1, 10, 25);
        /// </code>
        public static uint[] NumberGeneratorUniformUINT(uint min, uint max, int numLength)
        {
            uint[] charBuffer = new uint[numLength + 1];

            int error = numGenUniformUINT(charBuffer, min, max, numLength);

            if (error == -1)
                throw new ParameterError("Buffer not accessible");

            return charBuffer;
        }

        /// <summary>
        /// Load function numGenUniformUINT from the NumGen.dll with.
        /// </summary>
        /// <param name="buffer">Buffer from type uint[] for the password</param>
        /// <param name="my">µ of distribution</param>
        /// <param name="sigma">sigma of distribution</param>
        /// <param name="data">Length from the buffer as integer</param>
        /// <returns>Double as string</returns>
        [DllImport("NumGen.dll", EntryPoint = "numGenNormal", CallingConvention = CallingConvention.Cdecl)]
        public static extern int numGenNormal([Out] double[] buffer, double my, double sigma, int data);
        /// <summary>
        /// Function to generate a random number with Normal distribution
        /// </summary>
        /// <param name="my">µ of distribution</param>
        /// <param name="sigma">sigma of distribution</param>
        /// <param name="numLength">Count of Numbers</param>
        /// <returns>Numbers as string</returns>
        /// <exception cref="ParameterError">Throws an exeption if the parameter are wrong</exception>
        /// <example>This sample shows how to call the NumberGeneratorNormal function.</example>
        /// <code>
        ///     myAssebly assem = new myAssebly();
        ///     string passwd = assem.NumberGeneratorNormal(0.5, 0.5, 25);
        /// </code>
        public string NumberGeneratorNormal(double my, double sigma, int numLength)
        {
            double[] charBuffer = new double[numLength + 1];

            int error = numGenNormal(charBuffer, my, sigma, numLength);

            if (error == -1)
                throw new ParameterError("Buffer not accessible");

            return string.Join("", charBuffer);
        }

        /// <summary>
        /// Load function PasswordGenerator from the NumGen.dll with.
        /// </summary>
        /// <param name="stringBuffer">Buffer from type char[] for the password</param>
        /// <param name="stringLength">Length from the buffer as integer</param>
        /// <param name="type">Alphabet type as myAssebly.Alphabet</param>
        /// <returns>integer as error handler</returns>
        [DllImport("NumGen.dll", EntryPoint = "PasswordGenerator", CallingConvention = CallingConvention.Cdecl)]
        public static extern int PasswordGenerator([Out] char[] stringBuffer, int stringLength, Alphabet type);
        /// <summary>
        /// Function to generate a password with diffrent alphabets (letters, brackets).
        /// </summary>
        /// <param name="passLength">Password length</param>
        /// <param name="passType">Alphabet type as myAssebly.Alphabet</param>
        /// <returns>Alphabet type as myAssebly.Alphabet</returns>
        /// <exception cref="ParameterError">Throws an exeption if the parameter are wrong</exception>
        /// <example>This sample shows how to call the GeneratePassword function.</example>
        /// <code>
        ///     myAssebly assem = new myAssebly();
        ///     string passwd = assem.GeneratePassword(10, myAssebly.Alphabet.Uppercase);
        /// </code>
        public string GeneratePassword(int passLength, Alphabet passType)
        {
            char[] charBuffer = new char[passLength + 1];
            
            int error = PasswordGenerator(charBuffer, passLength, passType);

            if (error != 0)
            {
                if (error == -1)
                    throw new ParameterError("No valid password length");
                else if (error == -2)
                    throw new ParameterError("Buffer not accessible");
                else if (error == -3)
                    throw new ParameterError("Unknown myAssebly.Alphabet type");
            }

            return new string(charBuffer);
        }
    }
}