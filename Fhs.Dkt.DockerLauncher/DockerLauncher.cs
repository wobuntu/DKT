﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    public class DockerLauncher : ILauncher
    {
        public static void Main(string[] args)
        {
            DockerLauncher engine = new DockerLauncher();
        }

        private Object _lock = new Object();

        public DockerLauncher()
        {
            RemoteConnectionSettings.Host = "localhost";
            RemoteConnectionSettings.ServerPort = 54321;
            RemoteConnectionSettings.LocalPort = 12345;

            DependencyService.Register<ILauncher>(this);

            var logger = DependencyService.Get<ILogger>(
                new DependencyService.ForceLoadSettings()
                {
                    // This ensures loading the assemblies before they are needed so that the DependencyService finds our registration
                    // attributes
                    WildcardPatterns = new[] { "Fhs.Dkt*.dll", "*.exe" }
                });

            // Start the server after the DLLs were loaded (otherwise required DLLs might not have been loaded yet)
            Server.StartAsyncListenerIfNotActive(RemoteConnectionSettings.LocalPort);

            //this.Notify(new LogOnlyMessage("Docker GameEngine started. Initializing components..."));

            DependencyService.Get<IDataManager>();
            DependencyService.Get<IBank>();
            DependencyService.Get<ICardManager>();
            DependencyService.Get<IPropertyManager>();
            DependencyService.Get<IGameController>(allowToBeMissing: true);
            //
            //IGameField gameField = DependencyService.Get<IGameField>(allowToBeMissing: true);

            //this.Notify(new LogOnlyMessage("All components initialized."));

            // If the game field is null, it wouldn't make sense to run the application in our scenario right now

            //if (gameField == null)
            //{
            //    this.Notify(new LogOnlyMessage("No component registered for IGameField. Shutting down."));
            //    Console.WriteLine("Keine Komponente für die Darstellung (GameField) registriert. Beende.");
            //    Environment.Exit(0);
            //}
            //else
            //    gameField.Show();
        }

        #region Interface implementation
        public void SoftShutdown()
        {
            // Use a lock here since we work on multiple threads and cannot be sure that the shutdown will be called only from one
            // component at once
            lock (_lock)
            {
                // Unsubscribe from all messages, while sending an application-wide shutdown message
                this.UnsubscribeAll();
                this.Notify(new ApplicationShutdownMessage());

                // The game has ended and the different components perform cleanup tasks where required (may run async as well as sync,
                // depending on the components themselfes). All triggered by the sending of the ApplicationShutdownMessage();

                // Get the player with the most money
                Guid winnerId;
                string winnerName = null;
                try
                {
                    winnerId = DependencyService.Get<IBank>().GetWealthiestPlayer();
                    winnerName = DependencyService.Get<IGameController>().GetPlayerName(winnerId);

                    Console.WriteLine($"Das Spiel ist zu Ende. Der Gewinner ist {winnerName}, "
                    + $"sie/er hat nach Verkauf aller Immobilien das meiste Vermögen!");
                }
                catch
                {
                    Console.WriteLine("Das Spiel ist zu Ende. Aber ohje, es gibt keinen eindeutigen gewinner...");
                }


                Server.ShutdownAndWait();

                Environment.Exit(0);
            }
        }

        public void Shutdown()
        {
            Environment.Exit(0);
        }
        #endregion
    }
}
