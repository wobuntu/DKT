﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Fhs.Dkt.Types;
using Fhs.Dkt.Extensions;
using Fhs.Dkt.Interfaces;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Linq;

namespace Fhs.Dkt
{
    public class Server
    {
        static byte[] buffer = new byte[8192];
        static byte[] header = new byte[RemoteConnectionSettings.HeaderByteLength];

        static Thread listenerThread = null;

        static bool isShuttingDown = false;
        static bool shutDownDone = false;
        static int nextResponseCode = 0;

        static Object _invocationLock = new Object();

        //private static ConcurrentDictionary<int, object> responses = new ConcurrentDictionary<int, object>();

        public static void StartAsyncListenerIfNotActive(int port)
        {
            Console.WriteLine("Server: Starting the listener requested");
            if (listenerThread == null)
            {
                shutDownDone = isShuttingDown = false;
                listenerThread = new Thread(delegate () { listenerLoop(port); });
                listenerThread.Start();
            }
            else
            {
                Console.WriteLine("Server: Listener seems to be already running");
            }
        }

        public static void ShutdownAndWait()
        {
            Console.WriteLine("Server: Shutdown requested");

            isShuttingDown = true;
            listenerThread?.Abort();
            listenerThread = null;

            while (!shutDownDone)
                Thread.Sleep(RemoteConnectionSettings.MsBetweenShutdownChecks);

            Console.WriteLine("Server: Shutdown done");
        }

        //public static bool ResponseAvailable(int responseId)
        //{
        //    if (responses.ContainsKey(responseId))
        //        return true;
        //    else
        //        return false;
        //}

        //public static object GetResponse(int responseId)
        //{
        //    if (responses.TryGetValue(responseId, out object result))
        //        return result;

        //    throw new Exception($"Cannot find a response with the ID '{responseId}' in the currently available data");
        //}

        //public static int NextAvailableResponseId()
        //{
        //    if (responses.Keys.Count == 0)
        //        return -1;

        //    return responses.Keys.First();
        //}

        private static void listenerLoop(int port)
        {
            Console.WriteLine($"Server: Starting to listen to port {port} on thread {Thread.CurrentThread.ManagedThreadId}...");
            TcpListener listener = new TcpListener(IPAddress.Any, port);
            listener.Start();

            while (!isShuttingDown)
            {
                try
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine($"Server: Client connected on port {port}, starting reader loop...");
                    readLoop(client);
                }
                catch (ThreadAbortException ex)
                {
                    if (ex.ExceptionState is string s && s == "intended")
                    {
                        // everything ok, the abortion was intended
                        Console.WriteLine("Server: The listener thread was aborted intendedly.");
                    }
                    else
                    {
                        Console.WriteLine("Server: An unintended abortion of the listener thread occurred, see the exception for details: " + ex);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Server: Unexpected error occurred: " + ex);
                }

                // Wait some time before allowing new connections, so that the thread doesn't 'explode' if a firewall
                // is active or if a lot of concurrent connection attempts are made
                Thread.Sleep(RemoteConnectionSettings.MsBetweenConnectionAttempts);
            }

            shutDownDone = true;
            listenerThread = null;
        }

        private static void sendInvocation(RemoteInvocationInfo invocationInfo, NetworkStream stream)
        {
            var result = InvocationForwarder.DynamicInvoke(invocationInfo);


            // So we got the result from the dynamically invoked method, now check if the client expects a result
            // and if so, send it to him
            if (invocationInfo.ReturnExpected)
            {
                Console.WriteLine("Server: Remote expects an answer, building and sending the response...");

                RemoteInvocationInfo response = invocationInfo.BuildResponseFromObject(result);


                byte[] payload = Encoding.Unicode.GetBytes(response.ToString());

                byte[] header = new byte[RemoteConnectionSettings.HeaderByteLength];
                header.WriteInt(0, payload.Length, RemoteConnectionSettings.HeaderBitLength);

                stream.Write(header, 0, RemoteConnectionSettings.HeaderByteLength);
                stream.Write(payload, 0, payload.Length);
                stream.Flush();

                Console.WriteLine($"Server: Response sent (Header: {RemoteConnectionSettings.HeaderByteLength} bytes; " +
                    $"Payload: {payload.Length} bytes)");
            }
            else
            {
                Console.WriteLine("Server: Remote does not expect an answer.");
            }
        }

        private static void readLoop(TcpClient client)
        {
            Console.WriteLine("Server: Reader loop is starting");
            client.SendTimeout = RemoteConnectionSettings.MsTcpSendTimeout;
            client.ReceiveTimeout = RemoteConnectionSettings.MsTcpReceiveTimeout;

            var stream = client.GetStream();
            stream.ReadTimeout = RemoteConnectionSettings.MsStreamReadTimeout;
            stream.WriteTimeout = RemoteConnectionSettings.MsStreamWriteTimeout;

            try
            {
                while (!isShuttingDown)
                {
                    Console.WriteLine($"Server: Reader loop waits for incoming data on thread {Thread.CurrentThread.ManagedThreadId}...");
                    // wait for incoming data
                    while (!stream.DataAvailable)
                    {
                        if (isShuttingDown)
                            break;

                        Task.Delay(RemoteConnectionSettings.MsBeforeDataAvailableChecks).Wait();
                    }

                    // Read the first 4 byte, which contains the length of the data
                    int byteRead = stream.Read(buffer, 0, RemoteConnectionSettings.HeaderByteLength);

                    if (byteRead != RemoteConnectionSettings.HeaderByteLength)
                    {
                        Console.WriteLine("Server: It seems that an invalid request was made (header length is wrong), ignoring the request");
                        continue;
                    }

                    // Using our new byte extension methods here for convenience
                    int lg = buffer.ReadInt(0, RemoteConnectionSettings.HeaderBitLength);

                    Console.WriteLine($"Server: Data available on stream, header tells length of {lg} bytes");

                    // Now read the full content of the received message
                    string dataString = "";
                    while (lg > 0)
                    {
                        byteRead = stream.Read(buffer, 0, Math.Min(buffer.Length, lg));
                        Console.WriteLine($"Server: Received {byteRead}/{lg} bytes (buffer size: {buffer.Length})");
                        dataString += buffer.ReadString(0, (ushort)byteRead);

                        lg -= byteRead;
                    }

                    try
                    {
                        Console.WriteLine("Server: Deserializing received instructions...");

                        var invocationInfo = RemoteInvocationInfo.FromString(dataString);
                        if (invocationInfo.ComponentName == "___custom___" && invocationInfo.TargetName == "___notify___")
                            Console.WriteLine("Server: Invocation target is a notification broadcast of type " + invocationInfo.ParamTypes[0]);
                        else
                            Console.WriteLine($"Server: Invocation target is {invocationInfo.ComponentName}.{invocationInfo.TargetName}");

                        sendInvocation(invocationInfo, stream);
                                
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Server: A problem occurred during the deserialization or during sending the response, " +
                            "see the exception for details: " + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Server: A problem occurred during the communication, see the exception for details: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Server: The communication has ended, cleaning up resources and shutting down...");

                // do some cleanup here
                stream?.Close();
                stream = null;
                client?.Close();
                client = null;

                isShuttingDown = true;
            }
        }
    }
}