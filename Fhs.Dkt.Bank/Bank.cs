﻿using System;
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Messaging;

// [assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.Bank))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This class represents the Bank component and handles the player's money balance and money transactions between players.
    /// Created by Lukas Brugger.
    /// </summary>
    public class Bank : IBank
    {
        private Dictionary<Guid, BaseBankAccount> openedAccounts;
        private Guid bankKey;
        private Guid bankSecretKey;

        public List<BankTransaction> BankTransactions { get; }

        /// <summary>
        /// Bank Class constructor
        /// Initializes bankAccount of Type "AccountBank" for Bank Instance and an empty list to store player bank accounts
        /// </summary>
        public Bank()
        {
            //this.Notify(new LogOnlyMessage("Bank created."));

            openedAccounts = new Dictionary<Guid, BaseBankAccount>();
            BankTransactions = new List<BankTransaction>();

            bankKey = Guid.NewGuid();
            openedAccounts.Add(bankKey, new AccountBank(bankKey));
            bankSecretKey = Guid.NewGuid();

            try
            {
                BaseBankAccount createdBankAccount = (AccountBank)GetBankAccount(bankKey);
                bankSecretKey = createdBankAccount.GetSecretKey();
            }
            catch (Exception ex)
            {
                throw new Exception("No idea how this could go wrong here", ex);
            }
        }

        /// <summary>
        /// Returns the BankAccount Id of the Bank instance itself
        /// </summary>
        public Guid GetBankAccountId()
        {
            return bankKey;
        }

        /// <summary>
        /// Creates a new BankAccount for the passed player id. 
        /// Throws an Exception, if the player already holds an account with the bank instance.
        /// </summary>
        /// <param name="playerId">Guid of a player instance</param>
        public Guid OpenAccount(Guid playerId)
        {
            if(openedAccounts.ContainsKey(playerId))
            {
                throw new Exception("Account already created");
            }

            openedAccounts.Add(playerId, new BankAccount(playerId));

            this.Notify(new LogOnlyMessage("Bank - Create account for " + playerId));

            try
            {
                BaseBankAccount createdBankAccount = GetBankAccount(playerId);
                this.Notify(new AccountBalanceChangedMessage(playerId, createdBankAccount.GetAmount(), createdBankAccount.GetAmount()));
                return createdBankAccount.GetSecretKey();
            }
            catch (Exception ex)
            {
                throw new Exception("No idea how this could go wrong here", ex);
            }

        }

        /// <summary>
        /// Returns the player/account id that holds the most funds.
        /// </summary>
        public Guid GetWealthiestPlayer()
        {
            double maxFunds = 0;
            Guid? wealthiestPlayer = null;

            foreach (var baseBankAccount in openedAccounts)
            {
                if (baseBankAccount.Value.GetAmount() > maxFunds)
                {
                    maxFunds = baseBankAccount.Value.GetAmount();
                    wealthiestPlayer = baseBankAccount.Key;
                }
            }

            if (wealthiestPlayer == null)
            {
                throw new Exception("No player found");
            }

            return (Guid)wealthiestPlayer;
        }

        /// <summary>
        /// Handles a transaction of money from one BankAccount to another.
        /// The method takes the sending players id as well as its secret bank key to be able to access the BankAccount, as well
        /// as the to be transferred amount and the recipients id.
        /// It returns a boolean value representing wether the transaction was successful or not. 
        /// </summary>
        /// <param name="playerId">Guid of the sending account/player instance</param>
        /// <param name="recipientId">Guid of the recieving account/player instance</param>
        /// <param name="key">Secret bankAccount key of the sending player</param>
        /// <param name="amount">Amount of money to be transferred</param>
        public bool MakeTransaction(Guid playerId, Guid recipientId, Guid key, double amount)
        {
            BankTransaction transaction = new BankTransaction(playerId, recipientId, amount);

            try
            {
                this.Notify(new LogOnlyMessage("Bank - Transaction: From " + playerId + " to " + recipientId + " for " +
                                               amount));

                BaseBankAccount purchasingAccount = GetBankAccount(playerId);
                BaseBankAccount recievingAccount = GetBankAccount(recipientId);

                double purchasingBalanceBefore = purchasingAccount.GetAmount();
                double recievingBalanceBefore = recievingAccount.GetAmount();

                purchasingAccount.WithdrawMoney(key, amount);
                recievingAccount.DepositMoney(amount);

                double purchasingBalanceAfter = purchasingAccount.GetAmount();
                double recievingBalanceAfter = recievingAccount.GetAmount();

                transaction.Valid = true;

                this.Notify(new AccountBalanceChangedMessage(playerId, purchasingBalanceBefore, purchasingBalanceAfter));
                this.Notify(new AccountBalanceChangedMessage(recipientId, recievingBalanceBefore, recievingBalanceAfter));

                return true;
            }
            catch (Exceptions.BankActionNotPermittedException bEx)
            {
                throw bEx;
            }
            catch (Exception)
            {
                transaction.Valid = false;
                return false;

            } finally
            {
                BankTransactions.Add(transaction);
            }

        }

        /// <summary>
        /// Returns the BankAccount instance of a specific player. 
        /// Throws an exception if the BankAccount does not exist.
        /// </summary>
        /// <param name="playerId">Guid of the player instance, to which the account belongs</param>
        private BaseBankAccount GetBankAccount(Guid playerId)
        {
            if (openedAccounts.TryGetValue(playerId, out BaseBankAccount bankAccount))
            {
                return bankAccount;
            }
            else
            {
                throw new Exception("BankAccount does not exist");
            }
        }

        /// <summary>
        /// Returns the full transaction history recorded by the bank instance.
        /// </summary>
        public List<string> GetTransactionList()
        {
            List<string> transactionSummary = new List<string>();

            BankTransactions.ForEach(delegate (BankTransaction transaction)
            {
                transactionSummary.Add(transaction.ToString());
            });

            return transactionSummary;
        }

        /// <summary>
        /// Helper method to ease the case of transferring money to the bank account.
        /// This calls MakeTransaction with the bank's account key taken directly from the bank instance, 
        /// instead of having to pass it to the method
        /// </summary>
        /// <param name="playerId">Guid of the sending account/player instance</param>
        /// <param name="key">Secret bankAccount key of the sending player</param>
        /// <param name="amount">Amount of money to be transferred</param>
        public bool MakeBankTransaction(Guid playerId, Guid key, double amount)
        {
            return MakeTransaction(playerId, bankKey, key, amount);
        }

        /// <summary>
        /// Returns the current balance of the account of the passed player id.
        /// Needs the secret bank key of the player
        /// Throws an exception if the key is not correct
        /// </summary>
        /// <param name="playerId">Guid of the account/player instance</param>
        /// <param name="key">Secret bankAccount key of the player</param>
        public double GetAccountBalance(Guid playerId, Guid key)
        {
            BaseBankAccount bankAccount = GetBankAccount(playerId);

            if (!bankAccount.GetSecretKey().Equals(key))
            {
                throw new Exceptions.BankActionNotPermittedException("Account Key is not correct");
            }

            return bankAccount.GetAmount();
        }

        /// <summary>
        /// Helper method to ease the case of the bank sending money to a specific player.
        /// This calls MakeTransaction with the bank information being taken directly from the instance
        /// </summary>
        /// <param name="playerId">Guid of the recieving account/player instance</param>
        /// <param name="amount">Amount of money to be transferred</param>
        public bool DepositAmount(Guid playerId, double amount)
        {
            return MakeTransaction(bankKey, playerId, bankSecretKey, amount);
        }

        /// <summary>
        /// Method that transfers the fixed amount of 200.00 to a player in case he moves over the starting field
        /// </summary>
        /// <param name="playerId">Guid of the recieving account/player instance</param>
        public bool DepositNewRoundAmount(Guid playerId)
        {
            return DepositAmount(playerId, 200.00);
        }

        /// <summary>
        /// Method that checks, if the passed key is correct for the passed account/player id
        /// </summary>
        /// <param name="playerId">Guid of the account/player instance</param>
        /// <param name="key">Secret account key of the player</param>
        public bool CheckAccountPermission(Guid playerId, Guid key)
        {
            BaseBankAccount account = GetBankAccount(playerId);

            return key.Equals(account.GetSecretKey());
        }

        /// <summary>
        /// Abstract class for a BankAccount
        /// Holds account id and secret key needed by all extending classes, as well as defining the two 
        /// most important methods, which need to be implemented by any extending class
        /// Created by Lukas Brugger.
        /// </summary>
        private abstract class BaseBankAccount
        {
            private Guid holderId;
            private Guid secretKey;
            protected double cashAmount;

            public BaseBankAccount(Guid holderId)
            {
                this.holderId = holderId;
                SetSecretKey(Guid.NewGuid());
            }

            public Guid GetSecretKey() => secretKey;

            public double GetAmount()
            {
                return cashAmount;
            }

            private void SetSecretKey(Guid value)
            {
                secretKey = value;
            }

            public abstract void DepositMoney(double amount);

            public abstract void WithdrawMoney(Guid accountKey, double amount);
        }

        /// <summary>
        /// The special BankAccount, that is used by the Bank instance itself.
        /// DepositMoney and WithdrawMoney methods are defined here, but do not contain any logic, as we
        /// define the bank to have an infinite amount of money
        /// </summary>
        private class AccountBank : BaseBankAccount
        {
            public AccountBank(Guid holderId) : base(holderId) { }

            public override void DepositMoney(double amount) {
                // noop
            }

            public override void WithdrawMoney(Guid accountKey, double amount) {
                // noop
            }
        }

        /// <summary>
        /// BankAccount class for players.
        /// This BankAccount gets initialised with a fixed cash amount of 1500
        /// </summary>
        private class BankAccount : BaseBankAccount
        {
            public BankAccount(Guid holderId) : base(holderId) {
                cashAmount = 1500;
            }

            /// <summary>
            /// Method that deposits a given amount of money to the account
            /// </summary>
            /// <param name="amount">Amount of money to be deposited to the account</param>
            public override void DepositMoney(double amount)
            {
                if (amount > 0)
                {
                    cashAmount += amount;
                }
            }

            /// <summary>
            /// Method that withdraws a given amount of money from the account
            /// This method needs the secret account key of the player to make sure, that the player
            /// approved of this withdrawal and it is not getting called maliciously
            /// Throws an exception if the key is not correct, and if withdrawal is not possible because 
            /// there is not enough cash in the account anymore.
            /// </summary>
            /// <param name="accountKey">Secret account key only known to the owning player</param>
            /// <param name="amount">Amount of money to be withdrawn from the account</param>
            public override void WithdrawMoney(Guid accountKey, double amount)
            {
                if (!accountKey.Equals(GetSecretKey()))
                {
                    throw new Exceptions.BankActionNotPermittedException("Withdrawal not permitted");
                }

                if (!(amount > 0)) return;

                this.Notify(new LogOnlyMessage("Bank - Permitted money withdrawal from  " + accountKey + " by " + amount));
                this.Notify(new LogOnlyMessage("Bank - Current cashamount: " + cashAmount));

                if (cashAmount - amount < 0)
                {
                    throw new Exception("Withdrawal not possible. Not enough cash");
                }

                cashAmount -= amount;
                this.Notify(new LogOnlyMessage("Bank - cashamount after withdrawal: " + cashAmount));
            }
        }

        /// <summary>
        /// Class which represents an executed transaction and holds all the details to it.
        /// Instances of this class are used by the bank to hold a history of all taken transactions
        /// </summary>
        public class BankTransaction
        {
            public BankTransaction(Guid purchasingPlayer, Guid recievingPlayer, double amount){
                PurchasingPlayer = purchasingPlayer;
                RecievingPlayer = recievingPlayer;
                Amount = amount;
                Valid = false;
            }

            public bool Valid { get; set; }
            private Guid PurchasingPlayer { get; }
            private Guid RecievingPlayer { get; }
            private double Amount { get; }

            public override string ToString()
            {
                return
                    $"Transaction from {PurchasingPlayer} to {RecievingPlayer} by {Amount}. The transaction was " +
                    $"{(Valid ? "valid" : "not valid")}";
            }
        }
    }
}
