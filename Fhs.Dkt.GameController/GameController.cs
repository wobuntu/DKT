﻿// CREATED BY BORIS BRANKOVIC, GABRIEL DAX, LUKAS BRUGGER
using System;
using System.Collections.Generic;
using System.Linq;

using Fhs.Dkt.Messaging;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Types;
using Fhs.Dkt.Exceptions;
using System.Threading;

[assembly: Fhs.Dkt.Dependency(typeof(Fhs.Dkt.GameController))]

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This component is responsbile for controlling the flow of the game.
    /// This means it calls the defined components via the DependencyService - like PropertyManager, Bank, or CardManager and makes use of their methods to handle the game. 
    /// The methot "Start" handles therefor the entire game play and based on the player status, different options will be checked in the course of the game.
    /// Further it is responsbile for checking several options e.g. if the current player has to go to jail, if he is allowed to buy a property, or also if the player has to pay rent.
    /// Everything is handled within this component, also the communication with the bank i.e. if a player has to pay rent or if he wants to buy something. 
    /// Created by Boris Brankovic, Gabriel Dax, Lukas Brugger.
    /// </summary>
    public class GameController : IGameController
    {
        //Private list which contains the current PlayerStatus of each player in the game.
        private List<PlayerStatus> _players = new List<PlayerStatus>();
        //boolean variable for checking whether the game is still running or not.
        private bool isRunning = false;

        public bool IsRunning() { return isRunning; }

        //Declaring the interfaces of the defined components. 
        //This interfaces will be called via the DependencyService to make use of their methods.
        private IPropertyManager propertyManager;
        private ICardManager cardManager;
        private IGameField gameField;
        private IBank bank;

        //Integer variable for managing a possible timeout of a player e.g. if the dices are rolled ??
        int timeoutUiActions = 3000;

        /// <summary>
        /// Constructor of the component GameController. It initializes the interfaces of several other components via the DependencyService.
        /// This step is important for being able to make use of the methods defined in other components e.g. making transactions with the bank.
        /// </summary>
        public GameController()
        {
            this.SubscribeTo<PlayerCreatedMessage>(onCreatePlayer);
            this.SubscribeTo<ApplicationShutdownMessage>(onShutdown);

            propertyManager = DependencyService.Get<IPropertyManager>();
            cardManager = DependencyService.Get<ICardManager>();
            gameField = DependencyService.Get<IGameField>();
            bank = DependencyService.Get<IBank>();
        }

        /// <summary>
        /// The method "Start" sets the rounds of a new game and is the central logic of this component.
        /// It handles for instance moving the players around the gamefield and checking in combination with that several options and defined rules.
        /// The rules are applied accorind the original version of "DKT". 
        /// It will be checked if rolling the dices caused doublets and if there are e.g. 3 doublets in a row the player has to go to jail. 
        /// Also this method checks whether the current player is located on a field of a Risk-or Bank Card -> if this happens the component card manager will be called with its methods.
        /// Further this method checks also if the current player is located on a property -> therefor the type of the field will be checked and referring to the methods of the property manager component
        /// several options will be checked e.g. if the player is allowed to buy the property, a house/hotel or if he has to pay rent.
        /// All changes based on the called methods will be displayed in the view to inform the players.
        /// </summary>
        /// <param name="rounds">Integer variable for setting the rounds at the beginning of a game.</param>
        public void Start(int rounds)
        {
            this.isRunning = true;
            shuttingDown = false;

            this.Notify(new LogOnlyMessage($"GameController.Start() called. Players: {_players.Count}"));
            //this.Notify(new LogOnlyMessage("Current thread: " + Thread.CurrentThread.ManagedThreadId, LogLvl.Debug));

            int countDoubles = 0;
            sbyte eyesSum = 0;
            bool isDouble = false;
            int roundCnt = 0;

            try
            {
                while (_players.Where((p) => p.IsActive).Count() > 1)
                {
                    if (shuttingDown)
                        break;

                    if (rounds > 0 && roundCnt >= rounds)
                        break;

                    this.Notify(new LogOnlyMessage($"New round has begun (#{++roundCnt})"));

                    // Each iteration of the following loop represents one round
                    foreach (PlayerStatus curPlayerStatus in _players)
                    {

                        if (!curPlayerStatus.PlayerReference.IsAi)
                            timeoutUiActions = -1;
                        else
                        {
                            timeoutUiActions = 3000;
                        }

                        if (shuttingDown)
                            break;

                        if (!curPlayerStatus.IsActive)
                        {
                            this.Notify(new LogOnlyMessage($"Player '{curPlayerStatus.PlayerReference.Name}' (ID: "
                                + $"{curPlayerStatus.PlayerReference.ID}) is inactive (has lost already) and thus was skipped."));
                            continue;
                        }

                        this.Notify(new ActivePlayerChangedMessage(curPlayerStatus.PlayerReference.ID));

                        if (curPlayerStatus.RoundsToSuspend > 0)
                        {
                            // Player is currently not allowed to play, she/he must wait currently
                            curPlayerStatus.RoundsToSuspend -= 1;

                            string msg = curPlayerStatus.PlayerReference.Name;
                            if (curPlayerStatus.RoundsToSuspend == 0)
                                msg += ", diese Runde musst du leider noch im Gefängnis absitzen.";
                            else
                                msg += $", du musst noch weitere {curPlayerStatus.RoundsToSuspend} Runden aussetzen.";

                            if (curPlayerStatus.PlayerReference.CheckBalance(50) && !curPlayerStatus.PlayerReference.IsAi)
                            {
                                msg += "Du kannst dich jedoch mit einer Zahlung von 50,- freikaufen. Willst du das tun?";

                                UserChoice decision = gameField.WaitForDecision(
                                    msg,
                                    buttons: UserChoice.YesNo,
                                    textYes: "Ja, hol mich hier raus!",
                                    textNo: "Nein, danke.");

                                if (shuttingDown)
                                    break;

                                if (decision == UserChoice.No)
                                {
                                    continue;
                                }
                                else
                                {
                                    curPlayerStatus.PlayerReference.MakeBankPayment(50);
                                    curPlayerStatus.RoundsToSuspend = 0;
                                }
                            }
                            else
                            {
                                gameField.WaitForAcknowledge(msg, timeoutUiActions);

                                if (shuttingDown)
                                    break;

                                continue;
                            }

                        }

                        countDoubles = 0;
                        eyesSum = 0;

                        do
                        {
                            gameField.WaitForRollDiceAcknowledge(timeoutUiActions);

                            if (shuttingDown)
                                break;

                            Tuple<byte, byte> rolledDice = curPlayerStatus.PlayerReference.RollDice();
                            if (rolledDice.Item1 == rolledDice.Item2)
                            {
                                countDoubles++;
                                isDouble = true;
                                this.Notify(new LogOnlyMessage($"Player '{curPlayerStatus.PlayerReference.Name}' (ID: "
                                    + $"{curPlayerStatus.PlayerReference.ID}) rolled a double"));

                                if (shuttingDown)
                                    break;

                                gameField.WaitForAcknowledge($"{curPlayerStatus.PlayerReference.Name} hat einen Pasch gewürfelt!", timeoutUiActions);

                                if (shuttingDown)
                                    break;

                                if (countDoubles == 3)
                                {
                                    if (curPlayerStatus.PlayerHasOutOfJailCard)
                                    {
                                        gameField.WaitForDecision($"{curPlayerStatus.PlayerReference.Name}, da hast du nochmal Glück gehabt. "
                                            + $"Eigentlich musst du ins Gefängnis weil du 3x hintereinander einen Pasch gewürfelt hast, aber "
                                            + $"du hast ja noch deine Raus-aus-dem-Gefängnis-Karte :){Environment.NewLine}Dran bist du "
                                            + $"trotzdem nicht mehr.",
                                            caption: "Glück gehabt!",
                                            buttons: UserChoice.Yes,
                                            textYes: "OK!");

                                        if (shuttingDown)
                                            break;

                                        curPlayerStatus.PlayerHasOutOfJailCard = false;
                                        cardManager.UnlockCard(curPlayerStatus.PlayerReference.ID);
                                    }
                                    else
                                    {
                                        // If the player rolled 3 doubles at once --> he has to go to jail immediately
                                        string why = $"Oje {curPlayerStatus.PlayerReference.Name}, du hast 3x einen Pasch gewürfelt und musst "
                                            + $"daher leider für 3 Runden ins Gefängnis!";
                                        sendPlayerToArrest(curPlayerStatus, why);
                                    }
                                    break;
                                }
                            }
                            else
                            {
                                isDouble = false;
                            }

                            eyesSum += (sbyte)(rolledDice.Item1 + rolledDice.Item2);

                        }
                        while (isDouble);

                        if (isDouble)
                        {
                            // The last dices were a double, so if this line is reached, the player had to go to the jail because he rolled
                            // 3 doubles in a row
                            continue;
                        }

                        if (shuttingDown)
                            break;

                        movePlayerByOffset(curPlayerStatus, eyesSum);

                        FieldType curFieldType = propertyManager.GetFieldType(curPlayerStatus.FieldPosition);
                        switch (curFieldType)
                        {
                            case FieldType.Bank:
                            case FieldType.Risk:
                                // Enum field types for bank and risk are directly convertible to card types
                                handleCardDraw(curPlayerStatus, (CardType)curFieldType);
                                break;
                            case FieldType.FixedTax: // Field 33
                                gameField.WaitForAcknowledge("Du musst 80 an die Bank bezahlen.", timeoutUiActions);
                                curPlayerStatus.PlayerReference.MakeBankPayment(80);
                                break;
                            case FieldType.PropertyTax: // Field 21
                                gameField.WaitForAcknowledge("10% Vermögenssteuer!", timeoutUiActions);
                                curPlayerStatus.PlayerReference.MakeLevyPayment();
                                break;
                            case FieldType.GotoJail: // Field 11
                                if (curPlayerStatus.PlayerHasOutOfJailCard)
                                {
                                    gameField.WaitForDecision($"{curPlayerStatus.PlayerReference.Name}, da hast du nochmal Glück gehabt. "
                                            + $"Eigentlich musst du wegen dem Feld, auf das du gezogen bist, ins Gefängnis, aber "
                                            + $"du hast ja noch deine Raus-aus-dem-Gefängnis-Karte :){Environment.NewLine}Dran bist du "
                                            + $"trotzdem nicht mehr.",
                                            caption: "Glück gehabt!",
                                            buttons: UserChoice.Yes,
                                            textYes: "OK!");

                                    curPlayerStatus.PlayerHasOutOfJailCard = false;
                                    cardManager.UnlockCard(curPlayerStatus.PlayerReference.ID);
                                }
                                else
                                {
                                    sendPlayerToArrest(curPlayerStatus, $"Oh nein {curPlayerStatus.PlayerReference.Name}, du bist auf das Feld "
                                    + "Gesetzesübertretung gekommen und musst wegen einer Straftat leider für 3 Runden in den Arrest!");
                                }

                                break;
                            case FieldType.Property:
                                handlePropertyEntered(curPlayerStatus);
                                break;

                            // case FieldType.Start: Nothing to do
                            // case FieldType.Jail: Nothing to do
                        }
                    }

                    this.Notify(new LogOnlyMessage($"Round finished (#{roundCnt})."));
                }

                if (this.isRunning && !this.shuttingDown)
                    Stop();
            }
            catch (Exception ex)
            {
                this.Notify(new LogOnlyMessage("An exception terminated the gameloop in the player manager: " + ex, LogLvl.Error));
            }

            this.Notify(new LogOnlyMessage("GameController.Start() exited."));
        }

        /// <summary>
        /// The method "Stop" will be called when the rounds are over, or if the button "Spiel Beenden" was pressed in the view.
        /// It calls the method "SoftShutDown" of the component "GameEngine" and stops the game.
        /// </summary>
        public void Stop() => DependencyService.Get<ILauncher>().SoftShutdown();

        /// <summary>
        /// The method "GetPlayerName" gets the name of a current player based on the player ID.
        /// </summary>
        /// <param name="playerId">The ID of the current player as Guid.</param>
        /// <returns>The name of the current player as string.</returns>
        public string GetPlayerName(Guid playerId)
        {
            foreach (var playerStatus in this._players)
            {
                if (playerStatus.PlayerReference.ID == playerId)
                    return playerStatus.PlayerReference.Name;
            }

            return null;
        }

        //Boolean variable for checking whether the game was stopped or not.
        private bool shuttingDown = false;

        /// <summary>
        /// This method will be called if the game has stopped. It is responsible for selling all properties of a player back to the bank.
        /// And subsequently it is necessary to determine which player has won, if the game ends.
        /// </summary>
        /// <param name="msg">Parameter as type ApplicationShutdownMessage, which is important for subscribing to the method.</param>
        private void onShutdown(ApplicationShutdownMessage msg)
        {
            // Sell all properties of all players to the bank, so that we can determine a winner with the most money later
            foreach (var playerStatus in this._players)
            {
                var props = propertyManager.GetOwnedProperties(playerStatus.PlayerReference.ID);
                foreach (var prop in props)
                    playerStatus.PlayerReference.SellProperty(prop);
            }

            // Will be automatically invoked if the engine fires an ApplicationShutDownMessage via SoftShutdown
            this.isRunning = false;
            this.UnsubscribeAll();
            shuttingDown = true;
        }

        /// <summary>
        /// This method is responsible for moving a currenet player over the field "start".
        /// A notification will inform the player either he got the money for moving over "start" or not e.g. because the player was in jail.
        /// </summary>
        /// <param name="status">The current player status of each player as instance of the class "PlayerStatus".</param>
        private void playerMovedOverStart(PlayerStatus status)
        {
            if (!status.WasInJail)
            {
                gameField.WaitForDecision($"{status.PlayerReference.Name}, du gehst über Start und erhältst 200,-",
                    caption: "Neue Runde!",
                    buttons: UserChoice.Yes,
                    textYes: "Vielen Dank!");

                bank.DepositNewRoundAmount(status.PlayerReference.ID);
            }
            else
            {
                gameField.WaitForDecision($"{status.PlayerReference.Name}, leider erhältst du diese Runde keinen " +
                                          $"Rundenbonus, weil du im Gefängnis warst.",
                    caption: "Neue Runde!",
                    buttons: UserChoice.Yes,
                    textYes: "Oh Nein!");
            }

            status.WasInJail = false;
        }

        /// <summary>
        /// Method for creating a new Player by calling the instancinating the class "Player". 
        /// </summary>
        /// <param name="msg">This parameter contains important information about the player itself e.g. the name.</param>
        private void onCreatePlayer(PlayerCreatedMessage msg)
        {
            // Create the player itself
            Player player = new Player(msg.Guid, msg.Name, msg.IsAI);

            // Create a wrapper of the player which is only known by the GameController (private class) and which holds additional
            // information about the player which shall not be seen by the players themselfes or other 
            PlayerStatus playerStatus = new PlayerStatus(player);
            _players.Add(playerStatus);
        }

        /// <summary>
        /// The method "handleCardDraw" is responsible for taking action if a riskcard or bankcard was drawn by a player.
        /// It checks whether the player has got for example a card for going directly to jail and in combation with that, 
        /// it takes action if the player has to be moved over the gamefield. Another important functionality of this method is to 
        /// check also if the player has to make a payment because of a bankkard and also to transfer money to the player if he gets money from the bank.
        /// </summary>
        /// <param name="playerStatus">The current player status of each player as instance of the class "PlayerStatus".</param>
        /// <param name="cardType">A certain card from type "CardType" a player has drawn.</param>
        private void handleCardDraw(PlayerStatus playerStatus, CardType cardType)
        {
            CardBase drawnCard = playerStatus.PlayerReference.DrawCard(cardType);

            switch (drawnCard.Action.ActionType)
            {
                case CardActionType.Arrest:

                    if (playerStatus.PlayerHasOutOfJailCard)
                    {
                        gameField.WaitForDecision($"{playerStatus.PlayerReference.Name}, da hast du nochmal Glück gehabt. "
                                + $"Eigentlich musst du wegen der gezogenen Karte ins Gefängnis, aber "
                                + $"du hast ja noch deine Raus-aus-dem-Gefängnis-Karte :){Environment.NewLine}Dran bist du "
                                + $"trotzdem nicht mehr.",
                                caption: "Glück gehabt!",
                                buttons: UserChoice.Yes,
                                textYes: "OK!");

                        playerStatus.PlayerHasOutOfJailCard = false;
                        cardManager.UnlockCard(playerStatus.PlayerReference.ID);
                    }
                    else
                    {
                        sendPlayerToArrest(playerStatus, $"Lieber {playerStatus.PlayerReference.Name}, du hast eine "
                                + "Gehe-ins-Gefängnis-Karte gezogen und musst nun leider für 3 Runden in den Arrest!");
                    }
                    break;

                case CardActionType.Deposit:
                    gameField.WaitForDecision($"Glückwunsch {playerStatus.PlayerReference.Name}: {drawnCard.Action.GetFormattedText()}",
                        caption: "Glückwunsch!",
                        buttons: UserChoice.Yes,
                        textYes: "Wow, danke!");
                    playerStatus.PlayerReference.RecieveCardDeposit(drawnCard); break;

                case CardActionType.GetOutOfJail:
                    gameField.WaitForDecision($"Glückwunsch {playerStatus.PlayerReference.Name}: {drawnCard.Action.GetFormattedText()}",
                        caption: "Einmal ist keinmal!",
                        buttons: UserChoice.Yes,
                        textYes: "Werd ich brauchen!");
                    playerStatus.PlayerHasOutOfJailCard = true; break;

                case CardActionType.MoveBackward:
                    gameField.WaitForAcknowledge(drawnCard.Action.GetFormattedText(), timeoutUiActions);
                    movePlayerByOffset(playerStatus, (sbyte)(drawnCard.Action.IncrementFieldValue * -1)); break;

                case CardActionType.MoveForward:
                    gameField.WaitForAcknowledge(drawnCard.Action.GetFormattedText(), timeoutUiActions);
                    movePlayerByOffset(playerStatus, (sbyte)(drawnCard.Action.IncrementFieldValue)); break;

                case CardActionType.MoveToField:
                    gameField.WaitForAcknowledge(drawnCard.Action.GetFormattedText(), timeoutUiActions);
                    movePlayerToField(playerStatus, drawnCard.Action.GoToFieldValue.Value); break;

                case CardActionType.RelativePayment:
                    gameField.WaitForDecision(drawnCard.Action.GetFormattedText(),
                        caption: "Rechnungen, Rechnungen, Rechnungen...",
                        buttons: UserChoice.Yes,
                        textYes: "Ich hab ja genug...");

                    if (drawnCard.Action.PayPercentageProperties == null)
                    {
                        if (drawnCard.Action.PayPerHouse != null && drawnCard.Action.PayPerHotel != null)
                        {
                            // fixe Werte für Haus und Hotel

                            // get num houses and hotels and pay for that
                            IReadOnlyList<byte> ownedProperties = propertyManager.GetOwnedProperties(playerStatus.PlayerReference.ID);


                        }
                    }
                    else
                    {
                        // Prozentwerte für Properties usw.

                        // pay percentage on all the properties you got
                    }



                    /* switch (drawnCard.Action)
                    {
                        case Types.CardActionType.PropertyFixedPayment:
                            // make fixed payment for number of hotels and houses
                            break;
                        case Types.CardActionType.PropertyPayment:
                            // make payment based on % of buying price of all properties
                            break;
                        default:
                            throw new Exception("Invalid card passed");
                    } */

                    // TODO: Look for other possibilities
                    //bool relTransValid = playerStatus.PlayerReference.MakeRelativeCardPayment(drawnCard);
                    //if (!relTransValid)
                    //{
                    //    setPlayerBancrupt(playerStatus);
                    //}
                    break;

                case CardActionType.Payment:
                    gameField.WaitForDecision($"Oje {playerStatus.PlayerReference.Name}: {drawnCard.Action.GetFormattedText()}",
                        caption: "Pech gehabt!",
                        buttons: UserChoice.Yes,
                        textYes: "Ok...");

                    double amount = drawnCard.Action.Amount.Value;

                    bool transactionValid = playerStatus.PlayerReference.MakeBankPayment(amount);
                    if (!transactionValid)
                    {
                        setPlayerBancrupt(playerStatus);
                    }
                    break;
            }
        }

        /// <summary>
        /// handlePropertyEntered checks the ownerhsip of a certain property,
        /// e.g. whether the player is the owner of the property, the bank is the owner of it, 
        /// the property belongs to anoter player already or if the property is not buyable at all.
        /// Reffering to one possible owership-status, several further checks are done.
        /// These further steps in each if branch, are important to find out if the player is for instance entitled to buy a property, or a house/hotel on this property.
        /// Ownership- PropertyNotBuyable: the current property can not be bought by the player.
        /// Ownership- PlayerIsOwner: Based on the decisions yes, or no  (the player will be asked for example if he wants to buy a house/hotel) - 
        /// it will be decided if he has enough money on his account and a transaction will be made if the answer was yes. 
        /// If the answer of the player was no, then the game continues. 
        /// Ownership- BankIsOwner: The player will be asked if he wants to buy the property. If the decision was "yes" the player complies to buy the property and a transaction is done, 
        /// otherwise the game continues. 
        /// Ownership- OwnerIsAnotherPlayer: The current player has to pay rent on that property, because it belongs already to someone else. The rent is according to each property and
        /// it depends on how many houses/hotels a property has on it, or if it is other property-type (TrafficLine, Company) then the rent will be calculated differently <see cref="TrafficLineProperty"/> <see cref="CompanyProperty"/>
        /// In this case it will be also checked, if the current player runs out of money -> if this happens he will be set bankrupt.
        /// </summary>
        /// <param name="playerStatus">The current player status of each player as instance of the class "PlayerStatus".</param>
        private void handlePropertyEntered(PlayerStatus playerStatus)
        {
            Guid pid = playerStatus.PlayerReference.ID;
            byte pos = playerStatus.FieldPosition;

            var owner = propertyManager.GetRelativeOwnership(pid, pos);

            //property is not buyable by the current player
            if (owner == OwnerShip.PropertyNotBuyable)
                // The current method must not be called if the field is not a buyable property, this should have been checked before..
                throw new ArgumentException($"The field position for the player with ID '{playerStatus.PlayerReference.ID}' is not a "
                    + "buyable property.");

           
            if (owner == OwnerShip.OwnerIsPlayer)
            {
                //Player is allowed to buy a house
                if (propertyManager.PlayerAllowedToBuyHouse(pid, pos))
                {
                    UserChoice decision = UserChoice.Yes;

                    //Player is an AI
                    if (!playerStatus.PlayerReference.IsAi)
                    {
                        decision = gameField.WaitForDecision($"{playerStatus.PlayerReference.Name}, möchtest du hier ein Haus kaufen?",
                            caption: "Haus kaufen?",
                            buttons: UserChoice.YesNo,
                            textYes: "Klar!",
                            textNo: "Nein, danke.");
                    }

                    if (decision == UserChoice.Yes)
                    {
                        HousingProperty property = propertyManager.GetClone((byte)pos) as HousingProperty;
                        if (property == null)
                            throw new Exception("Invalid property received");

                        bool checker = playerStatus.PlayerReference.CheckBalance(property.HousePrice);

                        if (!checker)
                            gameField.WaitForAcknowledge($"{playerStatus.PlayerReference.Name}, du hast leider nicht genug Geld", timeoutUiActions);
                        else
                            playerStatus.PlayerReference.BuyHouse(pos);
                    }
                } //Player is allowed to buy a hotel
                else if (propertyManager.PlayerAllowedToBuyHotel(pid, pos))
                {
                    UserChoice decision = UserChoice.Yes;

                    if (!playerStatus.PlayerReference.IsAi)
                    {
                        decision = gameField.WaitForDecision($"{playerStatus.PlayerReference.Name}, möchtest du hier ein Hotel kaufen?",
                        caption: "Haus kaufen?",
                        buttons: UserChoice.YesNo,
                        textYes: "Klar!",
                        textNo: "Nein, danke.");
                    }

                    if (decision == UserChoice.Yes)
                    {
                        HousingProperty property = propertyManager.GetClone((byte)pos) as HousingProperty;
                        if (property == null)
                            throw new Exception("Invalid property received");

                        bool checker = playerStatus.PlayerReference.CheckBalance(property.HotelPrice);

                        if (!checker)
                            gameField.WaitForAcknowledge($"{playerStatus.PlayerReference.Name}, du hast leider nicht genug Geld", timeoutUiActions);
                        else
                            playerStatus.PlayerReference.BuyHotel(pos);
                    }
                }
                else
                {
                    gameField.WaitForAcknowledge($"{playerStatus.PlayerReference.Name}, du kannst leider hier gerade nichts kaufen.",
                        timeoutUiActions);
                }
            }
            else if (owner == OwnerShip.OwnerIsBank)
            {
                UserChoice decision = UserChoice.Yes;
                PropertyBase property = propertyManager.GetClone((byte)pos);


                if (!playerStatus.PlayerReference.IsAi)
                {
                    decision = gameField.WaitForDecision($"{playerStatus.PlayerReference.Name}, möchtest du dieses Grundstück für " 
                          + property.BuyingPrice + "€ kaufen?",
                    caption: "Grundstück kaufen?",
                    propertyId: playerStatus.FieldPosition,
                    buttons: UserChoice.YesNo,
                    textYes: "Klar!",
                    textNo: "Nein, danke.");
                }

                if (decision == UserChoice.Yes)
                {
                    bool checker = playerStatus.PlayerReference.CheckBalance(property.BuyingPrice);
                    if (!checker)
                        gameField.WaitForAcknowledge($"{playerStatus.PlayerReference.Name}, du hast leider nicht genug Geld", timeoutUiActions);
                    else
                        playerStatus.PlayerReference.BuyProperty(pos);
                }
            }
            else // last possibility: owner = OwnerShip.OwnerIsOtherPlayer
            {
                var prop = propertyManager.GetClone(pos);
                double amount = 0;

                Guid? owningPlayerId = propertyManager.GetOwnerOfProperty(prop.ID);

                if (owningPlayerId == null)
                {
                    throw new Exception("No owner returned, but there should be one");
                }

                if (prop is HousingProperty housingProp)
                    amount = housingProp.GetRentCost();
                else if (prop is TrafficLineProperty trafficLineProp)
                    amount = trafficLineProp.GetRentCost(propertyManager.GetNumberOwnedProperties(owningPlayerId.Value, PropertyType.TrafficLine));
                else if (prop is CompanyProperty companyProp)
                {
                    gameField.WaitForDecision("Du musst leider Miete zahlen! Würfle nochmal und bezahle die Augenzahl mal der Miete!",
                        caption: "Ojemine",
                        propertyId: prop.ID,
                        buttons: UserChoice.Yes,
                        textYes: "Ok");
                    Tuple<byte, byte> dices = playerStatus.PlayerReference.RollDice();
                    byte sum = (byte)(dices.Item1 + dices.Item2);
                    amount = companyProp.GetRentCost(propertyManager.GetNumberOwnedProperties(owningPlayerId.Value, PropertyType.Company), sum);

                    gameField.WaitForAcknowledge("Du hast " + sum + " gewürfelt, bezahle daher " + amount + "!", timeoutUiActions);
                }

                bool transactionValid = playerStatus.PlayerReference.MakePlayerPayment(amount, (Guid)owningPlayerId);

                if (!transactionValid)
                {
                    // Player is officially out of money, bye bye
                    setPlayerBancrupt(playerStatus);
                }

                gameField.WaitForAcknowledge($"{playerStatus.PlayerReference.Name}, du hast " + amount + "€ Miete für: " + prop.Name
                        + " bezahlt!", timeoutUiActions);
            }
        }

        /// <summary>
        /// This method moves the player on the gamefield. The number of fields, which have to be moved
        /// will be defined by the rolled dices in the game. 
        /// </summary>
        /// <param name="playerStatus">The current player status of each player as instance of the class "PlayerStatus".</param>
        /// <param name="fieldId">The current field ID where the player is located as byte value.</param>
        /// <param name="payForCrossingStart">Boolean value for checking if the player has finished a round.</param>
        private void movePlayerToField(PlayerStatus playerStatus, byte fieldId, bool payForCrossingStart = true)
        {
            byte posOld = playerStatus.FieldPosition;
            int moveDelta = 0;

            if (fieldId >= posOld)
                moveDelta = fieldId - posOld;
            else
                // e.g. field 39, move to 1 --> 40-39+1=2
                moveDelta = 40 - posOld + fieldId;

            movePlayerByOffset(playerStatus, (sbyte)moveDelta, payForCrossingStart);
        }

        /// <summary>
        /// The method "movePlayerByOffset" calculates the fieldOffset i.e. if a player goes over start.
        /// This functionality is important for calculatint the right amount of fields a player has to be moved according to the current gamiefield position. 
        /// </summary>
        /// <param name="status">The current player status of each player as instance of the class "PlayerStatus".</param>
        /// <param name="fieldOffset">The current offset of a player reffering to the position on the gamefield. </param>
        /// <param name="payForCrossingStart">Boolean value for checking if the player was already moved over the field "start".</param>
        private void movePlayerByOffset(PlayerStatus status, sbyte fieldOffset, bool payForCrossingStart = true)
        {
            byte posOld = status.FieldPosition;
            sbyte posNew = (sbyte)(status.FieldPosition + fieldOffset);

            // e.g. for the following two lines:
            // 39 + 2 = 41 --> field  1
            //  1 - 2 = -1 --> field 39
            posNew = (sbyte)(posNew > 40 ? (posNew - 40) : posNew);
            posNew = (sbyte)(posNew < 1 ? posNew + 40 : posNew);

            status.FieldPosition = (byte)posNew;

            this.Notify(new PlayerMovedMessage(status.PlayerReference.ID, posOld, status.FieldPosition));

            if (payForCrossingStart && posOld > posNew && fieldOffset > 0)
                // player moved forward and went over start
                playerMovedOverStart(status);
        }

        /// <summary>
        /// This method is called if the current player has to go to jail i.e. the player got three doubles,
        /// or due to a certain riskcard. The current player will be automatically moved to the arrest-field.
        /// </summary>
        /// <param name="playerStatus">The current status of a player as a defined in the class "PlayerStatus".</param>
        /// <param name="whyMsg">Message for the view as a string.</param>
        private void sendPlayerToArrest(PlayerStatus playerStatus, string whyMsg = null)
        {
            const byte arrestField = 31;

            if (whyMsg == null)
                whyMsg = $"Oje {playerStatus.PlayerReference.Name}, du musst leider für 3 Runden ins Gefängnis.";

            gameField.WaitForDecision(whyMsg,
                caption: "Gefängnis!",
                buttons: UserChoice.Yes,
                textYes: "Tja, Ok.");

            movePlayerToField(playerStatus, arrestField, false);
            playerStatus.RoundsToSuspend = 3;
            playerStatus.WasInJail = true;
        }

        /// <summary>
        /// This method sets a certain player bankrupt e.g. if a player runs out of money for a certain reason.
        /// </summary>
        /// <param name="playerstatus">The current status of a player as a defined in the class "PlayerStatus".</param>
        private void setPlayerBancrupt(PlayerStatus playerstatus)
        {
            playerstatus.IsActive = false;

            // Remove all properties and give them to bank
            // TODO we will need to auction them off to other players according to the rules
            IReadOnlyList<byte> readOnlyList = propertyManager.GetOwnedProperties(playerstatus.PlayerReference.ID);

            foreach (byte property in readOnlyList)
            {
                propertyManager.RemovePropFromOwner(playerstatus.PlayerReference.ID, property);
                propertyManager.AssignPlayerToProp(bank.GetBankAccountId(), property);
            }

            gameField.WaitForDecision("Du bist leider bankrott. Damit ist das Spiel für dich zu Ende",
                caption: "Bankrott!",
                buttons: UserChoice.Yes,
                textYes: "Schade");

            this.Notify(new PlayerBancruptMessage(playerstatus.PlayerReference.ID));
        }

        /// <summary>
        /// Class "PlayerStatus" defines several possible states of a certain player.
        /// </summary>
        private class PlayerStatus
        {
            /// <summary>
            /// Constructor for the class "PlayerStatus".
            /// </summary>
            /// <param name="player">Player required for the constructor if called.</param>
            public PlayerStatus(Player player)
            {
                this.PlayerReference = player;
            }

            //Integer variable for counting the numbers, how long a certain player is supsended. 
            private int roundsToSuspend = 0;
            /// <summary>
            /// Declaring getter and setter "PlayerReference" of type "Player". It references a certain player to a specific state.
            /// </summary>
            public Player PlayerReference { get; private set; }
            /// <summary>
            /// Declaring getter and setter "IsActive". Shows if a certain player is active or not i.e. if the player is in jail or maybe bankrupt. 
            /// </summary>
            public bool IsActive { get; set; } = true;
            /// <summary>
            /// Declaring getter and setter "PlayerHasOutOfJailCard". Will be set to true if a player has the riskcard "Get out of jail". 
            /// </summary>
            public bool PlayerHasOutOfJailCard { get; set; } = false;

            /// <summary>
            /// Declaring getter and setter "FieldPosition". Responsible for setting the current position of a player.
            /// </summary>
            public byte FieldPosition { get; set; } = 1;

            /// <summary>
            /// Declaring getter and setter "WasInJail". Contains information about a certain player if he was in jail.
            /// Important for checking if he was in jail before receiving money if the player got over start.
            /// </summary>
            public bool WasInJail { get; set; } = false;

            /// <summary>
            /// Declaring getter and setter "RoundsToSuspend". Sets the number of rounds a certain player has to suspend. 
            /// </summary>
            public int RoundsToSuspend
            {
                get => roundsToSuspend;
                set
                {
                    roundsToSuspend = value;
                }
            }
        }
    }
}
