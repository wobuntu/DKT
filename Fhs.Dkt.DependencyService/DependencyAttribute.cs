﻿// CREATED BY MATHIAS LACKNER
using System;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// This Attribute is used to register classes for the usage with the <see cref="DependencyService"/>. It does only require
    /// the type of the class, the dependency service will know which interfaces it implements and can then be used via
    /// <see cref="DependencyService.Get{T}(DependencyService.ForceLoadSettings, bool)"/> to create or fetch an instance of it.
    /// Created by Mathias Lackner.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class DependencyAttribute : Attribute
    {
        /// <summary>
        /// Registers a class for the usage with the <see cref="DependencyService"/>. It does only require the type of the class to
        /// register, the <see cref="DependencyService"/> will then know from that the interfaces it implements and can than provide
        /// the creation or receipt of instances of the given class type via the
        /// <see cref="DependencyService.Get{T}(DependencyService.ForceLoadSettings, bool)"/> method.
        /// </summary>
        /// <param name="typeToRegister">The type of the class to be registered for the usage with the <see cref="DependencyService"/>.</param>
        public DependencyAttribute(Type typeToRegister)
        {
            RegisteredType = typeToRegister;
        }

        public Type RegisteredType { get; }

        private string domain = Environment.OSVersion.ToString();
        public string Domain
        {
            get => domain;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    domain = Environment.OSVersion.ToString();
                else
                    domain = value;
            }
        }
    }
}
