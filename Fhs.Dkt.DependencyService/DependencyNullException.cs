﻿// CREATED BY MATHIAS LACKNER
using System;

/// <summary>
/// This namespace contains all specialized exceptions used within this solution across all components (Part of the core assembly).
/// </summary>
namespace Fhs.Dkt.Exceptions
{
    /// <summary>This exception indicates that a dependency could not have been found although it is required.</summary>
    public class DependencyNullException : Exception
    {
        /// <summary>
        /// Creates a new DependencyNullException, indicating that no dependency could have been resolved for the component type given via
        /// <paramref name="dependencyType"/>.
        /// </summary>
        /// <param name="dependencyType"></param>
        public DependencyNullException(Type dependencyType)
        {
            UnresolvedDependencyType = dependencyType;
            msg = $"The dependency component of type {dependencyType?.Name} is null.";
        }

        /// <summary>Private variable containing the exception message.</summary>
        private string msg;

        /// <summary>Holds the type of the dependency which could not be resolved.</summary>
        public Type UnresolvedDependencyType { get; }

        /// <summary>Holds the message describing what caused the exception.</summary>
        public override string Message => msg;
    }
}
