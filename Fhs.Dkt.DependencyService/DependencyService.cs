﻿// CREATED BY MATHIAS LACKNER
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Fhs.Dkt.Exceptions;
using Fhs.Dkt.Interfaces;

/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// The <see cref="DependencyService"/> acts as an application-wide instance creator/getter for registered types. It allows creating
    /// and receiving instances, of which only the interface definition is known. E.g., to receive an instance of a class implementing
    /// 'ICar', simply use <see cref="DependencyService.Get{T}(DependencyService.ForceLoadSettings, bool)"/> with 'ICar' as the type
    /// parameter.
    /// To let the <see cref="DependencyService"/> know, which specific instance to return (or create) for such an interface, register a
    /// class type via <see cref="DependencyService.Register{T}"/>. Assuming the class 'Bus' would implement 'ICar', then the type
    /// parameter would just be 'Bus' for the registration. The <see cref="DependencyService"/> will automatically recognize, that requests
    /// for 'ICar' shall return instances of 'Bus'.
    /// An even easier way to register a class type is the usage of the <see cref="DependencyAttribute"/>.
    /// Created by Mathias Lackner.
    /// Note that the idea for this class came from Xamarin.Forms. Parts of Xamarin's equally named implementation were adapted to fit our
    /// needs (see https://github.com/xamarin/Xamarin.Forms/blob/master/Xamarin.Forms.Core/DependencyService.cs)
    /// </summary>
    public static class DependencyService
    {
        /// <summary>
        /// Private list which holds all currently registered types. The contained items may not have been instantiated yet.
        /// </summary>
        private static Dictionary<string, List<Type>> registeredTypes = new Dictionary<string, List<Type>>();

        /// <summary>
        /// This private dictionary holds Types as the keys and the already instantiated objects as values. This means, every item in this
        /// list was already requested somewhere in the application and its references are kept for future
        /// <see cref="Get{T}(ForceLoadSettings, bool)"/> requests.
        /// </summary>
        private static Dictionary<Type, object> createdInstances = new Dictionary<Type, object>();

        /// <summary>
        /// This list is used to keep track of all assemblies which were investigated during the process of collecting the
        /// <see cref="DependencyAttribute"/>s of the currently loaded assemblies. This avoids circular and double checks of already
        /// processed assemblies.
        /// </summary>
        private static List<string> knownAssemblies = new List<string>();

        /// <summary>
        /// Since the type of the DependencyAttribute is required quite often, it is stored in this private static variable, so that
        /// we don't call 'typeof' an unnecessary amount of times.
        /// </summary>
        private static Type searchedAttributeType = typeof(DependencyAttribute);

        public static IReadOnlyList<Type> GetAllRegisteredTypes(string domain = null)
        {
            if (domain == null)
                domain = Environment.OSVersion.ToString();

            if (registeredTypes.ContainsKey(domain))
                return registeredTypes[domain];

            // Return an empty list if the domain is not known
            return new List<Type>();
        }

        public class ForceLoadSettings
        {
            public string[] LookupDirectories { get; set; } = null;
            public string[] WildcardPatterns { get; set; } = { "*.dll", "*.exe" };
            public bool Recursive { get; set; } = false;
        }

        /// <summary>
        /// This static constructor gets all assemblies loaded for the current application and searches all of them for
        /// <see cref="DependencyAttribute"/>s to register classes to the <see cref="DependencyService"/>.
        /// </summary>
        static DependencyService()
        {
            // Get all assemblies loaded for the current application domain and find all registered types
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly assembly in assemblies)
                registerByAttributes(assembly);
        }


        /// <summary>
        /// If a class was registered before via a <see cref="DependencyAttribute"/> or via the <see cref="Register{T}"/> method, it can
        /// be requested by using this method. E.g. if the class 'Bus' (which implements the interface 'ICar') was registered, simply
        /// pass 'ICar' as the type parameter <typeparamref name="T"/> to receive the instance (or create it if it is the first call to
        /// <see cref="Get{T}(ForceLoadSettings, bool)"/>.
        /// </summary>
        /// <typeparam name="T">The type of the interface or the class of which an instance shall be returned.</typeparam>
        /// <param name="settings">If this parameter is not null, additional steps may be performed to find the correct implementation
        /// for the passed type. See <see cref="ForceLoadSettings"/> for more information.</param>
        /// <param name="allowToBeMissing">If this parameter is set to true, the <see cref="DependencyService"/> will throw an
        /// <see cref="DependencyNullException"/> if it cannot find an implementation for the passed type. Otherwise it returns null.
        /// </param>
        public static T Get<T>(ForceLoadSettings settings = null, bool allowToBeMissing = false, string domain = null) where T : class, IDktComponent
        {
            return (T)Get(typeof(T), settings, allowToBeMissing, domain);
        }

        public static IDktComponent Get(Type requestedType, ForceLoadSettings settings = null, bool allowToBeMissing = false, string domain = null)
        {
            // It might be required that assemblies are forced to be loaded, which is done with the following method
            forceLoadingOfAssemblies(settings);

            bool foundMatch = false;

            if (domain == null)
                domain = Environment.OSVersion.ToString();

            if (!registeredTypes.ContainsKey(domain))
                registeredTypes[domain] = new List<Type>();

            if (!registeredTypes[domain].Contains(requestedType))
            {
                // If not the exact type was given, but a type which is assignable from a known type --> use the known one
                foreach (Type registeredType in registeredTypes[domain])
                {
                    if (requestedType.IsAssignableFrom(registeredType))
                    {
                        requestedType = registeredType;
                        foundMatch = true;
                        break;
                    }
                }

                if (!foundMatch)
                {
                    // If this line is reached, none of the known types match the requested one
                    if (allowToBeMissing)
                        return null;

                    throw new DependencyNullException(requestedType);
                }
            }

            if (!createdInstances.ContainsKey(requestedType))
                // Create an instance for this type if not already existing
                createdInstances[requestedType] = Activator.CreateInstance(requestedType);

            return (IDktComponent)createdInstances[requestedType];
        }


        /// <summary>
        /// Registers that instances of the type passed via <typeparamref name="T"/> shall be returned while using 
        /// <see cref="Get{T}(ForceLoadSettings, bool)"/>.
        /// Note that <typeparamref name="T"/> must be a concrete implementation which can be instantiated.
        /// </summary>
        /// <typeparam name="T">The type of the concrete implementation of a component.</typeparam>
        public static void Register<T>(string domain = null) where T : class, IDktComponent
        {
            if (domain == null)
                domain = Environment.OSVersion.ToString();

            Type type = typeof(T);

            if (!registeredTypes.ContainsKey(domain))
                registeredTypes[domain] = new List<Type>();

            if (!registeredTypes[domain].Contains(type))
                registeredTypes[domain].Add(type);
        }


        /// <summary>
        /// Registers an already created instance to be returned while using <see cref="Get{T}(ForceLoadSettings, bool)"/>.
        /// </summary>
        /// <typeparam name="T">The type of the already instantiated component.</typeparam>
        /// <param name="instance">The instance of <typeparamref name="T"/> to return for <see cref="Get{T}(ForceLoadSettings, bool)"/>
        /// requests.
        /// </param>
        public static void Register<T>(T instance, string domain = null) where T : class, IDktComponent
        {
            if (domain == null)
                domain = Environment.OSVersion.ToString();

            Type type = typeof(T);

            if (!registeredTypes.ContainsKey(domain))
                registeredTypes[domain] = new List<Type>();

            if (!registeredTypes[domain].Contains(type))
                registeredTypes[domain].Add(type);

            if (!createdInstances.ContainsKey(type))
                createdInstances.Add(type, instance);
        }


        /// <summary>
        /// This private static method is called during <see cref="Get{T}(ForceLoadSettings, bool)"/> requests and receives the forwarded
        /// <see cref="ForceLoadSettings"/>. It is required to ensure the load of assemblies which were not loaded into the current
        /// AppDomain until now, so that all <see cref="DependencyAttribute"/>s can be found, even if the CLR did not load the assembly
        /// yet.
        /// </summary>
        /// <param name="settings">The settings containing the paths and wildcards for further not yet loaded DLLs to investigate.</param>
        private static void forceLoadingOfAssemblies(ForceLoadSettings settings)
        {
            if (settings == null)
                return;

            // correct missing values first
            if (settings.LookupDirectories == null || settings.LookupDirectories.Length == 0)
                settings.LookupDirectories = new string[] { AppDomain.CurrentDomain.BaseDirectory };

            if (settings.WildcardPatterns == null)
                settings.WildcardPatterns = new[] { "*.dll", "*.exe" };

            SearchOption searchOption = settings.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            // get all files which match the pattern and which are contained in the lookup directories
            foreach (var path in settings.LookupDirectories)
            {
                foreach (var pattern in settings.WildcardPatterns)
                {
                    var files = Directory.GetFiles(path, pattern, searchOption);
                    foreach (var file in files)
                    {
                        var freshlyLoadedAsm = AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(file));

                        // now that the assembly has been loaded, search for our attributes in it
                        registerByAttributes(freshlyLoadedAsm);
                    }
                }
            }
        }


        /// <summary>
        /// This private static method is responsible to find the <see cref="DependencyAttribute"/>s in the given
        /// <paramref name="assembly"/>. It is invoked by the static constructor of the <see cref="DependencyService"/> as well as
        /// during performing a <see cref="Get{T}(ForceLoadSettings, bool)"/> request.
        /// </summary>
        /// <param name="assembly">The assembly in which <see cref="DependencyAttribute"/>s are searched.</param>
        private static void registerByAttributes(Assembly assembly)
        {
            string location = assembly.Location;
            if (knownAssemblies.Contains(location))
                // the assembly was already investigated
                return;

            knownAssemblies.Add(location);

            IEnumerable<Attribute> attributes = assembly.GetCustomAttributes(searchedAttributeType);
            if (attributes.Count() == 0)
                return;

            foreach (DependencyAttribute attribute in attributes)
            {
                if (!registeredTypes.ContainsKey(attribute.Domain))
                    registeredTypes[attribute.Domain] = new List<Type>();

                if (!registeredTypes[attribute.Domain].Contains(attribute.RegisteredType))
                    registeredTypes[attribute.Domain].Add(attribute.RegisteredType);
            }
        }
    }
}
