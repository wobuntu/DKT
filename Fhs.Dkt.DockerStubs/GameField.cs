﻿using Fhs.Dkt;
using Fhs.Dkt.Types;
using Fhs.Dkt.Interfaces;
using Fhs.Dkt.DockerStubs;
using System.Threading;

[assembly: Dependency(typeof(GameField))]
namespace Fhs.Dkt.DockerStubs
{
    public class GameField : IGameField
    {
        public void Close()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IGameField),
                targetName: nameof(Close),
                typeParams: null,
                returnExpected: false,
                parameters: null);

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public void Show()
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: nameof(IGameField),
                targetName: nameof(Show),
                typeParams: null,
                returnExpected: false,
                parameters: null);

            RemoteConnector.RemoteInvokeAsync(info);
        }

        public void WaitForAcknowledge(string text, int maxTimeout = -1)
        {
            RemoteInvocationInfo infoSetVisuals = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "SetAcknowledgeVisuals",
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { true, text, "OK" });

            RemoteConnector.RemoteInvokeAsync(infoSetVisuals);

            RemoteInvocationInfo infoActionMade = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "GetUiActionMade",
                typeParams: null,
                returnExpected: true,
                parameters: null);

            while(!RemoteConnector.RemoteInvokeWithReturnAsync(infoActionMade).Result.ToResponseValue<bool>())
            {
                Thread.Sleep(500);

            }

            infoSetVisuals = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "SetAcknowledgeVisuals",
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { false, null, null });

            RemoteConnector.RemoteInvokeAsync(infoSetVisuals);
        }

        public UserChoice WaitForDecision(string msg, string caption = null,
            byte propertyId = 0, UserChoice buttons = UserChoice.Yes,
            string textYes = "Ja", string textNo = "Nein", string textCancel = "Abbrechen")
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "AskForPropertyDecision",
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { msg, caption, propertyId, buttons, textYes, textNo, textCancel });

            RemoteConnector.RemoteInvokeAsync(info);

            RemoteInvocationInfo infoActionMade = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "GetUiActionMade",
                typeParams: null,
                returnExpected: true,
                parameters: null);

            while (!RemoteConnector.RemoteInvokeWithReturnAsync(infoActionMade).Result.ToResponseValue<bool>())
            {
                Thread.Sleep(500);
            }

            info = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "GetLastDialogResult",
                typeParams: null,
                returnExpected: true,
                parameters: null);

            return RemoteConnector.RemoteInvokeWithReturnAsync(info).Result.ToResponseValue<UserChoice>();
        }

        public void WaitForRollDiceAcknowledge(int maxTimeout = -1)
        {
            RemoteInvocationInfo infoSetVisuals = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "ShowRollDiceButton",
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { true, null } );

            RemoteConnector.RemoteInvokeAsync(infoSetVisuals);

            RemoteInvocationInfo infoActionMade = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "GetUiActionMade",
                typeParams: null,
                returnExpected: true,
                parameters: null);

            while (!RemoteConnector.RemoteInvokeWithReturnAsync(infoActionMade).Result.ToResponseValue<bool>())
            {
                Thread.Sleep(500);
            }

            infoSetVisuals = new RemoteInvocationInfo(
                componentName: "IGameView",
                targetName: "ShowRollDiceButton",
                typeParams: null,
                returnExpected: false,
                parameters: new object[] { false, null });

            RemoteConnector.RemoteInvokeAsync(infoSetVisuals);
        }
    }
}
