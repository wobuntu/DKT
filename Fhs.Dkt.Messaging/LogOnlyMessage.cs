﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to pass messages to the <see cref="Interfaces.ILogger"/> implementation without sending additional
    /// object data. It shall be used for general logging purposes, not to trigger any actions.
    /// Created by Mathias Lackner.
    /// </summary>
    public class LogOnlyMessage : Fhs.Dkt.Interfaces.ILogMessage
    {
        /// <summary>
        /// Private Ctor for serialization/deserialization.
        /// </summary>
        [Newtonsoft.Json.JsonConstructor]
        private LogOnlyMessage() { }

        /// <summary>
        /// Creates a new <see cref="LogOnlyMessage"/> with the given <paramref name="message"/> and <paramref name="logLvl"/>.
        /// Use this type of message only for logging purposes, avoid triggering any actions with it.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="logLvl">The log level to use.</param>
        public LogOnlyMessage(string message, LogLvl logLvl = LogLvl.Info)
        {
            this.LogLevel = logLvl;
            this.MessageDescription = message;
            messageData["Exception"] = null;
        }

        /// <summary>
        /// Creates a new <see cref="LogOnlyMessage"/> with the given <paramref name="message"/> and <paramref name="logLvl"/>.
        /// Use this type of message only for logging purposes, avoid triggering any actions with it.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="exception">An exception to log along with the message.</param>
        /// <param name="logLvl">The log level to use.</param>
        public LogOnlyMessage(string message, Exception exception, LogLvl logLvl = LogLvl.Error)
        {
            this.LogLevel = logLvl;
            if (exception == null)
                this.MessageDescription = message;
            else
                this.MessageDescription = $"{message}, Exception: {exception}";

            messageData["Exception"] = exception;
        }


        #region IMessage, ILogMessage Implementation
        public Dictionary<string, object> messageData = new Dictionary<string, object>();

        public IReadOnlyDictionary<string, object> MessageData => messageData;

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel { get; private set; }

        /// <summary>Gets the message which is intended to appear in the log.</summary>
        public string MessageDescription { get; private set; }
        #endregion


        #region Properties
        /// <summary>
        /// Gets the exception which was passed with this <see cref="LogOnlyMessage"/> (May be null).
        /// </summary>
        public Exception Exception => (Exception)messageData["Exception"];
        #endregion

    }
}
