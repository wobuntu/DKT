﻿// CREATED BY LUKAS BRUGGER
using System;
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{

    /// <summary>
    /// This message class is used to indicate that the balance of a users bank account has changed.
    /// It contains the player's id, as well as the balance before the balance change, and the updated balance after a transaction has
    /// been made.
    /// Created by Lukas Brugger.
    /// </summary>
    public class AccountBalanceChangedMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private AccountBalanceChangedMessage() { }

        /// <summary>
        /// Creates a new instance of a <see cref="AccountBalanceChangedMessage"/> Message.
        /// The id of the player whose balance was changed, as well as the balance before the change and the 
        /// balance after the change are required.
        /// </summary>
        /// <param name="playerId">Guid of player, whose balance was changed</param>
        /// <param name="oldBalance">Account balance before the change</param>
        /// <param name="newBalance">Account balance after the change</param>
        public AccountBalanceChangedMessage(Guid playerId, double oldBalance, double newBalance)
        {
            messageData["playerId"] = playerId.ToString();
            messageData["oldBalance"] = oldBalance.ToString();
            messageData["newBalance"] = newBalance.ToString();
        }

        /// <summary>
        /// Returns the id of the player whos Bankaccount balance was changed
        /// </summary>
        public Guid PlayerId => Guid.Parse((string)messageData["playerId"]);
        
        /// <summary>
        /// Returns the balance value before it was changed
        /// </summary>
        public double OldBalance => double.Parse((string)messageData["oldBalance"]);

        /// <summary>
        /// Returns the balance value after it was changed
        /// </summary>
        public double NewBalance => double.Parse((string)messageData["newBalance"]);

        /// <summary>
        /// Returns the message that will be logged
        /// </summary>
        public string MessageDescription => $"The player's account with ID {PlayerId} changed from {OldBalance} to {NewBalance}.";

        /// <summary>
        /// Returns the message's log level
        /// </summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>
        /// Private dictionary containing all data which is made public via the properties of this class.
        /// </summary>
        private Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>
        /// A dictionary containing all data which is stored along with this message.
        /// </summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
