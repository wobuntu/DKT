﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Interfaces;
using System.Collections.Generic;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that the system was requested to shut down. Note that it is not automatically logged (does not
    /// implement <see cref="ILogMessage"/>).
    /// Created by Mathias Lackner.
    /// </summary>
    public class ApplicationShutdownMessage : IMessage
    {
        /// <summary>Empty default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        public ApplicationShutdownMessage() { }

        /// <summary>
        /// An dictionary containing additional message data sent along with this message. For the <see cref="ApplicationShutdownMessage"/>
        /// it will always be empty.
        /// </summary>
        public IReadOnlyDictionary<string, object> MessageData { get; } = new Dictionary<string, object>();
    }
}
