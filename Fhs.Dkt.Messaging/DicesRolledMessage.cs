﻿// CREATED BY GABRIEL DAX
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that dices has been rolled. It contains the two numbers of the rolled dices.
    /// Creared by Gabriel Dax.
    /// </summary>
    public class DicesRolledMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private DicesRolledMessage() { }


        /// <summary>
        /// Initializes a new instance of the <see cref="DicesRolledMessage{T}"/> class.
        /// </summary>
        /// <param name="firstDiceNumber">The rolled number from the first dice.</param>
        /// <param name="secondDiceNumber">The rolled number from the secound dice.</param>
        public DicesRolledMessage(byte firstDiceNumber, byte secondDiceNumber)
        {
            // Store the numbers in the dictionary, so that even elements which do
            // not know this MessageType can access those easily
            this.messageData["firstDice"] = firstDiceNumber.ToString();
            this.messageData["secondDice"] = secondDiceNumber.ToString();
        }

        /// <summary>
        /// Gets the rolled number if the first dice.
        /// </summary>
        public byte FirstDiceNumber => byte.Parse((string)this.messageData["firstDice"]);

        /// <summary>
        /// Gets the rolled number if the secound dice.
        /// </summary>
        public byte SecondDiceNumber => byte.Parse((string)this.messageData["secondDice"]);

        /// <summary>
        /// Gets the message which is displayed in the log.
        /// </summary>
        public string MessageDescription
        {
            get
            {
                return $"Dice was rolled, numbers: {FirstDiceNumber} and {SecondDiceNumber}";
            }
        }

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        private Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
