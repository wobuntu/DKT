﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;


/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that a player moved. It contains additional data about the player who moved
    /// via its properties or the <see cref="MessageData"/> dictionary.
    /// Created by Mathias Lackner.
    /// </summary>
    public class PlayerMovedMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private PlayerMovedMessage() { }


        /// <summary>
        /// Creates a new instance of the <see cref="PlayerMovedMessage"/>, requiring the player ID, the old field id and the new
        /// field id to which the player moved to.
        /// </summary>
        /// <param name="playerId">The ID of the player who moved.</param>
        /// <param name="oldFieldId">The old field ID where the player stood before she/he moved.</param>
        /// <param name="newFieldId">The new field ID to which the player moved.</param>
        public PlayerMovedMessage(Guid playerId, byte oldFieldId, byte newFieldId)
        {
            messageData["playerId"] = playerId.ToString();
            messageData["oldFieldId"] = oldFieldId.ToString();
            messageData["newFieldId"] = newFieldId.ToString();
            messageData["fieldsMoved"] = ((newFieldId >= oldFieldId) ? (newFieldId - oldFieldId) : (oldFieldId - newFieldId)).ToString();
        }

        /// <summary>Gets the ID of the player who moved.</summary>
        public Guid PlayerId => Guid.Parse((string)messageData["playerId"]);

        /// <summary>Gets the amount of fields the player moved.</summary>
        public int FieldsMoved => int.Parse((string)messageData["fieldsMoved"]);

        /// <summary>Gets the old field ID where the player stood before she/he moved.</summary>
        public byte OldFieldId => byte.Parse((string)messageData["oldFieldId"]);

        /// <summary>Gets the new field ID to which the player moved.</summary>
        public byte NewFieldId => byte.Parse((string)messageData["newFieldId"]);

        /// <summary>Gets the message which is intended to appear in the log.</summary>
        public string MessageDescription => $"Player with ID {PlayerId} moved from field Id {OldFieldId} to {NewFieldId} ({FieldsMoved} fields)";

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        private Dictionary<string, object> messageData = new Dictionary<string, object>();
        
        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
