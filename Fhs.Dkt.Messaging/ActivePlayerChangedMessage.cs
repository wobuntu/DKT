﻿using Fhs.Dkt.Interfaces;
using System;
using System.Collections.Generic;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that a player changed to a active player. It contains data about the player.
    /// Creared by Gabriel Dax.
    /// </summary>
    public class ActivePlayerChangedMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private ActivePlayerChangedMessage() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivePlayerChangedMessage{T}"/> class.
        /// </summary>
        /// <param name="playerId">The unique id for the player.</param>
        public ActivePlayerChangedMessage(Guid playerId)
        {
            messageData["playerId"] = playerId.ToString();
        }

        /// <summary>
        /// Gets the id of the player which changed to active.
        /// </summary>
        public Guid PlayerId => Guid.Parse((string)messageData["playerId"]);

        /// <summary>
        /// Gets the message which is displayed in the log.
        /// </summary>
        public string MessageDescription => $"Player with ID {PlayerId} has been set as the active player.";

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        private Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
