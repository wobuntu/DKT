﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Interfaces;
using System;
using System.Collections.Generic;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that a card has been drawn. It contains additional data about the player who has drawn the
    /// card and the card itself via its properties or the <see cref="MessageData"/> dictionary. Note that it is not required to pass
    /// a clone of the card for security reasons, since only some meta data is stored, not the card as an object itself.
    /// Created by Mathias Lackner.
    /// </summary>
    public class CardDrawnMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private CardDrawnMessage() { }

        /// <summary>
        /// Creates a new instance of a <see cref="CardDrawnMessage"/>, requiring the player ID of the player who has drawn the card and
        /// the card itself. Note that it is not required to pass a clone of the card for security reasons, since only some meta data is
        /// stored, not the card as an object itself.
        /// </summary>
        /// <param name="playerId">The ID of the player who drew the card.</param>
        /// <param name="card">The card object which was drawn.</param>
        public CardDrawnMessage(Guid playerId, Types.CardBase card)
        {
            messageData["playerId"] = playerId.ToString();
            messageData["cardText"] = card.Action.GetFormattedText();
            messageData["cardType"] = card.CardType.ToString();
            messageData["cardAction"] = card.Action;
        }

        /// <summary>Gets the ID of the player who has drawn the card.</summary>
        public Guid PlayerId => Guid.Parse((string)messageData["playerId"]);

        /// <summary>Gets the type of the drawn card.</summary>
        public Types.CardType CardType => (Types.CardType)Enum.Parse(typeof(Types.CardType), (string)messageData["cardType"]);

        /// <summary>Gets the action of the drawn card.</summary>
        public Types.CardAction CardAction => (Types.CardAction)messageData["cardAction"];

        /// <summary>Gets the card text.</summary>
        public String CardText => (String)messageData["cardText"];

        /// <summary>Gets the message which is intended to appear in the log.</summary>
        public string MessageDescription => $"The player with ID {PlayerId} has drawn a {CardType} with Action {CardAction}";

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        [Newtonsoft.Json.JsonProperty]
        private Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
