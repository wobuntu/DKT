﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fhs.Dkt.Interfaces;
using Fhs.Dkt.Types;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that a property changed. It contains additional data about the property which changed.
    /// Created by Mathias Lackner.
    /// </summary>
    public class PropertyChangedMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private PropertyChangedMessage() { }

        /// <summary>
        /// Creates a new instance of the <see cref="PropertyChangedMessage"/>, requiring the property ID and an optional description
        /// about the change.
        /// </summary>
        /// <param name="propertyId">The ID of the property which changed.</param>
        /// <param name="logDescription">An optional description of the change.</param>
        public PropertyChangedMessage(PropertyBase propertyToClone, string logDescription = "-")
        {
            messageData["property"] = propertyToClone.Clone();
            messageData["logDescription"] = logDescription;
        }

        /// <summary>Gets the ID of the changed property.</summary>
        public PropertyBase Property => (PropertyBase)messageData["property"];

        /// <summary>Gets the message which is intended to appear in the log.</summary>
        public string MessageDescription => $"Property with ID {Property.ID} changed (Details: {messageData["logDescription"]})";

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        [Newtonsoft.Json.JsonProperty]
        private Dictionary<string, object> messageData = new Dictionary<string, object>();
        
        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
