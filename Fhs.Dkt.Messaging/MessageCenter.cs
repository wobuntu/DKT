﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;


/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This internal class provides the ability to subscribe to message objects implementing <see cref="IMessage"/>. For usage outside
    /// of the current assembly, use the <see cref="MessagingExtensions"/>. The open source project pubsub (https://github.com/upta/pubsub)
    /// was the base for this class, but was adapted/simplified to fit our needs. 
    /// Created by Mathias Lackner.
    /// </summary>
    internal static class MessageCenter
    {
        /// <summary>This private list stores all subscriptions for messages of any type.</summary>
        private static List<Subscription> subscriptions = new List<Subscription>();

        /// <summary>
        /// Subscribes the given object to message-objects of type <typeparamref name="T"/> (must implement <see cref="IMessage"/>) and
        /// executes the <paramref name="onNotify"/> action everytime such a message is received.
        /// Note that if one of the arguments is invalid or null, no exceptions are thrown and no subscriptions are made.
        /// </summary>
        /// <typeparam name="T">The type of messages to subscribe to. It must implement <see cref="IMessage"/>.</typeparam>
        /// <param name="subscriber">The object which subscribes for messages of <typeparamref name="T"/>.</param>
        /// <param name="onNotify">The action which will be executed everytime a message-object of type <typeparamref name="T"/>
        /// is received.</param>
        public static void SubscribeTo<T>(object subscriber, Action<T> onNotify) where T : IMessage
        {
            if (subscriber == null || onNotify == null)
                return;

            subscriptions.Add(new Subscription()
            {
                Subscriber = new WeakReference(subscriber),
                SubscriptionType = typeof(T),
                OnNotify = onNotify
            });
        }


        /// <summary>
        /// Unsubscribes the given object from receiving message objects of type <typeparamref name="T"/>. After this method was called,
        /// the registered callback will not be executed again. Note that no exceptions are thrown if the object is null or if it does
        /// not receive messages of type <typeparamref name="T"/> yet.
        /// </summary>
        /// <typeparam name="T">The type of message which shall no longer be received by the <paramref name="subscriber"/>.</typeparam>
        /// <param name="subscriber">The object which shall unsubsribe from messages of type <typeparamref name="T"/>.</param>
        public static void UnsubscribeFrom<T>(object subscriber) where T : IMessage
        {
            if (subscriber == null)
                return; // Nothing to do.

            Type msgType = typeof(T);

            // Iterate from back to front because we delete entries within the loop
            for (int i = subscriptions.Count - 1; i >= 0; i--)
            {
                if (!subscriptions[i].Subscriber.IsAlive)
                    // Remove all subscriptions which are not valid anymore (object references are dead)
                    subscriptions.RemoveAt(i);
                else if (subscriptions[i].Subscriber.Target.Equals(subscriber)
                    && subscriptions[i].SubscriptionType == msgType)
                    // Remove all subscriptions for the specific subscriber and message type
                    subscriptions.RemoveAt(i);
            }
        }


        /// <summary>
        /// Unsubscribes the given object from all messages. After this method was called, the registered callbacks will not be executed
        /// again. Note that no exceptions are thrown if the object is null or if it does not receive any messages yet.
        /// </summary>
        /// <param name="subscriber">The object which shall unsubscribe from all messages.</param>
        public static void UnsubscribeFromAll(object subscriber)
        {
            if (subscriber == null)
                return; // Nothing to do.

            // Iterate from back to front because we delete entries within the loop
            for (int i = subscriptions.Count - 1; i >= 0; i--)
            {
                if (!subscriptions[i].Subscriber.IsAlive)
                    // Remove all subscriptions which are not valid anymore (object references are dead)
                    subscriptions.RemoveAt(i);
                else if (subscriptions[i].Subscriber.Target.Equals(subscriber))
                    // Remove all subscriptions for the specific subscriber and message type
                    subscriptions.RemoveAt(i);
            }
        }


        /// <summary>
        /// Sends a new message of type <typeparamref name="T"/>. The <see cref="MessageCenter"/> will take care that all objects which
        /// subscribed to it will receive the message (this will invoke their registered callbacks for this message type).
        /// </summary>
        /// <typeparam name="T">The type of the message to send (must implement <see cref="IMessage"/>).</typeparam>
        /// <param name="message">The message to send.</param>
        public static void Notify<T>(T message) where T : IMessage
        {
            Type msgType = typeof(T);

            Notify(msgType, message);
        }

        public static void Notify(Type msgType, IMessage message)
        {
            // Prepare a list big enough to hold all subscriptions
            List<Subscription> matches = new List<Subscription>(subscriptions.Count);

            // Check all registered subscriptions which wait for messages of type T. In the same run, delete unnecessary subscriptions
            // which are not required anymore.
            for (int i = subscriptions.Count - 1; i >= 0; i--)
            {
                if (!subscriptions[i].Subscriber.IsAlive)
                {
                    subscriptions.RemoveAt(i);
                }
                else if (subscriptions[i].SubscriptionType.IsAssignableFrom(msgType))
                    matches.Add(subscriptions[i]);
            }

            // We iterated the list before from back to front, so do this again for invoking the actions to preserve the order
            for (int i = matches.Count - 1; i >= 0; i--)
                // The action will never be null, since we checked that before creating the subscription
                matches[i].OnNotify.DynamicInvoke(message);

            // Also invoke the messaging component on the remote, but be sure to not create an endless message loop by writing a
            // flag to the message
            if (message.MessageData == null || message.MessageData.ContainsKey("___CAME_FROM_REMOTE___"))
                return;

            var dict = (IDictionary<string, object>)message.MessageData;
            dict.Add("___CAME_FROM_REMOTE___", null);

            RemoteConnector.RemoteInvokeAsync(
                new Types.RemoteInvocationInfo(
                    componentName: "___custom___",
                    targetName: "___notify___",
                    typeParams: null,
                    returnExpected: false,
                    parameters: message));
        }


        /// <summary>
        /// Protected struct representing a subscription.
        /// </summary>
        protected struct Subscription
        {
            /// <summary>
            /// Holds a reference to the subscriber (A WeekReference is used, which does not prevent the garbage collector from deleting
            /// the object -- see https://msdn.microsoft.com/de-de/library/system.weakreference(v=vs.110).aspx).
            /// </summary>
            public WeakReference Subscriber;

            /// <summary>
            /// The delegate which shall be executed after a receiver got a message.
            /// </summary>
            public Delegate OnNotify;

            /// <summary>
            /// The type of the message for which this subscription was created.
            /// </summary>
            public Type SubscriptionType;
        }
    }
}
