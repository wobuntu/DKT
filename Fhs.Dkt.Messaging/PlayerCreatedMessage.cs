﻿// CREATED BY MATHIAS LACKNER
using System;
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that a player was created. It contains additional data about the player who was created
    /// via its properties or the <see cref="MessageData"/> dictionary.
    /// Created by Mathias Lackner.
    /// </summary>
    public class PlayerCreatedMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private PlayerCreatedMessage() { }

        /// <summary>
        /// Creates a new instance of the <see cref="PlayerCreatedMessage"/>, requiring the player name, ID and if she/he is an AI.
        /// </summary>
        /// <param name="playerName">The name of the player which was created.</param>
        /// <param name="playerGuid">The ID of the player which was created.</param>
        /// <param name="isAi">Indicates if the created player is an AI.</param>
        public PlayerCreatedMessage(string playerName, Guid playerGuid, bool isAi)
        {
            // Problems occurred during deserialization, therefore we store everything as strings here
            this.messageData["playerName"] = playerName;
            this.messageData["playerGuid"] = playerGuid.ToString();
            this.messageData["playerIsAI"] = isAi.ToString();
        }

        /// <summary>Gets the name of the created player.</summary>
        public string Name => (string)messageData["playerName"];

        /// <summary>Gets the ID of the created player.</summary>
        public Guid Guid => Guid.Parse((string)messageData["playerGuid"]);

        /// <summary>Specifies if the created player is an AI.</summary>
        public bool IsAI => bool.Parse((string)messageData["playerIsAI"]);

        /// <summary>Gets the message which is intended to appear in the log.</summary>
        public string MessageDescription => $"A player with name '{this.Name}' (Guid: {this.Guid}) was created.";

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        private Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
