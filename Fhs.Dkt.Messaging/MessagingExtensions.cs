﻿// CREATED BY MATHIAS LACKNER
using System;

using Fhs.Dkt.Interfaces;


/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This class provides extension methods for convenient access to the functionality of the <see cref="MessageCenter"/>. Use it to
    /// subscribe or unsubscribe objects to specific message types, or to send new messages.
    /// Created by Mathias Lackner.
    /// </summary>
    public static class MessagingExtensions
    {
        /// <summary>
        /// Subscribes the current object to message-objects of type <typeparamref name="T"/> (must implement <see cref="IMessage"/>) and
        /// executes the <paramref name="onNotify"/> action everytime such a message is received.
        /// Note that if one of the arguments is invalid or null, no exceptions are thrown and no subscriptions are made.
        /// </summary>
        /// <typeparam name="T">The type of messages to subscribe to. It must implement <see cref="IMessage"/>.</typeparam>
        /// <param name="obj">The current object which will be the subcriber of the messages of type <typeparamref name="T"/>.</param>
        /// <param name="onNotify">The action which will be executed everytime a message-object of type <typeparamref name="T"/>
        /// is received.</param>
        public static void SubscribeTo<T>(this object obj, Action<T> onNotify) where T : IMessage
        {
            MessageCenter.SubscribeTo<T>(obj, onNotify);
        }


        /// <summary>
        /// Unsubscribes the current object from receiving message objects of type <typeparamref name="T"/>. After this method was called,
        /// the registered callback will not be executed again. Note that no exceptions are thrown if the object is null or if it does
        /// not receive messages of type <typeparamref name="T"/> yet.
        /// </summary>
        /// <typeparam name="T">The type of message which shall no longer be received by the <paramref name="subscriber"/>.</typeparam>
        /// <param name="obj">The current object which shall unsubsribe from messages of type <typeparamref name="T"/>.</param>
        public static void UnsubscribeFrom<T>(this object obj) where T : IMessage
        {
            MessageCenter.UnsubscribeFrom<T>(obj);
        }


        /// <summary>
        /// Unsubscribes the current object from all messages. After this method was called, the registered callbacks will not be executed
        /// again. Note that no exceptions are thrown if the object is null or if it does not receive any messages yet.
        /// </summary>
        /// <param name="obj">The object which shall unsubscribe from all messages.</param>
        public static void UnsubscribeAll(this object obj)
        {
            MessageCenter.UnsubscribeFromAll(obj);
        }


        /// <summary>
        /// Sends a new message of type <typeparamref name="T"/>. The <see cref="MessageCenter"/> will take care that all objects which
        /// subscribed to it will receive the message (this will invoke their registered callbacks for this message type).
        /// </summary>
        /// <typeparam name="T">The type of the message to send (must implement <see cref="IMessage"/>).</typeparam>
        /// <param name="message">The message to send.</param>
        public static void Notify<T>(this object obj, T message) where T : IMessage
        {
            MessageCenter.Notify<T>(message);
        }

        public static void Notify(this object obj, IMessage message)
        {
            MessageCenter.Notify(message.GetType(), message);
        }
    }
}
