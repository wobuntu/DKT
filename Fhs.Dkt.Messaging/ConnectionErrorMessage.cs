﻿using Fhs.Dkt.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate, that an connection error occurred.
    /// Created by Mathias Lackner.
    /// </summary>
    public class ConnectionErrorMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private ConnectionErrorMessage() { }

        /// <summary>
        /// Creates a new instance of the <see cref="ConnectionErrorMessage"/>. It is used to indicate, that an connection error ocurred.
        /// </summary>
        /// <param name="componentName">The name of the component which could not be accessed.</param>
        public ConnectionErrorMessage(string componentName)
        {
            messageData["componentName"] = componentName;
        }

        /// <summary>Gets the name of the component which wasn't accessible.</summary>
        public string ComponentName => (string)messageData["componentName"];

        /// <summary>Gets the message which is intended to appear in the log.</summary>
        public string MessageDescription => $"Cannot connect to the cloud component '{this.ComponentName}'.";

        /// <summary>Gets the log level of this message.</summary>
        public LogLvl LogLevel => LogLvl.Error;

        /// <summary>Private dictionary containing all data which is made public via the properties of this class.</summary>
        public Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>An dictionary containing all data which is stored along with this message.</summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
