﻿// CREATED BY LUKAS BRUGGER
using System;
using System.Collections.Generic;

using Fhs.Dkt.Interfaces;

/// <summary>
/// This namespace contains the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> class which is used to send application wide messages to all
/// subscribers, while utilizing loose coupling. Additionally, extension methods are provided for convenient access of the functionality.
/// See the <see cref="Fhs.Dkt.Messaging.MessageCenter"/> for more details.
/// </summary>
namespace Fhs.Dkt.Messaging
{
    /// <summary>
    /// This message class is used to indicate that a player has gone bancrupt. 
    /// It contains the bancrupt player's id
    /// Created by Lukas Brugger
    /// </summary>
    public class PlayerBancruptMessage : ILogMessage
    {
        /// <summary>Private default constructor used during deserialization</summary>
        [Newtonsoft.Json.JsonConstructor]
        private PlayerBancruptMessage() { }


        /// <summary>
        /// Creates a new instance of the <see cref="PlayerBancruptMessage"/>.
        /// The id of the player who was gone bancrupt is required
        /// </summary>
        /// <param name="playerId">Guid of player who has gone bancrupt</param>
        public PlayerBancruptMessage(Guid playerId)
        {
            messageData["playerId"] = playerId.ToString();
        }

        /// <summary>
        /// Returns the id of the player who has gone bancrupt
        /// </summary>
        public Guid PlayerId => Guid.Parse((string)messageData["playerId"]);

        /// <summary>
        /// Returns the message that will be logged
        /// </summary>
        public string MessageDescription => $"Player with ID {PlayerId} is bancrupt";

        /// <summary>
        /// Returns the message's log level
        /// </summary>
        public LogLvl LogLevel => LogLvl.Info;

        /// <summary>
        /// Private dictionary containing all data which is made public via the properties of this class.
        /// </summary>
        private Dictionary<string, object> messageData = new Dictionary<string, object>();

        /// <summary>
        /// A dictionary containing all data which is stored along with this message.
        /// </summary>
        public IReadOnlyDictionary<string, object> MessageData => messageData;
    }
}
