﻿// CREATED BY LUKAS BRUGGER
using System;

/// <summary>
/// This namespace contains all specialized exceptions used within this solution across all components (Part of the core assembly).
/// </summary>
namespace Fhs.Dkt.Exceptions
{
    /// <summary>
    /// This exception indicates, that a bank transaction was executed without the right permissions to do so, because a wrong 
    /// private account key was provided for the method
    /// </summary>
    public class BankActionNotPermittedException : Exception
    {
        /// <summary>
        /// Creates a new BankActionNotPermittedException, indicating that a bank transaction was executed without
        /// the permission to do so
        /// </summary>
        /// <param name="message">The message to display for this exception</param>
        public BankActionNotPermittedException(string message) : base(message) { }
    }
}
