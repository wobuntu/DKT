﻿// CREATED BY LUKAS BRUGGER
using System;

/// <summary>
/// This namespace contains all specialized exceptions used within this solution across all components (Part of the core assembly).
/// </summary>
namespace Fhs.Dkt.Exceptions
{
    /// <summary>This exception indicates that a action or bank card could not format its data to text.</summary>
    public class InvalidCardTextException : Exception
    {
        /// <summary>
        /// Creates a new InvalidCardException indicating that a bank or action card could not format its data to text.
        /// </summary>
        public InvalidCardTextException() { }
        
        /// <summary>
        /// Creates a new InvalidCardException indicating that a bank or action card could not format its data to text.
        /// </summary>
        /// <param name="msg">The message to display for this exception.</param>
        public InvalidCardTextException(string msg)
        {
            this.msg = msg;
        }

        /// <summary>Private variable containing the exception message.</summary>
        private string msg = "The text of the card is invalid and cannot be formatted. Unassigned but required properties?";

        /// <summary>Holds the message describing what caused the exception.</summary>
        public override string Message => msg;
    }
}
