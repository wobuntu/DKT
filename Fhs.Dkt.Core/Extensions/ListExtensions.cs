﻿// CREATED BY LUKAS BRUGGER
using System;
using System.Collections.Generic;

/// <summary>
/// This namespace contains extension methods used across all components of the solution (Part of the core assembly).
/// </summary>
namespace Fhs.Dkt.Extensions
{
    /// <summary>
    /// Class holding extension helper methods for generic list types
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Static helper class to provide a thread safe random number generator
        /// Used for the list Shuffle extension method
        /// </summary>
        public static class ThreadSafeRandom
        {
            [ThreadStatic] private static Random Local;

            public static Random ThisThreadsRandom
            {
                get { return Local ?? (Local = new Random(unchecked(Environment.TickCount * 31 + System.Threading.Thread.CurrentThread.ManagedThreadId))); }
            }
        }

        /// <summary>
        /// Extension method for generic list type to shuffle the list
        /// </summary>
        /// <typeparam name="T">Generic type parameter</typeparam>
        /// <param name="list">The list that the shuffle method will act upon</param>
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}
