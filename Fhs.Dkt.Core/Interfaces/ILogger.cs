﻿// CREATED BY MATHIAS LACKNER
using System;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Instances which implement this interface are responsible for writing logs. Use an overload of
    /// <see cref="Report(string, LogLvl)"/> to add a new entry to the logs.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface ILogger : IDktComponent
    {
        /// <summary>
        /// Reports the given <paramref name="message"/> to the logging component.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        /// <param name="progressPercentage">A percentage indicating a the current progress.</param>
        /// <param name="logLevel">The log level to use while writing the given information to the log.</param>
        void Report(String message, double progressPercentage, LogLvl logLevel = LogLvl.Info);


        /// <summary>
        /// Reports the given <paramref name="message"/> to the logging component.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        /// <param name="logLevel">The log level to use while writing the the given information to the log.</param>
        void Report(String message, LogLvl logLevel = LogLvl.Info);


        /// <summary>
        /// Reports the given <paramref name="message"/> to the logging component.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        /// <param name="exception">An exception object which will be included in the log output.</param>
        /// <param name="logLevel">The log level to use while writing the given information to the log.</param>
        void Report(String message, Exception exception, LogLvl logLevel = LogLvl.Error);


        /// <summary>
        /// Reports the given <paramref name="exception"/> to the logging component.
        /// </summary>
        /// <param name="exception">The exception which will be written into the log.</param>
        /// <param name="logLevel">The log level to use while writing the given information to the log.</param>
        void Report(Exception exception, LogLvl logLevel = LogLvl.Error);
    }
}
