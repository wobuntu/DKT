﻿// CREATED BY MATHIAS LACKNER

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Indicates that objects implementing this interface provide a method to create a deep clone of themselves.
    /// Created by Mathias Lackner.
    /// </summary>
    /// <typeparam name="T">The type of the resulting clone.</typeparam>
    public interface IDeepCloneable<T>
    {
        /// <summary>Returns a deep clone of the current object.</summary>
        T GetClone();
    }
}
