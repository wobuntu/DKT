﻿// CREATED BY MATHIAS LACKNER

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Specifies the interface of the entry point component of this solution.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface ILauncher : IDktComponent
    {
        /// <summary>
        /// Forces the application to shutdown. Note that the caller must run on the same thread as the <see cref="ILauncher"/>.
        /// </summary>
        void Shutdown();

        /// <summary>
        /// Requests the application to shutdown (also sends an <see cref="Fhs.Dkt.Messaging.ApplicationShutdownMessage"/>), while waiting
        /// for all threads and components to shutdown and finish properly.
        /// </summary>
        void SoftShutdown();
    }
}
