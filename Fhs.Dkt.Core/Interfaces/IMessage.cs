﻿// CREATED BY MATHIAS LACKNER
using System.Collections.Generic;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// This interface is used for messages generated for the <see cref="Messaging.MessageCenter"/>. Classes which implement it may be
    /// sent via it or via the extension methods provided by <see cref="Messaging.MessagingExtensions"/>.
    /// See the <see cref="Messaging"/> namespace and its implementations for more information.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Provides a dictionary containing relevant meta data of the current <see cref="IMessage"/> implementation (e.g. how many 
        /// fields a player moved, etc.).
        /// </summary>
        IReadOnlyDictionary<string, object> MessageData { get; }
    }
}
