﻿// CREATED BY MATHIAS LACKNER

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Pseudo interface to provide a common ancestor for the type parameter of
    /// <see cref="DependencyService.Get{T}(DependencyService.ForceLoadSettings)"/>.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface IDktComponent
    {
    }
}