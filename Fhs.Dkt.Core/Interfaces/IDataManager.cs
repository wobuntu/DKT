﻿// CREATED BY MATHIAS LACKNER
using System.Collections.Generic;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Instances which implement this interface are responsible to serialize/deserialize objects and data from/to a defined source/target.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface IDataManager : IDktComponent
    {
        /// <summary>
        /// Deserializes a list of objects (of type <typeparamref name="T"/>) from a given source. If no source identifier is given, a
        /// default one is used. Note that the type of <typeparamref name="T"/> may also be a base class or an interface and
        /// that also private members are deserialized.
        /// </summary>
        /// <typeparam name="T">The type of the objects to deserialize (or a common base class or interface).</typeparam>
        /// <param name="source">The source to use or null if the default source for the given type shall be used. Depending on the
        /// specific implementation, this may be a file or a database table.</param>
        List<T> DeserializeList<T>(string source = null);


        /// <summary>
        /// Deserializes an object of type <typeparamref name="T"/> from a given source. If no source identifier is given, a default one
        /// is used. Note that the type of <typeparamref name="T"/> may also be a base class or an interface and that also private
        /// members are deserialized.
        /// </summary>
        /// <typeparam name="T">The type of the object to deserialize (or a common base class or interface).</typeparam>
        /// <param name="source">The source to use or null if the default file for the given type shall be used. Depending on the
        /// specific implementation, this may be a file or a database table.</param>
        T DeserializeObject<T>(string source = null);


        /// <summary>Serializes the given object to a given target. If no target is given, a default one is used.</summary>
        /// <typeparam name="T">The type of the object to serialize.</typeparam>
        /// <param name="toSerialize">The object which shall be serialized to the target.</param>
        /// <param name="target">The target to use. Depending on the specific implementaiton, this may be a file or a database table.
        /// </param>
        /// <param name="overwriteExisting">If set to true, the target to which the object shall be serialized will be overwritten if it
        /// already exists.</param>
        void SerializeObject<T>(T toSerialize, string target = null, bool overwriteExisting = true);
    }
}
