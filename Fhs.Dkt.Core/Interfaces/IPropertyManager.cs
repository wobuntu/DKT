﻿// CREATED BY BORIS BRANKOVIC
using System;
using System.Collections.Generic;

using Fhs.Dkt.Types;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// This interface contains all neccessary methods for the class PropertyManager.
    /// Components which implement this interface, are responsbile for changing the ownership of a property or adding houses/hotels to a property.
    /// Created by Boris Brankovic.
    /// </summary>
    public interface IPropertyManager : IDktComponent
    {
        /// <summary>
        /// The method "GetNameOfProp" returns the name of a specific HousingProperty, by checking the given ID.
        /// </summary>
        /// <param name="id">This parameter specifies a certain ID in the Properties. Each ID is connected to a Property.</param>
        /// <returns>The return value is a string, which represents a specific Property of the type "HousingProperty".</returns>
        string GetNameOfProp(int id);
        
        /// <summary>
        /// GetCityOfProp returns the name of a certain HousingProperty. 
        /// </summary>
        /// <param name="id">The name of the city is found, based on the given ID</param>
        /// <returns>The return value is of the type "City",  which contains all the Citys specified in a Json-File. If no city is found -> null will be returned</returns>
        City? GetCityOfProp(int id);

        /// <summary>
        /// The method "GetClone" returns the cloned property-objects based on the propertyID. 
        /// This method is called by the component "PlayerManager" everytime, a property is required by the view e.g. if a property is bought, 
        /// or if the property is affected by changes i.e. a house/hotel is bought. Therefor it is important to clone the original objects, because the view gets only copies of them 
        /// and the orignal ones stay at the component itself. 
        /// </summary>
        /// <param name="propertyId">By handing over this parameter, the property with the given ID will be cloned</param>
        /// <returns>The method returns the cloned object, based on the given property ID</returns>
        PropertyBase GetClone(byte propertyId);

        /// <summary>
        /// GetNumberOwnedProperties checks how many properties a certian has in his possession. 
        /// </summary>
        /// <param name="playerId">The specific Player-ID which is important for checking the number of properties.</param>
        /// <returns>The number (as byte) of all properties, which belong to specific Player.</returns>
        byte GetNumberOwnedProperties(Guid playerId);

        /// <summary>
        /// GetNumberOwnedProperties checks how many properties of a specific type
        /// e.g. HousingProperty, TrafficLineProperty or CompanyProperty a certian has in his possession. 
        /// </summary>
        /// <param name="playerId">The specific Player-ID which is important for checking the number of properties.</param>
        /// <param name="filter">The filter has to be handed over to the method to specify a certain property i.e. HousingProperty, TrafficLineProperty, CompanyProperty.</param>
        /// <returns>The number (as byte) of a specific property-type, which belongs to a certain player.</returns>
        byte GetNumberOwnedProperties(Guid playerId, PropertyType filter);

        /// <summary>
        /// GetNumberOwnedHouses checks how many houses a certain player has bought in total.
        /// </summary>
        /// <param name="playerId">The specific player ID, for checking which property is assigned to it.</param>
        /// <returns>The total number (in byte) of all houses from a certain Player</returns>
        byte GetNumberOwnedHouses(Guid playerId);

        /// <summary>
        /// GetNumberOwnedHotels checks how many hotels a certain player has bought in total.
        /// </summary>
        /// <param name="playerId">The specific player ID, for checking which property is assigned to it.</param>
        /// <returns>The total number (in byte) of all hotels from a certain Player</returns>
        byte GetNumberOwnedHotels(Guid playerId);

        /// <summary>
        /// This method is only for testing purposes. It adds a specified number of Houses or Hotels to a certain player.
        /// </summary>
        /// <param name="playerID">Specific Player ID</param>
        /// <returns>Returns a boolean-"True" if the house/hotel was successfully added, otherwise boolean-"False".</returns>
        bool AddHouse(Guid playerID);

        /// <summary>
        /// GetownedProperties returns a IReadOnlyList of all properties, which are assigned to a certian player.
        /// The properties will only be represent as IDs of the type byte.
        /// </summary>
        /// <param name="playerID">The specific Player-ID for checking the owned properties in the ownership-dictionary.</param>
        /// <returns>Returns a IReadOnlyList with the PropertyIDs, assigned to a certain player.</returns>
        IReadOnlyList<byte> GetOwnedProperties(Guid playerID);

        /// <summary>
        /// AssignPlayerToProp assigns a property to a specific player. 
        /// This is done by checking the Ownership-Dictionary. If the player is not in the List a new entry will be made
        /// and the "ID" of the Property will be assigned to the Players-ID. 
        /// </summary>
        /// <param name="ownerID">The specific ID, which each player gets on creation i.e. when the game starts.</param>
        /// <param name="propertyID">A certain ID, which is connected to a property.</param>
        void AssignPlayerToProp(Guid ownerID, byte propertyID);

        /// <summary>
        /// Remove a property from a specific player.
        /// This is done by checking the Ownership-Dictionary - if the given "ownerID" contains the property which should be removed,
        /// then exactly this property will be immediately removed from the Owner.
        /// </summary>
        /// <param name="ownerID">The ownerID specifies the Player to whom the property belongs to.</param>
        /// <param name="propertyID">The propertyID reprents the property, which should be removed.</param>
        /// <returns>The method returns a boolean-"true", if the property was successfully removed, otherwise a boolean-"false".</returns>
        bool RemovePropFromOwner(Guid ownerID, byte propertyID);

        /// <summary>
        /// This method gets the owner of an specific property, by cheking the ownership of a given property ID.
        /// </summary>
        /// <param name="propertyID">Specific property ID, which is assigned to a certain player.</param>
        /// <returns>Returns the ID of the owner (type Guid), if no owner is specified then the return value is null.</returns>
        Guid? GetOwnerOfProperty(byte propertyID);

        /// <summary>
        /// Gets the relative ownership of a certain player in connection to a property ID,
        /// i.e. it checks whether the property can be bought by the current Player, if the property belongs to the current player, 
        /// if the bank is owner of the property, or if the property belongs to another player.
        /// This functionality is decisve for the game logic, it makes it possible to decide which step will be executed next. 
        /// </summary>
        /// <param name="playerId">The ID of the current player.</param>
        /// <param name="propertyID">The ID of the property, where the player currently is located in the game.</param>
        /// <returns>Returns an Enum of Relative-Ownerships</returns>
        OwnerShip GetRelativeOwnership(Guid ownerID, byte propertyID);

        /// <summary>
        /// This method checks in combination with different steps, whether the current player is allowed to buy a house on a certain property.
        /// Different steps will be verifcated and finally if the player is the owner of all streets from a specific city and if he has no hotels, or less than 4 houses built,
        /// then the player is allowed to buy a house.
        /// </summary>
        /// <param name="playerId">The ID of the current Player which has to be checked in combination with the given property ID.</param>
        /// <param name="propertyId">The ID of a certain property, which needs to be verificated by different steps.</param>
        /// <returns>Returns a boolean-"true" if the current Player is allowed to buy a house on a certain property, otherwise a boolean-"false".</returns>
        bool PlayerAllowedToBuyHouse(Guid playerId, byte propertyId);

        /// <summary>
        /// This method checks in combination with different steps, whether the current player is allowed to buy a hotel on a certain property.
        /// Different steps will be verifcated and finally if the player has 4 houses built on the given property, and no hotels built on it, 
        /// then the player is allowed to buy a hotel.
        /// </summary>
        /// <param name="playerId">The ID of the current Player which has to be checked in combination with the given property ID.</param>
        /// <param name="propertyId">The ID of a certain property, which needs to be verificated by different steps.</param>
        /// <returns>Returns a boolean-"true" if the current Player is allowed to buy a hotel on a certain property, otherwise a boolean-"false".</returns>
        bool PlayerAllowedToBuyHotel(Guid playerId, byte propertyId);

        /// <summary>
        /// This method is called if a player is allowed to buy a certain property.
        /// First the relative-ownership will be checked,
        /// i.e. if the bank is the owner of the desired property, or if it is not buyable because the current player owns it already.
        /// If the property has been bought, the view will be immediately informed about this change and the number of owned properties will be increased.
        /// </summary>
        /// <param name="playerId">The ID of the current player who wants to buy a certain property.</param>
        /// <param name="propertyId">The ID of a certain property, which should be bought.</param>
        /// <returns>Returns a boolean-"True" if the property was successfully bought, otherwise boolean-"False".</returns>
        bool TryBuyProperty(Guid playerId, byte propertyId);

        /// <summary>
        /// This method is called if the player is allowed to buy a house on a certain property. 
        /// This will be checked by calling the previous defined methods "PlayerIsOWner" and "PlayerAllowedToBuyHouse".
        /// The desired property will be get by searching for it in the list of all properties of type "HousingProperty". 
        /// If the house was added to the property -> the view will get immediately a notification about a change: 
        /// a house will be drawn and the number of owned houses will be increased.
        /// </summary>
        /// <param name="playerId">The ID of the current player who wants to buy a house on his property.</param>
        /// <param name="propertyId">The ID of a certain property, where the house should be built on it.</param>
        /// <returns>Returns a boolean-"True" if the house was successfully added to the given property, otherwise boolean-"False".</returns>
        bool TryAddHouse(Guid playerId, byte propertyId);

        /// <summary>
        /// This method is called if the player is allowed to buy a hotel on a certain property. 
        /// This will be checked by calling the previous defined methods "PlayerIsOWner" and "PlayerAllowedToBuyHotel".
        /// The desired property will be get by searching for it in the list of all properties of type "HousingProperty". 
        /// If the hotel was added to the property -> the view will get immediately a notification about a change: 
        /// a hotel will be drawn and the number of owned hotels will be increased.
        /// </summary>
        /// <param name="playerId">The ID of the current player who wants to buy a hotel on his property.</param>
        /// <param name="propertyId">The ID of a certain property, where the hotel should be built on it.</param>
        /// <returns>Returns a boolean-"True" if the hotel was successfully added to the given property, otherwise boolean-"False".</returns>
        bool TryAddHotel(Guid playerId, byte propertyId);

        /// <summary>
        /// This method determines whether the current player is the owner of a certain property.
        /// The verification is done by checking if the ownership-dictionary contains the given player ID
        /// --> if this step is true then the further verification is done by checking whether this player contains the given property ID. 
        /// </summary>
        /// <param name="playerId">The ID of a specific player. Important for the verification of an ownership.</param>
        /// <param name="propertyId">The ID of a specific property, which has to be checked, if it belongs to a player.</param>
        /// <returns>Returns a boolean-"True" if the Player is the owner of the current property, otherwise boolean-"False".</returns>
        bool PlayerIsOwner(Guid playerId, byte propertyId);

        /// <summary>
        /// GetFieldType checks on which field-type the current player is located.
        /// This verification is done by comparing the given property ID with a defined field-type.
        /// If the property ID is invalid i.e. the ID number is out of a specific range -> an ArgumentException will be thrown. 
        /// </summary>
        /// <param name="propertyId">The ID of the property, where the player is located.</param>
        /// <returns>Returns an Enum (FieldType).</returns>
        FieldType GetFieldType(byte propertyId);
    }
}
