﻿// CREATED BY LUKAS BRUGGER
using System;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Interface definining all public methods for the CardManager component used in the game.
    /// Components implementing this interface are responsible for handling
    /// Bank- and RiskCard decks and to provide methods to draw from those decks
    /// Created by Lukas Brugger
    /// </summary>
    public interface ICardManager : IDktComponent
    {

        /// <summary>
        /// Method that draws a card from one of the decks based on the CardType passed to the function 
        /// and returns the drawn card
        /// </summary>
        /// <param name="cardType">Type of the card to be drawn, based on enum CardType</param>
        /// <param name="playerId">Guid of player who draws the card</param>
        Types.CardBase DrawCard(Types.CardType cardType, Guid playerId);

        /// <summary>
        /// Helper method to draw card of the type "BankCard"
        /// Calls DrawCard with the cardType BankCard passed directly
        /// </summary>
        /// <param name="playerId">Guid of player who draws the card</param>
        Types.BankCard DrawBankCard(Guid playerId);

        /// <summary>
        /// Helper method to draw card of the type "RiskCard"
        /// Calls DrawCard with the cardType RiscCard passed directly
        /// </summary>
        /// <param name="playerId">Guid of player who draws the card</param>
        Types.RiskCard DrawRiskCard(Guid playerId);

        /// <summary>
        /// Method to check if a player currently holds the "Get Free From Jail" card
        /// </summary>
        /// <param name="playerId">Guid of player to check</param>
        bool PlayerHasJailCard(Guid playerId);

        /// <summary>
        /// Method to remove a card from the "lockedCards" list
        /// This is used when a player uses a "Get Free From Jail" Card, so it is put back into the stack
        /// to be drawn again
        /// </summary>
        /// <param name="playerId">Guid of player in locked cards list</param>
        void UnlockCard(Guid playerId);
    }
}
