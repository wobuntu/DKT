﻿// CREATED BY LUKAS BRUGGER
using System;
using System.Collections.Generic;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Interface definining all public methods for the Bank component used in the game.
    /// Components implementing this interface are responsible for handling the player's money balance and 
    /// money transactions between players.
    /// Created by Lukas Brugger
    /// </summary>
    public interface IBank : IDktComponent
    {
        /// <summary>
        /// Returns the BankAccount Id of the Bank instance itself
        /// </summary>
        Guid GetBankAccountId();

        /// <summary>
        /// Creates a new BankAccount for the passed player id. 
        /// Throws an Exception, if the player already holds an account with the bank instance.
        /// </summary>
        /// <param name="playerId">Guid of a player instance</param>
        Guid OpenAccount(Guid playerId);

        /// <summary>
        /// Returns the player/account id that holds the most funds.
        /// </summary>
        Guid GetWealthiestPlayer();

        /// <summary>
        /// Handles a transaction of money from one BankAccount to another.
        /// The method takes the sending players id as well as its secret bank key to be able to access the BankAccount, as well
        /// as the to be transferred amount and the recipients id.
        /// It returns a boolean value representing wether the transaction was successful or not. 
        /// </summary>
        /// <param name="playerId">Guid of the sending account/player instance</param>
        /// <param name="recipientId">Guid of the recieving account/player instance</param>
        /// <param name="key">Secret bankAccount key of the sending player</param>
        /// <param name="amount">Amount of money to be transferred</param>
        bool MakeTransaction(Guid playerId, Guid recipientId, Guid key, double amount);

        /// <summary>
        /// Helper method to ease the case of transferring money to the bank account.
        /// This calls MakeTransaction with the bank's account key taken directly from the bank instance, 
        /// instead of having to pass it to the method
        /// </summary>
        /// <param name="playerId">Guid of the sending account/player instance</param>
        /// <param name="key">Secret bankAccount key of the sending player</param>
        /// <param name="amount">Amount of money to be transferred</param>
        bool MakeBankTransaction(Guid playerId, Guid key, double amount);

        /// <summary>
        /// Returns the current balance of the account of the passed player id.
        /// Needs the secret bank key of the player
        /// Throws an exception if the key is not correct
        /// </summary>
        /// <param name="playerId">Guid of the account/player instance</param>
        /// <param name="key">Secret bankAccount key of the player</param>
        double GetAccountBalance(Guid playerId, Guid key);

        /// <summary>
        /// Helper method to ease the case of the bank sending money to a specific player.
        /// This calls MakeTransaction with the bank information being taken directly from the instance
        /// </summary>
        /// <param name="playerId">Guid of the recieving account/player instance</param>
        /// <param name="amount">Amount of money to be transferred</param>
        bool DepositAmount(Guid playerId, double amount);

        /// <summary>
        /// Method that transfers the fixed amount of 200.00 to a player in case he moves over the starting field
        /// </summary>
        /// <param name="playerId">Guid of the recieving account/player instance</param>
        bool DepositNewRoundAmount(Guid playerId);

        /// <summary>
        /// Method that checks, if the passed key is correct for the passed account/player id
        /// </summary>
        /// <param name="playerId">Guid of the account/player instance</param>
        /// <param name="key">Secret account key of the player</param>
        bool CheckAccountPermission(Guid playerId, Guid key);

        /// <summary>
        /// Returns the full transaction history recorded by the bank instance.
        /// </summary>
        List<string> GetTransactionList();
    }
}
