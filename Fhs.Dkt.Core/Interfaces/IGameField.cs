﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Types;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// The interface for the <see cref="IGameField"/> component of this solution. The game field is responsible to control the visual 
    /// output for the user and also to allow the other components the request of user input.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface IGameField : IDktComponent
    {
        /// <summary>
        /// Requests the game field to show up. Depending on the implementation, this may trigger a WPF window or a console to open.
        /// </summary>
        void Show();


        /// <summary>
        /// Requests the game field to close. Depending on the implementation, this may also trigger the component implementing
        /// <see cref="ILauncher"/> to stop.
        /// </summary>
        void Close();


        /// <summary>
        /// Asks the user to acknowledge that a dice will be rolled after confirming. Depending on the implementation, this may for
        /// example present the user a confirmation button to do so. The dice will not be rolled by this method, the caller is
        /// responsible to do so after this method returned.
        /// </summary>
        /// <param name="maxTimeout">The maximum amount of milliseconds to wait for a user input. Pass -1 to wait indefinitely long.
        /// </param>
        void WaitForRollDiceAcknowledge(int maxTimeout = -1);


        /// <summary>
        /// Asks the user to respond to a informative message. Depending on the implementation, this may present the user for example
        /// a confirmation button.
        /// </summary>
        /// <param name="text">A message to display during waiting for the user confirmation.</param>
        /// <param name="maxTimeout">The maximum amount of milliseconds to wait for a user input. Pass -1 to wait indefinitely long.
        /// </param>
        void WaitForAcknowledge(string text, int maxTimeout = -1);


        /// <summary>
        /// Asks the user for a decision. Depending on the implementation, this may present the user for example a dialog box with
        /// different options.
        /// </summary>
        /// <param name="msg">A message for the user, which describes what she/he shall decide.</param>
        /// <param name="caption">A caption for the decision.</param>
        /// <param name="propertyId">If a valid property ID is passed here, a short description of a property will be shown in addition
        /// to the message given in <paramref name="msg"/>.</param>
        /// <param name="buttons">The decision possibilities for the user. Predefined decision combinations are available in
        /// <see cref="UserChoice"/>, however a logical OR can be used to select arbitrary permutations.</param>
        /// <param name="textYes">The text for the 'Yes' decision (Depending on the implementation e.g. displayed on a button).</param>
        /// <param name="textNo">The text for the 'No' decision (Depending on the implementation e.g. displayed on a button).</param>
        /// <param name="textCancel">The text for the 'Cancel' decision (Depending on the implementation e.g. displayed on a button).
        /// </param>
        UserChoice WaitForDecision(string msg, string caption = null, byte propertyId = 0, UserChoice buttons = UserChoice.Yes,
            string textYes = "Ja", string textNo = "Nein", string textCancel = "Abbrechen");
    }
}
