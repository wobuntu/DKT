﻿// CREATED BY MATHIAS LACKNER

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// This interface extends the <see cref="IMessage"/> interface by providing properties which are required for the logging process.
    /// Classes which implement this interface and which are sent via the <see cref="Messaging.MessageCenter"/> or via the extension
    /// methods provided by <see cref="Messaging.MessagingExtensions"/> will be automatically received by the current implementation of
    /// <see cref="ILogger"/> component. See the <see cref="Messaging"/> namespace and its implementations for more information.
    /// Created by Mathias Lackner.
    /// </summary>
    public interface ILogMessage : IMessage
    {
        /// <summary>
        /// A descriptive message which will appear in the log.
        /// </summary>
        string MessageDescription { get; }

        /// <summary>
        /// The log level with which the <see cref="MessageDescription"/> will be written into the log.
        /// </summary>
        LogLvl LogLevel { get; }
    }
}
