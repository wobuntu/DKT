﻿// CREATED BY BORIS BRANKOVIC, GABRIEL DAX, LUKAS BRUGGER (alphabetical order)
using System;

/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    public interface IGameController : IDktComponent
    {
        void Start(int rounds);
        void Stop();

        /// <summary>A value which indicates if the component is within its working loop.</summary>
        bool IsRunning();

        string GetPlayerName(Guid playerId);
    }
}
