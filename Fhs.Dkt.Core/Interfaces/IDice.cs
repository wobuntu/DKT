﻿/// <summary>Contains interfaces which are used across different components of the solution.</summary>
namespace Fhs.Dkt.Interfaces
{
    /// <summary>
    /// Interface definining all public methods for the Dice component. The dice is responible to generate a random number as a thrown dice.
    /// Creared by Gabriel Dax.
    /// </summary>
    public interface IDice : IDktComponent
    {
        /// <summary>
        /// This method generates a random number between one and six (as rolled dice).
        /// </summary>
        /// <exception cref="ParameterError">Throws an exeption if the generation process went wrong</exception>
        /// <returns>The number as a <c>byte</c></returns>
        byte RollDice();
    }
}
