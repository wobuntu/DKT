﻿// CREATED BY LUKAS BRUGGER

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{

    /// <summary>
    /// This enum specifies the possible types of card actions in the game and is used by <see cref="CardBase"/> and its ancestors.
    /// The Card Action defines, what action is being taken when the card is drawn by a player.
    /// Created by Lukas Brugger
    /// </summary>
    public enum CardActionType
    {
        Payment = 0,
        Deposit = 1,
        Arrest = 2,
        MoveToField = 3,
        MoveForward = 4,
        MoveBackward = 5,
        GetOutOfJail = 6,
        RelativePayment = 7
    }
}
