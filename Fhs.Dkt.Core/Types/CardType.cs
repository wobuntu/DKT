﻿// CREATED BY LUKAS BRUGGER
/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This enum specifies the possible types of cards in the game and is used by <see cref="CardBase"/> and its ancestors,
    /// as well as in the UserControl "CardControl" in the User Interface
    /// Created by Lukas Brugger
    /// </summary>
    public enum CardType
    {
        BankCard = 0,
        RiscCard = 1
    }
}
