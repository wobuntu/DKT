﻿// CREATED BY BORIS BRANKOVIC

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This abstract class is inherited by the classes TrafficLineProperty, CompanyProperty and HousingProperty.
    /// It contains all important definitions for a property.
    /// Created by Boris Brankovic.
    /// </summary>
    public abstract class PropertyBase
    {
        /// <summary>
        /// The base constructor of PropertyBase defines the type, 
        /// the name, id and the buying price of a current property. It is inherited by the classes
        /// HousingProperty, TrafficLineProperty and CompanyProperty.
        /// The parameters will be set within the classes, which inherit PropertyBase.
        /// </summary>
        /// <param name="type">The type of the current property e.g. Housing, TrafficLine, Company.</param>
        /// <param name="name">The name of the current property as string.</param>
        /// <param name="id">The id of the current property as byte value.</param>
        /// <param name="buyingPrice">The buying price of the current property as double value.</param>
        protected PropertyBase(PropertyType type, string name, byte id, double buyingPrice)
        {
            this.Name = name;
            this.ID = id;
            this.BuyingPrice = buyingPrice;
            this.SellingPrice = buyingPrice / 2;
        }

        /// <summary>
        /// Declaring the getter Method "Name", which gets the name of a current property.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Declaring the getter Method "ID", which gets the ID of a current property.
        /// </summary>
        public byte ID { get; }
        /// <summary>
        /// Declaring the getter Method "BuyingPrice", which gets the buying price of a current property.
        /// </summary>
        public double BuyingPrice { get; }
        /// <summary>
        /// Declaring the getter Method "SellingPrice", which gets the name of a current property.
        /// </summary>
        public double SellingPrice { get; }

        /// <summary>
        /// Declaring the getter Method "PropertyType", which gets the type of a current property.
        /// </summary>
        public abstract PropertyType PropertyType { get; }

        /// <summary>
        /// Declaring the abstract method Clone(), which will be implemented by the classes CompanyProperty, TrafficLineProperty and HousingProperty.
        /// It is responsible for cloning the original values of a specific property.
        /// </summary>
        /// <returns>Returns the current property object.</returns>
        public abstract PropertyBase Clone();
    }
}
