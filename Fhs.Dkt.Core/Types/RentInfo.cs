﻿// CREATED BY MATHIAS LACKNER
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    public class RentInfo : Interfaces.IDeepCloneable<RentInfo>
    {
        /// <summary>
        /// Private default constructor for internal usage (required for deserialization with our <see cref="Interfaces.IDataManager"/>
        /// componenent).
        /// Created by Mathias Lackner.
        /// </summary>
        private RentInfo() { }

        /// <summary>
        /// Creates a new rent info according to the given data. It is used for <see cref="HousingProperty"/>s to provide convenient
        /// access to the many different types of rents.
        /// </summary>
        /// <param name="rentNoHouse">Specifies the rent for a property without houses or hotels on it.</param>
        /// <param name="rentHotel">Specifies the rent for a property with an hotel on it.</param>
        /// <param name="rentOneHouse">Specifies the rent for a property with one house on it.</param>
        /// <param name="rentTwoHouses">Specifies the rent for property with two houses on it.</param>
        /// <param name="rentThreeHouses">Specifies the rent for a property with three houses on it.</param>
        /// <param name="rentWithFourHouses">Specifies the rent for a property with four houses on it.</param>
        public RentInfo(double rentNoHouse, double rentHotel,
            double rentOneHouse, double rentTwoHouses, double rentThreeHouses, double rentWithFourHouses)
        {
            this.RentWithHouses = new double[4] { rentOneHouse, rentTwoHouses, rentThreeHouses, rentWithFourHouses };
            this.RentWithHotel = rentHotel;
            this.RentNoHouses = rentNoHouse;
        }

        /// <summary>
        /// Creates a new rent info according to the given data. It is used for <see cref="HousingProperty"/>s to provide convenient
        /// access to the many different types of rents.
        /// </summary>
        /// <param name="rentNoHouse">Specifies the rent for a property without houses or hotels on it.</param>
        /// <param name="rentHotel">Specifies the rent for a property with an hotel on it.</param>
        /// <param name="rentWithHouses">Specifies the different rents for a property with one to four houses on it (value must be
        /// an array of length 4).</param>
        public RentInfo(double rentNoHouse, double rentHotel, double[] rentWithHouses)
        {
            if (rentWithHouses == null || rentWithHouses.Length != 4)
                throw new ArgumentException("The given rent four multiple houses must not be null and must consist of exactly 4 double values.");

            this.RentWithHouses = (double[])rentWithHouses.Clone();
            this.RentWithHotel = rentHotel;
            this.RentNoHouses = rentNoHouse;
        }

        /// <summary>
        /// Gets the rent of an associated property without houses on it.
        /// </summary>
        public double RentNoHouses { get; private set; }

        /// <summary>
        /// Gets the rent of an associated property with one to four houses on it in form of an array.
        /// </summary>
        public double[] RentWithHouses { get; private set; }

        /// <summary>
        /// Gets the rent of an associated property with one hotel on it.
        /// </summary>
        public double RentWithHotel { get; private set; }

        /// <summary>
        /// Creates a deep copy of the current object and returns it.
        /// </summary>
        public RentInfo GetClone()
        {
            return new RentInfo(this.RentNoHouses, this.RentWithHotel, (double[])RentWithHouses.Clone());
        }
    }
}
