﻿// CREATED BY MATHIAS LACKNER
/// <summary>
/// Specifies the top level namespace of this application.
/// </summary>
namespace Fhs.Dkt
{
    /// <summary>
    /// Specifies the log level which may be used for the <see cref="Interfaces.ILogger"/> methods. Also, classes implementing
    /// <see cref="Interfaces.ILogMessage"/> and the <see cref="Messaging.LogOnlyMessage"/> require this enum.
    /// Created by Mathias Lackner.
    /// </summary>
    public enum LogLvl
    {
        Error,
        Warn,
        Info,
        Debug
    }
}
