﻿// CREATED BY BORIS BRANKOVIC
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// HousingeProperty is inherited from the Parent-Class PropertyBase.
    /// It defines all necessary methods concerning a HousingProperty.
    /// Created by Boris Brankovic.
    /// </summary>
    public class HousingProperty : PropertyBase
    {        
        /// <summary>
        /// Constructor for the HousingProperty-class.
        /// The Constructor has several parameters, which have to be set by creating a new instance of the HousingProperty-class.
        /// The parameters will be used to initiliaze the variables within this class.
        /// </summary>
        /// <param name="name">The name of the housing property as string.</param>
        /// <param name="city">The name of the city defined in the HousingProperty as enum.</param>
        /// <param name="id">The ID of the current HousingProperty as byte value.</param>
        /// <param name="buyingPrice">The buying price of a current HousingProperty as double value.</param>
        /// <param name="housePrice">The price of one house for the current property as double value.</param>
        /// <param name="hotelPrice">The price of one hotel for the current property as double value.</param>
        /// <param name="rent">The rent price for the current property as type "RentInfo".</param>
        public HousingProperty(string name, City city, byte id, double buyingPrice, double housePrice, double hotelPrice, RentInfo rent)
            : base(PropertyType.Housing, name, id, buyingPrice)
        {
            this.RentInfo = rent;
            this.City = city;
            this.HousePrice = housePrice;
            this.HotelPrice = hotelPrice;
        }


        //Definition of the variables numHouses and numHotels, each as byte values.
        //This variables are responsible for setting the current number of owned houses or hotels of a certain property.
        
        private byte numHouses = 0;
        private byte numHotels = 0;
        
        /// <summary>
        ///Declaring getter and setter for an instance of the class "RentInfo". 
        ///RentInfo contains important informations about the rent of a current property e.g. rent of a property with houses / hotel.
        /// </summary>
        public RentInfo RentInfo { get; private set; }
    
        /// <summary>
        /// Declaring getter for the enum of type "City". 
        /// The enum "City" contains all cities which occur on the gamefield.
        /// </summary>
        public City City { get; }

        ///<summary>Declaring getter for the price of a house.</summary>
        public double HousePrice { get; }
        ///<summary>Declaring getter for the price of a hotel.</summary>
        public double HotelPrice { get; }

        #region Type specific methods
        /// <summary>
        /// GetRentCost() returns the rent of a current property, depending on the Rent-Info i.e. if a house or a hotel was bought.
        /// </summary>
        /// <returns>Returns the exact rent price for a certain property.</returns>
        public double GetRentCost()
        {
            // No houses may be built after a hotel was built
            if (numHotels != 0)
                return RentInfo.RentWithHotel;

            if (numHouses != 0)
                return RentInfo.RentWithHouses[numHouses - 1];

            return RentInfo.RentNoHouses;
        }

        /// <summary>
        /// Implementation of the getter and setter Method "NumHouses". 
        /// It sets the local variable numHouses of the class HousingProperty and checks how many houses are already on the current property.
        /// If the number is greater than 4, then an exception will be thrown. Otherwise, the number of houses will be set to the current value and the view will 
        /// be notified, because the number of houses has changed.
        /// </summary>
        public byte NumHouses
        {
            get => numHouses;
            set
            {
                if (value > 4)
                    throw new Exception("A property cannot have more than four houses on it.");
                numHouses = value;
                onPropertyChanged(this, $"Number of houses changed for property {this.ID}");
            }
        }

        /// <summary>
        /// Implementation of the getter and setter Method "NumHotels". 
        /// It sets the local variable numHotels of the class HousingProperty and checks how many hotels are already on the current property.
        /// If the number is greater than 1, then an exception will be thrown. Otherwise, the number of houses will be set to zero and the view will 
        /// be notified, because the number of hotels has changed.
        /// </summary>
        public byte NumHotels
        {
            get => numHotels;
            set
            {
                if (value > 1)
                    throw new Exception("A property cannot have more than one hotel on it.");

                if (value == 1)
                    numHouses = 0;
                numHotels = value;
                onPropertyChanged(this, $"Number of hotels changed for property {this.ID}");
            }
        }
        #endregion


        #region PropertyBase implementation

        /// <summary>
        /// This lambda expression, defines the current used PropertyType -> in this case with the type Housing.
        /// </summary>
        public override PropertyType PropertyType => PropertyType.Housing;

        /// <summary>
        /// This method clones the original values and returns a copy of them.
        /// It is important to perform this step. Because any other component which works with the properties uses a copy of them.
        /// The original properties stay at the PropertyManager itself. 
        /// It is also importen to set the number of houses and hotels to the current property, otherwise the view will not get the changes.
        /// </summary>
        /// <returns>Returns a copy of the current HousingProperty.</returns>
        public override PropertyBase Clone()
        {
            HousingProperty prop = new HousingProperty(this.Name, this.City, this.ID, this.BuyingPrice, this.HousePrice, this.HotelPrice, this.RentInfo.GetClone());
            prop.numHotels = this.NumHotels;
            prop.numHouses = this.NumHouses;

            return prop;
        }
        #endregion


        public delegate void PropertyChangedDelegate(PropertyBase prop, string description);

        public event PropertyChangedDelegate PropertyChanged;

        private void onPropertyChanged(PropertyBase prop, string description)
        {
            PropertyChanged?.Invoke(prop, description);
        }
    }
}
