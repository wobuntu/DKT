﻿// CREATED BY LUKAS BRUGGER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// Class representing the risk cards in the game.
    /// Extends <see cref="CardBase"/>
    /// Created by Lukas Brugger
    /// </summary>
    public class RiskCard : CardBase
    {

        /// <summary>
        /// Instantiates a risk card, calls according parent constructor, <see cref="CardBase"/>
        /// </summary>
        /// <param name="text">The card text</param>
        /// <param name="action">The card action, see <see cref="CardAction"/></param>
        public RiskCard(string text, CardAction action) : base(text, action) { }

        /// <summary>
        /// Instantiates a risk card, calls according parent constructor, <see cref="CardBase"/>
        /// </summary>
        /// <param name="text">The card text</param>
        /// <param name="action">The card action, see <see cref="CardAction"/></param>
        /// <param name="ownedByPlayer">Id of the player that holds the card</param>
        private RiskCard(string text, CardAction action, Guid ownedByPlayer) : base(text, action, ownedByPlayer) { }

        /// <summary>
        /// Returns CardType of the card.
        /// Always returns CardType.RiskCard, see <see cref="CardType"/>
        /// </summary>
        public override CardType CardType => CardType.RiscCard;

        /// <summary>
        /// Returns a deep clone of the current object.
        /// </summary>
        public override CardBase GetClone()
        {
            return new RiskCard(this.Text, this.Action.GetClone(), this.OwnedByPlayer);
        }
    }
}
