﻿// CREATED BY LUKAS BRUGGER
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// Class representing the bank cards in the game.
    /// Extends <see cref="CardBase"/>
    /// Created by Lukas Brugger
    /// </summary>
    public class BankCard : CardBase
    {
        /// <summary>
        /// Instantiates a bank card, calls according parent constructor, <see cref="CardBase"/>
        /// </summary>
        /// <param name="text">The card text</param>
        /// <param name="action">The card action, see <see cref="CardAction"/></param>
        public BankCard(string text, CardAction action) : base(text, action) { }

        /// <summary>
        /// Instantiates a bank card, calls according parent constructor, <see cref="CardBase"/>
        /// </summary>
        /// <param name="text">The card text</param>
        /// <param name="action">The card action, see <see cref="CardAction"/></param>
        /// <param name="ownedByPlayer">Id of the player that holds the card</param>
        private BankCard(string text, CardAction action, Guid ownedByPlayer) : base(text, action, ownedByPlayer) { }

        /// <summary>
        /// Returns CardType of the card.
        /// Always returns CardType.BankCard, see <see cref="CardType"/>
        /// </summary>
        public override CardType CardType => CardType.BankCard;

        /// <summary>
        /// Returns a deep clone of the current object.
        /// </summary>
        public override CardBase GetClone()
        {
            return new BankCard(this.Text, this.Action.GetClone(), this.OwnedByPlayer);
        }
    }
}
