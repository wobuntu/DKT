﻿// CREATED BY LUKAS BRUGGER

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This enum specifies the possible fieldtypes used in the GameField and is used to determine, on which type of field
    /// a user has landed after moving and what actions are being taken according to this information afterwards in the game loop
    /// Created by Lukas Brugger
    /// </summary>
    public enum FieldType
    {
        Bank = 0,
        Risk = 1,
        Property,
        FixedTax,
        PropertyTax,
        Start,
        Jail,
        GotoJail
    }
}
