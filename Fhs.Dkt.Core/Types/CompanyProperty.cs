﻿// CREATED BY BORIS BRANKOVIC
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// CompanyProperty is inherited from the Parent-Class PropertyBase.
    /// It defines all necessary methods concerning a CompanyProperty.
    /// Created by Boris Brankovic.
    /// </summary>
    public class CompanyProperty : PropertyBase
    {
        
        ///static readonly double-Array - defines the multiplicator for calculating a CompanyProperty rent
        private static readonly double[] priceMultiplicatorByNumCompanies = new double[]{ 5d, 10d, 20d };

        /// <summary>
        /// Empty constructor for the CompanyProperty-Class.
        /// This constructor is important for the Clone-Method.
        /// </summary>
        /// <param name="name">Name of the CompanyProperty as string.</param>
        /// <param name="id">ID of the CompanyProperty as byte-value.</param>
        /// <param name="buyingPrice">The BuyingPrice of a CompanyProperty as double-value.</param>
        public CompanyProperty(string name, byte id, double buyingPrice)
            : base(PropertyType.Company, name, id, buyingPrice) { }

        /// <summary>
        /// GetRentCost calculates the rent for a current CompanyProperty.
        /// The calculation is done by counting the number of owned properties and looking up the price for the number of owned CompanyProperties.
        /// This price will be multiplicated by the number of rolled dice. 
        /// </summary>
        /// <param name="landlordCompaniesOwned">Byte value for checking how many CompanyProperties are owned from the player.</param>
        /// <param name="rolledDiceEyes">Number of the rolled dice.</param>
        /// <returns>Returns the current rent cost for a CompanyProperty as double-value.</returns>
        public double GetRentCost(byte landlordCompaniesOwned, byte rolledDiceEyes)
        {
            if (rolledDiceEyes < 2 || rolledDiceEyes > 12)
                throw new ArgumentException("Invalid value of rolled dice eyes (Must be a number between 2 and 12).");
            if (landlordCompaniesOwned < 1 || landlordCompaniesOwned > 3)
                throw new ArgumentException("Invalid number of owned companies (Must be a number between 1 and 3).");

            return priceMultiplicatorByNumCompanies[landlordCompaniesOwned - 1] * rolledDiceEyes;
        }

        /// <summary>
        /// This lambda expression, defines the current used PropertyType -> in this case with the type Company.
        /// </summary>
        public override PropertyType PropertyType => PropertyType.Company;

        /// <summary>
        /// This method clones the original values and returns a copy of them.
        /// It is important to perform this step. Because any other component which works with the properties uses a copy of them.
        /// The original properties stay at the PropertyManager itself. 
        /// </summary>
        /// <returns>Returns a copy of the current CompanyProperty.</returns>
        public override PropertyBase Clone()
        {
            return new CompanyProperty(this.Name, this.ID, this.BuyingPrice);
        }
    }
}
