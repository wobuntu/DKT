﻿// CREATED BY MATHIAS LACKNER
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhs.Dkt.Types
{
    public class RemoteInvocationInfo
    {
        static JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new Fhs.Dkt.Types.JsonHiddenPropertyResolver(),
            Formatting = Formatting.Indented, // <-- easier for debugging
            TypeNameHandling = TypeNameHandling.All
        };

        private static long nextId = 0;

        [JsonConstructor]
        private RemoteInvocationInfo() { }

        public RemoteInvocationInfo(string componentName, string targetName, Type[] typeParams, bool returnExpected/*, bool blockWhileExecuting*/, params object[] parameters)
        {
            this.ComponentName = componentName;
            this.TargetName = targetName;
            this.Params = parameters;
            this.TypeParams = typeParams;
            this.ReturnExpected = returnExpected;
            this.Id = nextId++;

            // Also store the types of the params for later deserialisation
            if (Params == null)
            {
                ParamTypes = null;
            }
            if (Params != null)
            {
                ParamTypes = new Type[Params.Length];
                for (int i = 0; i < Params.Length; i++)
                {
                    if (Params[i] == null)
                        ParamTypes[i] = null;
                    else
                        ParamTypes[i] = Params[i].GetType();
                }
            }
        }

        public string ComponentName { get; private set; }
        public string TargetName { get; private set; }
        public object[] Params { get; private set; }
        //public bool BlockWhileExecuting { get; private set; }
        public bool ReturnExpected { get; private set; }
        public Type[] TypeParams { get; private set; }
        public Type[] ParamTypes { get; private set; }
        public long Id { get; private set; }

        public RemoteInvocationInfo BuildResponseFromObject(object obj)
        {
            RemoteInvocationInfo info = new RemoteInvocationInfo();
            info.ComponentName = "___custom___";
            info.TargetName = "___response___";
            info.Id = this.Id;
            info.ReturnExpected = false;
            info.Params = new object[] { obj };
            info.ParamTypes = new Type[] { obj?.GetType() };

            return info;
        }

        public T ToResponseValue<T>()
        {
            if (this.ComponentName != "___custom___"
                || this.TargetName != "___response___")
            {
                throw new Exception($"Cannot create a response object from a {nameof(RemoteInvocationInfo)}, which is not a response.");
            }

            // Methods of this types are just for convenience and to hide the implementation details...
            return (T)this.Params[0];
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, this.GetType(), jsonSettings);
        }

        public static RemoteInvocationInfo FromString(String json)
        {
            var info = JsonConvert.DeserializeObject<RemoteInvocationInfo>(json, jsonSettings);

            // Restore faulty types which were not correctyl deserialized by json
            if (info.Params == null)
                return info;

            for (int i = 0; i < info.Params.Length; i++)
            {
                if (info.Params[i] == null)
                    continue;

                if (info.ParamTypes[i] == typeof(Guid) && info.Params[i] is string guidStr)
                    info.Params[i] = Guid.Parse(guidStr);
                else if (info.ParamTypes[i] != null && info.ParamTypes[i].IsEnum)
                    info.Params[i] = Enum.Parse(info.ParamTypes[i], info.Params[i].ToString());
                else
                    info.Params[i] = Convert.ChangeType(info.Params[i], info.ParamTypes[i]);
            }

            return info;
        }
    }
}
