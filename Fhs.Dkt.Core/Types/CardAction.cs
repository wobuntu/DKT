﻿// CREATED BY LUKAS BRUGGER
using System.Linq;

using Fhs.Dkt.Exceptions;
using Fhs.Dkt.Interfaces;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// Class representing the action that is performed when a card is drawn from any card deck. 
    /// If different properties of this class are set or used depends on the always set <see cref="CardActionType"/>.
    /// Distinctions regarding the different behaviour are made when methods are called
    /// Instances of this class are used by any card extending the abstract <see cref="CardBase"/>
    /// </summary>
    public class CardAction : IDeepCloneable<CardAction>
    {
        /// <summary>
        /// Instantiates CardAction object with the passed <see cref="CardActionType"/>.
        /// </summary>
        /// <param name="type">Type of card action, needs to be of type <see cref="CardActionType"/></param>
        public CardAction(CardActionType type)  
        {
            this.ActionType = type;
        }

        /// <summary>
        /// Returns a deep clone of the current object.
        /// </summary>
        public CardAction GetClone()
        {
            return new CardAction(this.ActionType)
            {
                Amount = this.Amount,
                GoToFieldValue = this.GoToFieldValue,
                IncrementFieldValue = this.IncrementFieldValue,
                PayPerHotel = this.PayPerHotel,
                PayPerHouse = this.PayPerHouse,
                PayPercentageProperties = this.PayPercentageProperties,
                Text = this.Text
            };
        }

        /// <summary>
        /// Returns the type value of the action, see <see cref="CardActionType"/>
        /// </summary>
        public CardActionType ActionType { get; private set; }

        /// <summary>
        /// Returns text property of the card that holds the action
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Returns cash amount associated with the card holding the action
        /// </summary>
        public double? Amount { get; set; } = null;

        /// <summary>
        /// Returns the percentage value of how much of the buying price of all properties needs to be paid to the bank
        /// Value is set if the <see cref="CardActionType"/> of the object is RelativePayment
        /// </summary>
        public double? PayPercentageProperties { get; set; } = null;

        /// <summary>
        /// Returns the value, how much has to be paid to the bank for any house the player owns
        /// Value is set if the <see cref="CardActionType"/> of the object is RelativePayment
        /// </summary>
        public double? PayPerHouse { get; set; } = null;

        /// <summary>
        /// Returns the value, how much has to be paid to the bank for any hotel the player owns
        /// Value is set if the <see cref="CardActionType"/> of the object is RelativePayment
        /// </summary>
        public double? PayPerHotel { get; set; } = null;

        /// <summary>
        /// Returns the field index of the field, to which the user is moved when drawing the card
        /// The value is set if the <see cref="CardActionType"/> of the object is MoveToField
        /// </summary>
        public byte? GoToFieldValue { get; set; } = null;

        /// <summary>
        /// Returns the amount of fields, that the user is moved in any given direction when drawing the card
        /// The value is set if the <see cref="CardActionType"/> of the object is MoveForward or MoveBackward
        /// </summary>
        public byte? IncrementFieldValue { get; set; } = null;

        /// <summary>
        /// Returns a formatted text, taking all possible values that need to be included in the text, depending on the <see cref="CardActionType"/>
        /// of the object
        /// Throws a <see cref="InvalidCardTextException"/> if the text for a given <see cref="CardActionType"/> can not be formatted correctly based
        /// on the set properties of the object
        /// </summary>
        /// <returns>Formatted String</returns>
        public string GetFormattedText()
        {
            switch (ActionType)
            {
                // No need to replace something for the following action types:
                case CardActionType.Arrest:
                case CardActionType.GetOutOfJail:
                    if (Text.Contains('{'))
                        throw new InvalidCardTextException($"The card of type {ActionType} must not have placeholders.");
                    return Text;

                case CardActionType.Deposit:
                case CardActionType.Payment:
                    if (this.Amount == null)
                        throw new InvalidCardTextException("The payment card has no amount assigned.");
                    try { return string.Format(Text, this.Amount); }
                    catch
                    {
                        throw new InvalidCardTextException("The payment card has an invalid base text (Too many/few placeholders?).");
                    }

                case CardActionType.MoveToField:
                    if (this.GoToFieldValue == null)
                        throw new InvalidCardTextException("The move-to-card is missing a field value.");
                    if (this.Amount != null)
                        try { return string.Format(Text, this.GoToFieldValue, this.Amount); }
                        catch
                        {
                            throw new InvalidCardTextException("The move-to-card has an invalid base text (Too many/few placeholders?).");
                        }
                    else
                        return string.Format(Text, this.GoToFieldValue);

                case CardActionType.MoveForward:
                case CardActionType.MoveBackward:
                    if (this.IncrementFieldValue == null)
                        throw new InvalidCardTextException("The movement card is missing a incrementing value.");
                    if (this.Amount != null)
                        try { return string.Format(Text, this.IncrementFieldValue, this.Amount); }
                        catch
                        {
                            throw new InvalidCardTextException("The movement card has an invalid base text (Too many/few placeholders?).");
                        }
                    else
                        return string.Format(Text, this.IncrementFieldValue);

                case CardActionType.RelativePayment:
                    if (this.PayPercentageProperties != null
                        && this.PayPerHouse == null
                        && this.PayPerHotel == null)
                    {
                        try { return string.Format(Text, this.PayPercentageProperties); }
                        catch
                        {
                            throw new InvalidCardTextException("The relative payment card has an invalid base text (Too many/few placeholders?");
                        }
                    }
                    else if (this.PayPercentageProperties == null
                        && this.PayPerHouse != null
                        && this.PayPerHotel != null)
                    {
                        try { return string.Format(Text, this.PayPerHouse, this.PayPerHotel); }
                        catch
                        {
                            throw new InvalidCardTextException("The relative payment card has an invalid base text (Too many/few placeholders?)");
                        }
                    }
                    else
                        throw new InvalidCardTextException("The relative payment card is missing its required properties.");
                default:
                    throw new InvalidCardTextException("The action type is unknown, thus the card text cannot be formatted.");
            }
        }
    }
}
