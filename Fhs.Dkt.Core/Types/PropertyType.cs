﻿// CREATED BY MATHIAS LACKNER
/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This enum specifies the possible types of properties and is used by <see cref="PropertyBase"/> and its ancestors.
    /// Created by Mathias Lackner.
    /// </summary>
    public enum PropertyType
    {
        /// <summary>The property is a housing property (Houses and hotels can be built on it).</summary>
        Housing,
        /// <summary>The property is a traffic line.</summary>
        TrafficLine,
        /// <summary>The property is a company.</summary>
        Company
    }
}
