﻿// CREATED BY MATHIAS LACKNER
using Fhs.Dkt.Interfaces;
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// Abstract class providing a base class for any card class used in the game.
    /// Contains all shared properties and methods, that all cards share, like the card text, card type and card action.
    /// </summary>
    public abstract class CardBase : IDeepCloneable<CardBase>
    {
        /// <summary>
        /// Base constructor for card instances, taking card text and action as arguments.
        /// Sets the card text and the card action (see <see cref="CardAction"/>
        /// Throws an <see cref="ArgumentNullException"/> if no action type was passed
        /// </summary>
        /// <param name="text">The card text</param>
        /// <param name="action">The card Action, see <see cref="CardAction"/></param>
        protected CardBase(string text, CardAction action)
        {
            if (action == null)
                throw new ArgumentNullException("The action of a card must not be null.");

            this.Text = text;
            this.Action = action;
        }

        /// <summary>
        ///  Base constructor for card instances, taking card text, action and the ownedByPlayer id as arguments
        /// Sets the card text, card action (see <see cref="CardAction"/> and the ownedByPlayer value, if a player holds the card
        /// on instantiation
        /// Throws an <see cref="ArgumentNullException"/> if no action type was passed
        /// </summary>
        /// <param name="text">The card text</param>
        /// <param name="action">The card Action, see <see cref="CardAction"/></param>
        /// <param name="ownedByPlayer">Guid of player that holds the card</param>
        protected CardBase(string text, CardAction action, Guid ownedByPlayer)
        {
            if (action == null)
                throw new ArgumentNullException("The action of a card must not be null.");

            this.Text = text;
            this.Action = action;
            this.OwnedByPlayer = ownedByPlayer;
;       }

        /// <summary>
        /// Returns the text of the card
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Abstract <see cref="CardType"/> property of the card
        /// </summary>
        public abstract CardType CardType { get; }

        /// <summary>
        /// Returns the <see cref="CardAction"/> property of the card
        /// </summary>
        public CardAction Action { get; }

        /// <summary>
        /// Returns the id of the holding player, if a player holds the card at the time of call
        /// </summary>
        public Guid OwnedByPlayer { get; set; } = new Guid();

        /// <summary>
        /// Returns a deep clone of the current object.
        /// </summary>
        public abstract CardBase GetClone();
    }
}
