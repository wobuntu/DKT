﻿// CREATED BY MATHIAS LACKNER
/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This enum represents the cities available for the <see cref="CardBase"/>s.
    /// CREATED BY MATHIAS LACKNER
    /// </summary>
    public enum City
    {
        Bregenz,
        Eisenstadt,
        Graz,
        Innsbruck,
        Salzburg,
        Klagenfurt,
        Linz,
        Wien
    }
}
