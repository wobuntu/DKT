﻿// CREATED BY MATHIAS LACKNER
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This class represents a custom contract resolver for Newtonsoft.Json. It enables the serialization / deserialization of
    /// private properties, which is not possible by using the default one.
    /// </summary>
    public class JsonHiddenPropertyResolver : Newtonsoft.Json.Serialization.DefaultContractResolver
    {
        // With help of: https://stackoverflow.com/questions/4066947/private-setters-in-json-net
        protected override Newtonsoft.Json.Serialization.JsonProperty CreateProperty(
            MemberInfo member, MemberSerialization memberSerialization)
        {
            var prop = base.CreateProperty(member, memberSerialization);

            if (!prop.Writable)
            {
                var property = member as PropertyInfo;
                if (property != null)
                {
                    prop.Writable = property.GetSetMethod(true) != null;
                    prop.Readable = property.GetGetMethod(true) != null;
                }
            }

            return prop;
        }
    }
}
