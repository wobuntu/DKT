﻿// CREATED BY BORIS BRANKOVIC, GABRIEL DAX (alphabetical order)

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This enum is responsible for checking the relative ownership of a current property.
    /// The enum OwnerShip defines whether the bank is the owner, the current player is the owner, 
    /// another player is the owner, or if the property is not buyable for a certain reason.
    /// Created by Boris Brankovic and Gabriel Dax.
    /// </summary>
    public enum OwnerShip
    {
        OwnerIsBank,
        OwnerIsOtherPlayer,
        OwnerIsPlayer,
        PropertyNotBuyable
    }
}
