﻿// CREATED BY MATHIAS LACKNER
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// This enum represents the choices the user may have for decisions she/he has to make (e.g. if a property shall be bought). It is
    /// also used to return the selected choice back to the caller of methods from the <see cref="Interfaces.IGameField"/> method
    /// implementations. This is a flagged value, meaning arbitrary combinitions via an logical OR ('|') are allowed.
    /// Created by Mathias Lackner.
    /// </summary>
    [Flags]
    public enum UserChoice
    {
        Yes = 1,
        No = 2,
        Cancel = 4,
        YesNo = 3,
        YesNoCancel = 7
    }
}
