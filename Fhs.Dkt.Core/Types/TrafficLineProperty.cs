﻿// CREATED BY BORIS BRANKOVIC
using System;

/// <summary>This namespace contains common types of all components within this solution (Part of the core assembly).</summary>
namespace Fhs.Dkt.Types
{
    /// <summary>
    /// TrafficLineProperty is inherited from the Parent-Class PropertyBase.
    /// It defines all necessary methods concerning a TrafficLineProperty.
    /// Created by Boris Brankovic.
    /// </summary>
    public class TrafficLineProperty : PropertyBase
    {
        ///static readonly double-Array - defines the multiplicator for calculating a TrafficLine-Property rent
        private static readonly double[] priceMultiplicatorByNumLines = new double[] { 20d, 40d, 80d, 160d };

        /// <summary>
        /// Empty constructor for the TrafficLineProperty-Class.
        /// This constructor is important for the Clone-Method.
        /// </summary>
        /// <param name="name">Name of the TrafficLineProperty as string.</param>
        /// <param name="id">ID of the TrafficLineProperty as byte-value.</param>
        /// <param name="buyingPrice">The BuyingPrice of a TrafficLineProperty as double-value.</param>
        public TrafficLineProperty(string name, byte id, double buyingPrice)
            : base(PropertyType.TrafficLine, name, id, buyingPrice) { }

        /// <summary>
        /// GetRentCost defines the rent-price for a certain TrafficLineProperty.
        /// The price is calculated by counting the owned TrafficLine-Properties and multiplicating afterwards with the predefined rent values, 
        /// reffering to the number of owned properties in "priceMultiplicatorByNumLines".
        /// </summary>
        /// <param name="landlordTrafficLinesOwned">Byte value for checking how many TrafficLineProperties are owned from the player.</param>
        /// <returns>Returns the current rent cost for a TrafficLineProperty as double-value.</returns>
        public double GetRentCost(byte landlordTrafficLinesOwned)
        {
            if (landlordTrafficLinesOwned < 1 || landlordTrafficLinesOwned > 4)
                throw new ArgumentException("Invalid number of owned traffic lines (Must be a number between 1 and 4).");

            return priceMultiplicatorByNumLines[landlordTrafficLinesOwned - 1];
        }

        /// <summary>
        /// This lambda expression, defines the current used PropertyType -> in this case with the type TrafficLine.
        /// </summary>
        public override PropertyType PropertyType => PropertyType.TrafficLine;

        /// <summary>
        /// This method clones the original values and returns a copy of them.
        /// It is important to perform this step. Because any other component which works with the properties uses a copy of them.
        /// The original properties stay at the PropertyManager itself. 
        /// </summary>
        /// <returns>Returns a copy of the current TrafficLineProperty.</returns>
        public override PropertyBase Clone()
        {
            return new TrafficLineProperty(this.Name, this.ID, this.BuyingPrice);
        }
    }
}