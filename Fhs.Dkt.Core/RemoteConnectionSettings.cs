﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhs.Dkt
{
    public static class RemoteConnectionSettings
    {
        public static string Host = "localhost";
        public static int ServerPort = 0;
        public static int LocalPort = 0;

        public const int MsBetweenShutdownChecks = 200;
        public const int MsTcpSendTimeout = 10000;
        public const int MsTcpReceiveTimeout = 10000;
        public const int MsStreamReadTimeout = 10000;
        public const int MsStreamWriteTimeout = 10000;
        public const int MsBetweenConnectionAttempts = 500;
        public const int MsBeforeDataAvailableChecks = 100;
        public const int HeaderByteLength = 4;
        public const int HeaderBitLength = HeaderByteLength * 8;
    }
}
